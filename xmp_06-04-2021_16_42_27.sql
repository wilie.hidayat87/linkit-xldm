-- MySQL dump 10.13  Distrib 8.0.23, for Linux (x86_64)
--
-- Host: localhost    Database: xmp
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `rev_check`
--

DROP TABLE IF EXISTS `rev_check`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rev_check` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `service` varchar(100) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `morev` bigint(20) DEFAULT '0',
  `modrev` bigint(20) DEFAULT '0',
  `dprev` bigint(20) DEFAULT '0',
  `dpdrev` bigint(20) DEFAULT '0',
  `charging` int(11) DEFAULT '1000',
  PRIMARY KEY (`id`),
  KEY `subject` (`subject`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rev_check`
--

LOCK TABLES `rev_check` WRITE;
/*!40000 ALTER TABLE `rev_check` DISABLE KEYS */;
INSERT INTO `rev_check` VALUES (1,'memberpush','NORMAL',0,0,506000,0,2000),(2,'memberpush','EXT',0,0,0,0,2000),(3,'memberpush','BP',0,0,0,0,2000),(4,'memberpush','HPR',0,0,4000,0,2000),(5,'Videorazzi_register','OK',0,0,0,0,2000),(6,'Videorazzi_register','NORMAL',0,0,254000,0,2000),(7,'Videorazzi_register','EXT',0,0,244000,0,2000),(8,'Videorazzi_register','BP',0,0,10000,0,2000),(9,'Videorazzi_register','HPR',0,0,0,0,2000),(10,'memberpush','PORTAL',0,0,0,0,2000),(11,'latino','HPR',0,0,22000,0,2000),(12,'latino','NORMAL',0,0,196000,0,2000),(13,'famcepek','NORMAL',0,0,168000,0,2000),(14,'famcepek','HPR',0,0,2000,0,2000),(15,'nudigital','OK',0,0,0,0,2000),(16,'nudigital','NORMAL',0,0,0,0,2000),(17,'CicilanXL','NORMAL',0,0,0,100000,2000),(18,'cicilanXL1','SPC',0,0,33810000,35000000,2000),(19,'cicilanXL2','SPC',0,0,1620000,4000000,2000),(20,'cicilanXL3','NORMAL',0,0,0,100000,2000),(21,'cicilanXL4','NORMAL',0,0,0,100000,1000),(22,'cicilanXL5','NORMAL',0,0,0,100000,1000),(23,'cicilanXL6','NORMAL',0,0,0,100000,1000),(24,'cicilanXL1','NORMAL',0,0,0,7240000,2000),(25,'cicilanXL2','NORMAL',0,0,16000,3000000,2000),(26,'famcepek','KE',0,0,0,0,2000),(27,'cicilanXL5','SPC',0,0,0,100000,1000),(28,'cicilanXL4','SPC',0,0,0,100000,1000),(29,'cicilanXL6','SPC',0,0,0,100000,1000),(32,'cicilanXL2','TMW',0,0,0,3000000,2000);
/*!40000 ALTER TABLE `rev_check` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;


DROP TABLE IF EXISTS `mt_delay`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mt_delay` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `obj` text NOT NULL,
  `service` varchar(10) NOT NULL,
  `operator` varchar(20) DEFAULT '',
  `adn` int(10) NOT NULL,
  `msisdn` varchar(32) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_expired` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `exp_stat` (`date_expired`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `adn`
--

DROP TABLE IF EXISTS `adn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `adn` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL,
  `description` varchar(128) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `status` tinyint NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `adn`
--

LOCK TABLES `adn` WRITE;
/*!40000 ALTER TABLE `adn` DISABLE KEYS */;
INSERT INTO `adn` VALUES (1,'99876','PUSH SDC','2016-07-19 12:03:55','2016-07-19 12:07:09',1),(2,'98876','PULL SDC','2016-07-19 12:08:06','2021-04-06 08:14:35',1),(3,'9987609','Test SDC','2017-04-04 11:27:17','2017-04-04 11:27:26',0),(4,'97770','PUSH SDC new','2019-09-13 12:45:18','2019-09-23 16:55:47',1);
/*!40000 ALTER TABLE `adn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `charging`
--

DROP TABLE IF EXISTS `charging`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `charging` (
  `id` int NOT NULL AUTO_INCREMENT,
  `operator` int NOT NULL,
  `adn` char(10) NOT NULL,
  `charging_id` varchar(128) NOT NULL,
  `gross` decimal(10,2) NOT NULL,
  `netto` decimal(10,2) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `sender_type` varchar(20) NOT NULL,
  `message_type` varchar(20) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `operator` (`operator`),
  CONSTRAINT `charging_ibfk_1` FOREIGN KEY (`operator`) REFERENCES `operator` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `charging`
--

LOCK TABLES `charging` WRITE;
/*!40000 ALTER TABLE `charging` DISABLE KEYS */;
INSERT INTO `charging` VALUES (1,1,'99876','99876',0.00,0.00,'linkit','linkit','text','mo','2016-07-19 14:54:11','2016-07-19 14:54:11'),(2,1,'99876','9987605',2000.00,2000.00,'linkit','linkit','text','dailypush','2016-07-19 14:55:00','2016-07-19 14:55:00'),(3,1,'97770','977704',500.00,500.00,'admin','hello','text','mtpush','2016-07-19 14:58:43','2019-12-16 14:16:13'),(4,1,'99876','9987600',0.00,0.00,'linkit','linkit','text','mtpull','2016-07-22 16:51:11','2016-07-22 16:51:11'),(5,1,'98876','9887603',1000.00,1000.00,'linkit','linkit','text','mtpull','2016-07-22 18:00:35','2016-07-22 18:00:35'),(6,1,'99876','9987605',2000.00,2000.00,'admin','wakipwd','text','mtpush','2018-03-15 15:12:26','2018-03-15 15:12:26'),(7,1,'99876','9987605',2000.00,2000.00,'linkit','linkit','text','mtpull','2018-07-19 16:31:46','2018-07-19 16:31:46'),(8,1,'98876','988761',500.00,500.00,'admin','hello','text','mtpull','2018-07-25 17:53:06','2018-07-25 17:53:06'),(9,1,'98876','988762',1000.00,1000.00,'admin','hello','text','mtpull','2018-07-25 17:54:00','2018-07-25 17:54:00'),(10,1,'99876','9987601',1000.00,1000.00,'admin','hello','text','mtpush','2019-02-01 10:22:40','2019-02-01 10:22:40'),(11,1,'97770','9777005',2000.00,2000.00,'admin','hello','text','dailypush','2019-09-23 17:01:30','2019-09-23 17:01:30'),(12,1,'97770','9777006',2000.00,2000.00,'admin','hello','text','mtpush','2019-09-23 17:03:26','2019-09-23 17:03:26'),(13,1,'97770','97770',0.00,0.00,'admin','hello','text','mo','2019-09-24 17:39:36','2019-09-24 17:39:36'),(14,1,'97770','977701',2000.00,2000.00,'admin','hello','text','mtpull','2019-10-14 16:31:42','2019-10-24 12:10:04'),(15,1,'99876','9987604',1000.00,1000.00,'linkit','linkit','text','dailypush','2019-11-26 11:01:38','2019-11-26 11:01:38'),(16,1,'98876','9887604',2000.00,2000.00,'admin','hello','text','mtpull','2019-12-03 12:12:17','2019-12-03 12:12:17'),(17,1,'97770','977702',0.00,0.00,'admin','hello','text','mtpull','2019-12-16 14:11:36','2019-12-16 14:11:36'),(18,1,'97770','977703',500.00,500.00,'admin','hello','text','mtpull','2019-12-16 14:14:05','2019-12-16 14:14:05'),(19,1,'97770','977705',3000.00,3000.00,'admin','hello','text','mtpull','2019-12-16 14:20:36','2019-12-16 14:20:36'),(20,1,'97770','977706',5000.00,5000.00,'admin','hello','text','mtpull','2019-12-16 14:23:22','2019-12-16 14:23:22'),(21,1,'97770','977707',8000.00,8000.00,'admin','hello','text','mtpull','2019-12-16 14:23:47','2019-12-16 14:23:47'),(22,1,'97770','977708',10000.00,10000.00,'admin','hello','text','mtpull','2019-12-16 14:24:09','2019-12-16 14:24:09'),(23,1,'97770','977709',15000.00,15000.00,'admin','hello','text','mtpull','2019-12-16 14:24:33','2019-12-16 14:54:53'),(29,1,'97770','9777015',1000.00,1000.00,'admin','hello','text','mtpull','2019-12-16 14:36:54','2019-12-16 14:36:54');
/*!40000 ALTER TABLE `charging` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mechanism`
--

DROP TABLE IF EXISTS `mechanism`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mechanism` (
  `id` int NOT NULL AUTO_INCREMENT,
  `pattern` varchar(128) NOT NULL,
  `operator_id` int NOT NULL,
  `service_id` int NOT NULL,
  `handler` varchar(50) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `status` tinyint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `operator_mechanism_map` (`operator_id`),
  KEY `service_id_map` (`service_id`),
  CONSTRAINT `mechanism_ibfk_1` FOREIGN KEY (`operator_id`) REFERENCES `operator` (`id`),
  CONSTRAINT `mechanism_ibfk_2` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mechanism`
--

LOCK TABLES `mechanism` WRITE;
/*!40000 ALTER TABLE `mechanism` DISABLE KEYS */;
INSERT INTO `mechanism` VALUES (1,'reg member',1,1,'service_creator_handler','2016-07-19 15:06:28','2020-12-18 05:27:29',1),(2,'unreg member',1,1,'service_creator_handler','2016-07-19 15:15:01','2016-07-19 15:15:01',1),(3,'reg daftar',1,3,'service_creator_handler','2016-07-22 17:33:15','2019-05-23 17:05:42',1),(4,'unreg daftar',1,3,'service_creator_handler','2016-07-22 17:34:03','2019-04-08 13:00:41',1),(5,'reg videorazzi',1,4,'service_creator_handler','2017-03-24 16:02:08','2019-06-28 17:30:59',1),(6,'unreg videorazzi',1,4,'service_creator_handler','2017-03-24 16:04:23','2017-03-24 16:12:11',1),(17,'reg latino',1,17,'service_creator_handler','2017-12-11 10:32:40','2020-12-18 07:13:17',1),(18,'unreg latino',1,17,'service_creator_handler','2017-12-11 10:34:16','2018-04-05 11:10:44',1),(29,'unreg cepek',1,28,'service_creator_handler','2018-07-16 18:52:18','2018-07-16 20:12:27',1),(30,'reg cepek',1,28,'service_creator_handler','2018-07-16 18:53:23','2019-07-17 16:25:22',1),(48,'cepek1',1,30,'service_creator_handler','2018-07-18 17:03:22','2019-11-26 15:21:47',1),(49,'reg nu',1,33,'service_creator_handler','2018-09-14 16:33:34','2019-05-23 17:05:19',0),(50,'unreg nu',1,33,'service_creator_handler','2018-09-14 16:38:05','2019-04-08 12:54:13',1),(51,'nu1',1,35,'service_creator_handler','2018-09-14 16:43:00','2019-04-08 12:51:36',1),(71,'reg cicil1',1,42,'service_creator_handler','2019-02-20 15:13:31','2020-12-18 21:48:04',1),(72,'unreg cicil1',1,42,'service_creator_handler','2019-02-20 15:14:38','2019-02-20 15:14:38',1),(73,'reg cicil2',1,43,'service_creator_handler','2019-02-20 15:15:30','2020-12-18 21:45:46',1),(75,'reg cicil3',1,44,'service_creator_handler','2019-02-20 15:17:46','2020-12-18 21:45:39',1),(76,'Unreg cicil3',1,44,'service_creator_handler','2019-02-20 15:18:23','2019-02-20 15:18:23',1),(77,'reg cicil4',1,45,'service_creator_handler','2019-02-20 15:28:29','2019-11-15 17:49:13',1),(78,'unreg cicil4',1,45,'service_creator_handler','2019-02-20 15:29:00','2019-02-20 15:29:00',1),(79,'reg cicil5',1,46,'service_creator_handler','2019-02-20 15:30:07','2019-11-15 17:50:01',1),(80,'unreg cicil5',1,46,'service_creator_handler','2019-02-20 15:30:29','2019-02-20 15:30:29',1),(81,'reg cicil6',1,47,'service_creator_handler','2019-02-20 15:31:23','2019-11-15 17:50:14',1),(82,'unreg cicil6',1,47,'service_creator_handler','2019-02-20 15:31:43','2019-02-20 15:31:43',1),(83,'reg ygf',1,48,'service_creator_handler','2019-04-01 17:58:37','2020-12-23 12:27:25',1),(84,'unreg ygf',1,48,'service_creator_handler','2019-04-01 17:59:18','2019-04-01 17:59:18',1),(90,'reg cerpen',1,52,'service_creator_handler','2019-10-14 15:46:16','2020-12-18 05:45:51',1),(93,'unreg cerpen',1,52,'service_creator_handler','2019-10-15 11:46:03','2019-10-15 11:46:03',1),(94,'cerpen1',1,53,'service_creator_handler','2019-10-15 13:02:33','2019-10-15 13:15:33',1),(96,'latino1',1,55,'service_creator_handler','2019-12-03 14:49:12','2019-12-03 16:32:07',1),(97,'test',1,56,'service_creator_handler','2019-12-16 11:34:14','2019-12-16 14:11:51',0),(98,'test 100',1,56,'service_creator_handler','2019-12-16 11:43:37','2019-12-16 14:12:04',0),(99,'test 101',1,56,'service_creator_handler','2019-12-16 14:29:56','2019-12-16 14:29:56',0),(100,'test 102',1,56,'service_creator_handler','2019-12-16 14:34:24','2019-12-17 10:40:18',0),(101,'test 103',1,56,'service_creator_handler','2019-12-16 14:37:22','2019-12-16 14:37:22',0),(102,'Test 104',1,56,'service_creator_handler','2019-12-16 14:39:00','2019-12-17 10:41:23',0),(103,'test 105',1,56,'service_creator_handler','2019-12-16 14:39:53','2019-12-16 14:39:53',0),(104,'test 106',1,56,'service_creator_handler','2019-12-16 14:40:47','2019-12-17 10:43:34',0),(105,'test 107',1,56,'service_creator_handler','2019-12-16 14:41:46','2019-12-16 14:41:46',0),(106,'test 108',1,56,'service_creator_handler','2019-12-16 14:43:11','2019-12-16 14:43:11',0),(107,'test 109',1,56,'service_creator_handler','2019-12-16 14:43:52','2019-12-16 14:43:52',0),(108,'test 110',1,56,'service_creator_handler','2019-12-16 14:44:31','2019-12-16 14:44:31',0),(109,'test 112',1,56,'service_creator_handler','2019-12-16 14:54:02','2019-12-17 10:44:35',0),(110,'test 111',1,56,'service_creator_handler','2019-12-16 14:56:08','2019-12-16 14:56:08',0),(111,'test 113',1,56,'service_creator_handler','2019-12-16 14:57:06','2019-12-17 10:45:08',0),(112,'test 114',1,56,'service_creator_handler','2019-12-16 14:58:27','2019-12-17 10:45:59',0),(113,'test 115',1,56,'service_creator_handler','2019-12-16 14:59:13','2019-12-17 10:46:29',0),(114,'test 116',1,56,'service_creator_handler','2019-12-16 15:00:09','2019-12-17 10:46:54',0),(117,'reg trimxs',1,58,'service_creator_handler','2020-03-10 14:29:51','2020-12-18 14:32:14',1),(118,'unreg trimxs',1,58,'service_creator_handler','2020-03-10 14:30:18','2020-03-10 14:30:18',1),(119,'unreg cicil2',1,43,'service_creator_handler','2020-09-25 09:54:03','2020-09-25 09:54:03',1);
/*!40000 ALTER TABLE `mechanism` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module`
--

DROP TABLE IF EXISTS `module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `module` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(128) DEFAULT NULL,
  `handler` varchar(30) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `status` tinyint NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module`
--

LOCK TABLES `module` WRITE;
/*!40000 ALTER TABLE `module` DISABLE KEYS */;
INSERT INTO `module` VALUES (1,'text','-','service_module_text','2016-07-19 15:01:53','2021-04-06 08:16:00',1),(2,'registration','-','service_module_registration','2016-07-19 15:02:16','2021-04-06 08:16:00',1),(3,'unregistration','-','service_module_unregistration','2016-07-19 15:02:39','2021-04-06 08:16:00',1),(4,'waplink','-','service_module_waplink','2016-07-19 15:02:55','2021-04-06 08:16:00',1),(5,'wappush','-','service_module_wappush','2016-07-19 15:03:13','2021-04-06 08:16:00',1),(6,'textdelay','-','service_module_textdelay','2016-07-19 15:03:31','2021-04-06 08:16:00',1),(7,'proxlconfirm','-','proxl_service_custom_confirm','2016-07-19 15:04:03','2021-04-06 08:16:00',1),(8,'notification','-','service_module_notification','2016-07-19 15:04:24','2021-04-06 08:16:00',1),(9,'point','-','service_module_point','2016-07-19 15:04:47','2021-04-06 08:16:00',1),(10,'test','-','service_module_test','2017-04-04 11:58:32','2021-04-06 08:16:00',0);
/*!40000 ALTER TABLE `module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `operator`
--

DROP TABLE IF EXISTS `operator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `operator` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `long_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `operator`
--

LOCK TABLES `operator` WRITE;
/*!40000 ALTER TABLE `operator` DISABLE KEYS */;
INSERT INTO `operator` VALUES (1,'XLSDP','XLSDP'),(2,'indosat','INDOSAT');
/*!40000 ALTER TABLE `operator` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reply`
--

DROP TABLE IF EXISTS `reply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reply` (
  `id` int NOT NULL AUTO_INCREMENT,
  `mechanism_id` int NOT NULL,
  `module_id` int NOT NULL,
  `charging_id` int DEFAULT NULL,
  `subject` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `message` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  PRIMARY KEY (`id`),
  KEY `mechanism_map` (`mechanism_id`),
  KEY `module_map` (`module_id`),
  KEY `charging_id` (`charging_id`),
  CONSTRAINT `reply_ibfk_1` FOREIGN KEY (`mechanism_id`) REFERENCES `mechanism` (`id`) ON DELETE CASCADE,
  CONSTRAINT `reply_ibfk_2` FOREIGN KEY (`module_id`) REFERENCES `module` (`id`),
  CONSTRAINT `reply_ibfk_3` FOREIGN KEY (`charging_id`) REFERENCES `charging` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=469 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reply`
--

LOCK TABLES `reply` WRITE;
/*!40000 ALTER TABLE `reply` DISABLE KEYS */;
INSERT INTO `reply` VALUES (3,2,3,1,' ','Terima kasih telah berhenti dari REG MEMBER'),(40,6,3,1,' ','Terima kasih Anda sudah berhenti dari layanan Konten VideoRazzi. CS:087878000208'),(80,18,3,1,' ','Terima kasih Anda sudah berhenti dari layanan Konten Latino. CS:087878000208'),(115,29,3,1,' ','unreg done'),(263,72,3,1,' ','Unreg done'),(270,76,2,1,' ','Unreg done'),(273,78,3,1,' ','unreg done'),(276,80,3,1,' ','unreg done'),(279,82,3,1,' ','unreg done'),(284,84,3,1,' ','terima kasih sudah tidak terdaftar ygf'),(301,51,1,2,' ','Terima kasih sudah membeli content NU DIGITAL Tarif Rp.2200/sms klik http://fe.linkit360.com/nuportal/ CS:087878000208'),(304,50,3,1,' ','Anda sudah berhenti dari langganan NU DIGITAL. Trimakasih atas partisipasinya.CS :087878000208'),(311,4,3,1,' ','Anda tlah dinonatifkan dr  layanan main. Anda tdk lagi bergabung di layanan ini&tdk; menerima konten dr kami. Trims atas partisipasinya.cs:087878000208'),(332,49,2,1,' ','Terima kasih, Anda telah mendaftar ke layanan NU DIGITAL  4SMS/Minggu. Trf Rp.2200/sms. Ketik Ya, Yes, Iya untuk gabung. CS:087878000208'),(333,49,6,2,' ','Yuk Donwload konten yang menyejukkan hati, Klik http://xl.nu.videorazzi.mobi #userpass#  untuk stop, UNREG NU ke 99876 CS:087878000208 WA:http://bit.ly/cslinkit360'),(336,3,2,1,' ','Hai, kamu telah berlangganan Game MEMBER. 4sms/Mnggu Tarif 2rb/sms. Stop ketik  UNREG MEMBER kirim ke 3985 CS:087878000208'),(337,3,6,2,' ','Selamat Layanan Slypee  Kamu sudah aktif. Login di http://xl.slypee.com/member  user: 8888 pasword:123456 CS:087878000208 WA:http://bit.ly/cslinkit360'),(352,5,2,1,' ','Terima kasih anda telah mendaftar ke VideoRazzi 4sms/minggu Rp2200/sms. Balas sms ini dengan ketik Ya,Yes,Iya,Y untuk berlangganan. CS: 087878000208'),(353,5,6,2,' ','Klip Bollywood. xl.videorazzi.mobi #userpass# trf GPRS berlaku.stop:UNREG VIDEORAZZI ke 99876 atau *123*66# CS:087878000208 WA:bit.ly/cslinkit360'),(356,30,2,1,' ','Terimakasih layanan Family Cepek kamu sudah aktip. Max 3SMS/Minggu, 2rb/sms GPRS aktif.Stop ketik: UNREG CEPEK ke 99876 CS:087878000208'),(357,30,6,2,' ','Kamu udah bisa langsung main kuis Family Cepek yuk ketik *500*79# CS:087878000208 WA:http://bit.ly/cslinkit360'),(379,93,3,13,' ','Anda sudah berhenti dari langganan konten CERPEN. Trimakasih atas partisipasinya.CS :021-52902182'),(381,94,1,14,' ','Terimakasih sudah membeli content CERPEN Tariff Rp.2200/sms, Klik http://xl.cerpen.mobi/  #userpass#  CS:02152902182'),(388,77,2,1,' ','Wow! Kamu sdh bisa nikmati dan download game keren ini (url http://xl.slypee.com ). C dgn harga awal Rp.2000+tarif GPRS dan dicicil 1x/minggu\n'),(389,77,6,10,' ','Terimakasih kamu dapat terus menikmati game menarik ini (url http://bit.ly/2CKzuVl). CS:087878000208 WA:http://bit.ly/cslinkit360'),(390,79,2,1,' ','Wow! Kamu sdh bisa nikmati dan download game keren ini (url http://xl.slypee.com ). C dgn harga awal Rp.2000+tarif GPRS dan dicicil 1x/minggu\n'),(391,79,6,10,' ','Terimakasih kamu dapat terus menikmati game menarik ini (url http://bit.ly/2CKzuVl). CS:087878000208 WA:http://bit.ly/cslinkit360\n'),(392,81,2,1,' ','Wow! Kamu sdh bisa nikmati dan download game keren ini (url http://xl.slypee.com ). C dgn harga awal Rp.2000+tarif GPRS dan dicicil 1x/minggu\n'),(393,81,6,10,' ','Terimakasih kamu dapat terus menikmati game menarik ini (url http://bit.ly/2CKzuVl). CS:087878000208 WA:http://bit.ly/cslinkit360'),(394,48,1,2,' ','Terima kasih Anda sdh membeli konten Family Cepek (Pull) yuk ketik *500*79#'),(397,96,1,2,' ','Terimakasih sudah membeli content LATINO Tariff Rp.2200/sms, Klik http://xlatino.slypee.com CS:02152902182'),(407,97,1,17,' ','Test FTLinkIt360'),(408,98,1,17,' ','Test FTLinkIT 100'),(409,99,1,18,' ','Test FTLinkIT 101'),(411,101,1,29,' ','Test FTLinkIT 103'),(413,103,1,11,' ','Test FTLinkIT 105'),(415,105,1,19,' ','Test FTLinkIT 107'),(416,106,1,20,' ','Test FTLinkIT 108'),(417,107,1,21,' ','Test FTLinkIT 109'),(418,108,1,22,' ','Test FTLinkIT 110'),(420,110,1,23,' ','Test FTLinkIT 111'),(425,100,1,18,' ','Test FTLinkIT 102'),(426,102,1,29,' ','Test FTLinkIT 104'),(427,104,1,14,' ','Test FTLinkIT 106'),(428,109,1,19,' ','Test FTLinkIT 112'),(429,111,1,20,' ','Test FTLinkIT 113'),(431,112,1,21,' ','Test FTLinkIT 114'),(432,113,1,22,' ','Test FTLinkIT 115'),(433,114,1,23,' ','Test FTLinkIT 116'),(447,118,3,13,' ','Terima kasih kamu sudah berhenti layanan TRIMXS. Terima kasih atas partisipasinya CS:021-52902182'),(448,119,3,1,' ',''),(449,1,2,1,' ',''),(450,1,6,2,' ','Slmt Layanan Member Slypee km sdhaktif.Login di http://goo.gl/8jBJ7g user:888888 pasword:123456 Info stop *123*66# CS:087878000208 WA:bit.ly/cslinkit360'),(451,90,2,13,' ',''),(452,90,6,11,' ','Yuk lihat video ceramah Ust. Subhan Bawazier, klik http://xl.cerpen.mobi/\n #userpass# (Tarif Data Berlaku). CS:021-52902182'),(453,17,2,4,' ',''),(454,17,6,6,' ','Download goyang dance latinmu disini, Klik http://xlatino.slypee.com #userpass# untuk stop, *123*66# CS:087878000208 WA:http://bit.ly/cslinkit360'),(457,117,2,13,' ',''),(458,117,6,14,' ','Yuk mulai program fitness klik http://xl.trimxs.co/ #userpass# (Tarif Data Berlaku). Stop: UNREG TRIMXS ke 97770. CS:021-52902182'),(461,75,2,1,' ',''),(462,75,6,2,' ','Terimakasih kamu dpt menikmati game menarik di http://bit.ly/2CKzuVl login dgn #userpass# CS:087878000208 WA:http://bit.ly/cslinkit360'),(463,73,2,1,' ',''),(464,73,6,2,' ','Terimakasih kamu dpt menikmati game menarik di http://bit.ly/2CKzuVl login dgn #userpass# CS:087878000208 WA:http://bit.ly/cslinkit360'),(465,71,2,1,' ','Terima kasih, layanan Cicilan game1x 2rb/week sdh aktif. Utk berhenti,ketik UNREG cicil1 ke 99876 atau tlp *500*8#. Info 817/livechat CS XL in http://bit.ly/LC_myXLcare'),(466,71,6,2,' ','Terimakasih kamu dpt menikmati game menarik di http://bit.ly/2CKzuVl login dgn #userpass# CS:087878000208 WA:http://bit.ly/cslinkit360'),(467,83,2,1,' ',''),(468,83,6,2,' ','Yuk Latihan Yoga hari ini sblm beraktifitas cek id.yogaforyou.club\n#userpass# stop:*500*8# CS:087878000208 WA:http://bit.ly/cslinkit360');
/*!40000 ALTER TABLE `reply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service`
--

DROP TABLE IF EXISTS `service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `service` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `adn` varchar(10) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service`
--

LOCK TABLES `service` WRITE;
/*!40000 ALTER TABLE `service` DISABLE KEYS */;
INSERT INTO `service` VALUES (1,'memberpush','99876','2016-07-19 15:05:11','2021-04-06 09:01:04'),(3,'slypee','99876','2016-07-22 17:30:42','2021-04-06 09:01:04'),(4,'Videorazzi_register','99876','2017-03-24 15:57:43','2021-04-06 09:01:04'),(5,'reg tes','98876','2017-04-03 17:10:29','2021-04-06 09:01:04'),(6,'REG Space','99876','2017-04-03 17:23:46','2021-04-06 09:01:04'),(7,'Reg SpaceNine','99876','2017-04-03 18:01:03','2021-04-06 09:01:04'),(9,'REG Test','99876','2017-04-04 10:40:10','2021-04-06 09:01:04'),(11,'REG Test','98876','2017-04-04 10:47:32','2021-04-06 09:01:04'),(12,'Reg news','98876','2017-04-05 13:22:28','2021-04-06 09:01:04'),(13,'Reg news','9987609','2017-04-05 13:27:03','2021-04-06 09:01:04'),(15,'Reg apa','99876','2017-12-11 10:15:49','2021-04-06 09:01:04'),(16,'gold','99876','2017-12-11 10:19:18','2021-04-06 09:01:04'),(17,'latino','99876','2017-12-11 10:22:49','2021-04-06 09:01:04'),(18,'memberac','99876','2018-05-18 11:55:34','2021-04-06 09:01:04'),(19,'family cepek ','98876','2018-07-13 11:54:35','2021-04-06 09:01:04'),(20,'family cepek ','99876','2018-07-13 13:40:26','2021-04-06 09:01:04'),(22,'cepek','98876','2018-07-16 11:53:59','2021-04-06 09:01:04'),(23,'test','98876','2018-07-16 11:55:24','2021-04-06 09:01:04'),(24,'familycepek ','98876','2018-07-16 12:00:07','2021-04-06 09:01:04'),(26,'family_cepek ','99876','2018-07-16 15:09:55','2021-04-06 09:01:04'),(27,'familycepek ','99876','2018-07-16 18:48:41','2021-04-06 09:01:04'),(28,'famcepek','99876','2018-07-16 18:52:00','2021-04-06 09:01:04'),(29,'reg cepek1','99876','2018-07-18 13:02:07','2021-04-06 09:01:04'),(30,'famcepek1','99876','2018-07-18 13:04:26','2021-04-06 09:01:04'),(31,'famcepek pull','98876','2018-07-18 13:08:02','2021-04-06 09:01:04'),(32,'beli','98876','2018-07-25 17:48:54','2021-04-06 09:01:04'),(33,'nudigital','99876','2018-09-14 16:29:47','2021-04-06 09:01:04'),(34,'nudigital1','9987609','2018-09-14 16:39:07','2021-04-06 09:01:04'),(35,'nudigital1','99876','2018-09-14 16:41:34','2021-04-06 09:01:04'),(37,'Cicilan XL Non Reward','98876','2019-01-24 10:45:43','2021-04-06 09:01:04'),(42,'cicilanXL1','99876','2019-02-20 15:11:14','2021-04-06 09:01:04'),(43,'cicilanXL2','99876','2019-02-20 15:15:10','2021-04-06 09:01:04'),(44,'cicilanXL3','99876','2019-02-20 15:17:02','2021-04-06 09:01:04'),(45,'cicilanXL4','99876','2019-02-20 15:27:25','2021-04-06 09:01:04'),(46,'cicilanXL5','99876','2019-02-20 15:29:19','2021-04-06 09:01:04'),(47,'cicilanXL6','99876','2019-02-20 15:30:44','2021-04-06 09:01:04'),(48,'Yoga_For_You','99876','2019-04-01 17:57:31','2021-04-06 09:01:04'),(49,'Test','97770','2019-09-13 12:46:25','2021-04-06 09:01:04'),(52,'CERPEN','97770','2019-10-14 15:02:12','2021-04-06 09:01:04'),(53,'CERPEN_PULL','97770','2019-10-15 13:01:56','2021-04-06 09:01:04'),(55,'Latino_Pull','99876','2019-12-03 14:48:56','2021-04-06 09:01:04'),(56,'linkit-test0','97770','2019-12-16 11:33:25','2021-04-06 09:01:04'),(57,'TRIMXS','97770','2020-03-09 15:43:50','2021-04-06 09:01:04'),(58,'TRIMXS_XL','97770','2020-03-10 14:28:49','2021-04-06 09:01:04');
/*!40000 ALTER TABLE `service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_charging_mapping`
--

DROP TABLE IF EXISTS `service_charging_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `service_charging_mapping` (
  `service_id` int NOT NULL,
  `charging_id` int NOT NULL,
  PRIMARY KEY (`service_id`,`charging_id`),
  KEY `charging_id` (`charging_id`),
  CONSTRAINT `service_charging_mapping_ibfk_3` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE,
  CONSTRAINT `service_charging_mapping_ibfk_4` FOREIGN KEY (`charging_id`) REFERENCES `charging` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_charging_mapping`
--

LOCK TABLES `service_charging_mapping` WRITE;
/*!40000 ALTER TABLE `service_charging_mapping` DISABLE KEYS */;
INSERT INTO `service_charging_mapping` VALUES (1,1),(3,1),(4,1),(6,1),(7,1),(9,1),(15,1),(16,1),(17,1),(18,1),(20,1),(26,1),(27,1),(28,1),(29,1),(30,1),(33,1),(35,1),(42,1),(43,1),(44,1),(45,1),(46,1),(47,1),(48,1),(55,1),(1,2),(3,2),(4,2),(6,2),(7,2),(9,2),(15,2),(16,2),(17,2),(18,2),(20,2),(26,2),(27,2),(28,2),(29,2),(30,2),(33,2),(35,2),(42,2),(43,2),(44,2),(45,2),(46,2),(47,2),(48,2),(55,2),(1,3),(3,3),(4,3),(6,3),(7,3),(9,3),(15,3),(16,3),(17,3),(18,3),(20,3),(26,3),(27,3),(28,3),(29,3),(30,3),(33,3),(35,3),(42,3),(43,3),(44,3),(45,3),(46,3),(47,3),(48,3),(55,3),(57,3),(58,3),(1,4),(3,4),(4,4),(6,4),(7,4),(9,4),(15,4),(16,4),(17,4),(18,4),(20,4),(26,4),(27,4),(28,4),(29,4),(30,4),(33,4),(35,4),(42,4),(43,4),(44,4),(45,4),(46,4),(47,4),(48,4),(55,4),(1,5),(3,5),(5,5),(11,5),(12,5),(19,5),(22,5),(23,5),(24,5),(31,5),(32,5),(37,5),(1,6),(3,6),(4,6),(5,6),(6,6),(7,6),(9,6),(11,6),(12,6),(13,6),(15,6),(16,6),(17,6),(18,6),(20,6),(26,6),(27,6),(28,6),(29,6),(30,6),(33,6),(35,6),(42,6),(43,6),(44,6),(45,6),(46,6),(47,6),(48,6),(55,6),(1,7),(3,7),(4,7),(5,7),(6,7),(7,7),(9,7),(11,7),(12,7),(13,7),(15,7),(16,7),(17,7),(18,7),(19,7),(20,7),(22,7),(23,7),(24,7),(26,7),(27,7),(28,7),(29,7),(30,7),(31,7),(33,7),(35,7),(42,7),(43,7),(44,7),(45,7),(46,7),(47,7),(48,7),(55,7),(1,8),(3,8),(4,8),(5,8),(6,8),(7,8),(9,8),(11,8),(12,8),(13,8),(15,8),(16,8),(17,8),(18,8),(19,8),(20,8),(22,8),(23,8),(24,8),(26,8),(27,8),(28,8),(29,8),(30,8),(31,8),(32,8),(37,8),(1,9),(3,9),(4,9),(5,9),(6,9),(7,9),(9,9),(11,9),(12,9),(13,9),(15,9),(16,9),(17,9),(18,9),(19,9),(20,9),(22,9),(23,9),(24,9),(26,9),(27,9),(28,9),(29,9),(30,9),(31,9),(32,9),(37,9),(1,10),(3,10),(4,10),(5,10),(6,10),(7,10),(9,10),(11,10),(12,10),(13,10),(15,10),(16,10),(17,10),(18,10),(19,10),(20,10),(22,10),(23,10),(24,10),(26,10),(27,10),(28,10),(29,10),(30,10),(31,10),(32,10),(33,10),(34,10),(35,10),(37,10),(42,10),(43,10),(44,10),(45,10),(46,10),(47,10),(48,10),(55,10),(1,11),(3,11),(4,11),(5,11),(6,11),(7,11),(9,11),(11,11),(12,11),(13,11),(15,11),(16,11),(17,11),(18,11),(19,11),(20,11),(22,11),(23,11),(24,11),(26,11),(27,11),(28,11),(29,11),(30,11),(31,11),(32,11),(33,11),(34,11),(35,11),(37,11),(42,11),(43,11),(44,11),(45,11),(46,11),(47,11),(48,11),(49,11),(52,11),(53,11),(56,11),(57,11),(58,11),(1,12),(3,12),(4,12),(5,12),(6,12),(7,12),(9,12),(11,12),(12,12),(13,12),(15,12),(16,12),(17,12),(18,12),(19,12),(20,12),(22,12),(23,12),(24,12),(26,12),(27,12),(28,12),(29,12),(30,12),(31,12),(32,12),(33,12),(34,12),(35,12),(37,12),(42,12),(43,12),(44,12),(45,12),(46,12),(47,12),(48,12),(49,12),(52,12),(53,12),(56,12),(57,12),(58,12),(1,13),(3,13),(4,13),(5,13),(6,13),(7,13),(9,13),(11,13),(12,13),(13,13),(15,13),(16,13),(17,13),(18,13),(19,13),(20,13),(22,13),(23,13),(24,13),(26,13),(27,13),(28,13),(29,13),(30,13),(31,13),(32,13),(33,13),(34,13),(35,13),(37,13),(42,13),(43,13),(44,13),(45,13),(46,13),(47,13),(48,13),(49,13),(52,13),(53,13),(56,13),(57,13),(58,13),(1,14),(3,14),(4,14),(5,14),(6,14),(7,14),(9,14),(11,14),(12,14),(13,14),(15,14),(16,14),(17,14),(18,14),(19,14),(20,14),(22,14),(23,14),(24,14),(26,14),(27,14),(28,14),(29,14),(30,14),(31,14),(32,14),(33,14),(34,14),(35,14),(37,14),(42,14),(43,14),(44,14),(45,14),(46,14),(47,14),(48,14),(49,14),(52,14),(53,14),(56,14),(57,14),(58,14),(1,15),(3,15),(4,15),(5,15),(6,15),(7,15),(9,15),(11,15),(12,15),(13,15),(15,15),(16,15),(17,15),(18,15),(19,15),(20,15),(22,15),(23,15),(24,15),(26,15),(27,15),(28,15),(29,15),(30,15),(31,15),(32,15),(33,15),(34,15),(35,15),(37,15),(42,15),(43,15),(44,15),(45,15),(46,15),(47,15),(48,15),(49,15),(52,15),(53,15),(55,15),(1,16),(3,16),(4,16),(5,16),(6,16),(7,16),(9,16),(11,16),(12,16),(13,16),(15,16),(16,16),(17,16),(18,16),(19,16),(20,16),(22,16),(23,16),(24,16),(26,16),(27,16),(28,16),(29,16),(30,16),(31,16),(32,16),(33,16),(34,16),(35,16),(37,16),(42,16),(43,16),(44,16),(45,16),(46,16),(47,16),(48,16),(49,16),(52,16),(53,16),(1,17),(3,17),(4,17),(5,17),(6,17),(7,17),(9,17),(11,17),(12,17),(13,17),(15,17),(16,17),(17,17),(18,17),(19,17),(20,17),(22,17),(23,17),(24,17),(26,17),(27,17),(28,17),(29,17),(30,17),(31,17),(32,17),(33,17),(34,17),(35,17),(37,17),(42,17),(43,17),(44,17),(45,17),(46,17),(47,17),(48,17),(49,17),(52,17),(53,17),(55,17),(56,17),(57,17),(58,17),(1,18),(3,18),(4,18),(5,18),(6,18),(7,18),(9,18),(11,18),(12,18),(13,18),(15,18),(16,18),(17,18),(18,18),(19,18),(20,18),(22,18),(23,18),(24,18),(26,18),(27,18),(28,18),(29,18),(30,18),(31,18),(32,18),(33,18),(34,18),(35,18),(37,18),(42,18),(43,18),(44,18),(45,18),(46,18),(47,18),(48,18),(49,18),(52,18),(53,18),(55,18),(56,18),(57,18),(58,18),(1,19),(3,19),(4,19),(5,19),(6,19),(7,19),(9,19),(11,19),(12,19),(13,19),(15,19),(16,19),(17,19),(18,19),(19,19),(20,19),(22,19),(23,19),(24,19),(26,19),(27,19),(28,19),(29,19),(30,19),(31,19),(32,19),(33,19),(34,19),(35,19),(37,19),(42,19),(43,19),(44,19),(45,19),(46,19),(47,19),(48,19),(49,19),(52,19),(53,19),(55,19),(56,19),(57,19),(58,19),(1,20),(3,20),(4,20),(5,20),(6,20),(7,20),(9,20),(11,20),(12,20),(13,20),(15,20),(16,20),(17,20),(18,20),(19,20),(20,20),(22,20),(23,20),(24,20),(26,20),(27,20),(28,20),(29,20),(30,20),(31,20),(32,20),(33,20),(34,20),(35,20),(37,20),(42,20),(43,20),(44,20),(45,20),(46,20),(47,20),(48,20),(49,20),(52,20),(53,20),(55,20),(56,20),(57,20),(58,20),(1,21),(3,21),(4,21),(5,21),(6,21),(7,21),(9,21),(11,21),(12,21),(13,21),(15,21),(16,21),(17,21),(18,21),(19,21),(20,21),(22,21),(23,21),(24,21),(26,21),(27,21),(28,21),(29,21),(30,21),(31,21),(32,21),(33,21),(34,21),(35,21),(37,21),(42,21),(43,21),(44,21),(45,21),(46,21),(47,21),(48,21),(49,21),(52,21),(53,21),(55,21),(56,21),(57,21),(58,21),(1,22),(3,22),(4,22),(5,22),(6,22),(7,22),(9,22),(11,22),(12,22),(13,22),(15,22),(16,22),(17,22),(18,22),(19,22),(20,22),(22,22),(23,22),(24,22),(26,22),(27,22),(28,22),(29,22),(30,22),(31,22),(32,22),(33,22),(34,22),(35,22),(37,22),(42,22),(43,22),(44,22),(45,22),(46,22),(47,22),(48,22),(49,22),(52,22),(53,22),(55,22),(56,22),(57,22),(58,22),(1,23),(3,23),(4,23),(5,23),(6,23),(7,23),(9,23),(11,23),(12,23),(13,23),(15,23),(16,23),(17,23),(18,23),(19,23),(20,23),(22,23),(23,23),(24,23),(26,23),(27,23),(28,23),(29,23),(30,23),(31,23),(32,23),(33,23),(34,23),(35,23),(37,23),(42,23),(43,23),(44,23),(45,23),(46,23),(47,23),(48,23),(49,23),(52,23),(53,23),(55,23),(56,23),(57,23),(58,23),(1,29),(3,29),(4,29),(5,29),(6,29),(7,29),(9,29),(11,29),(12,29),(13,29),(15,29),(16,29),(17,29),(18,29),(19,29),(20,29),(22,29),(23,29),(24,29),(26,29),(27,29),(28,29),(29,29),(30,29),(31,29),(32,29),(33,29),(34,29),(35,29),(37,29),(42,29),(43,29),(44,29),(45,29),(46,29),(47,29),(48,29),(49,29),(52,29),(53,29),(55,29),(56,29),(57,29),(58,29);
/*!40000 ALTER TABLE `service_charging_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscription`
--

DROP TABLE IF EXISTS `subscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `subscription` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `transaction_id_subscribe` varchar(64) DEFAULT NULL,
  `transaction_id_unsubscribe` varchar(64) DEFAULT NULL,
  `msisdn` bigint NOT NULL,
  `service` varchar(64) NOT NULL,
  `adn` varchar(8) NOT NULL,
  `operator` varchar(32) NOT NULL,
  `channel_subscribe` varchar(16) DEFAULT NULL,
  `channel_unsubscribe` varchar(16) DEFAULT NULL,
  `subscribed_from` datetime NOT NULL,
  `subscribed_until` datetime NOT NULL,
  `partner` varchar(20) NOT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `time_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `time_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `msisdn` (`msisdn`) USING BTREE,
  KEY `service` (`service`) USING BTREE,
  KEY `short_code` (`adn`) USING BTREE,
  KEY `operator` (`operator`) USING BTREE,
  KEY `active` (`active`) USING BTREE,
  KEY `populate_dp` (`service`,`adn`,`operator`,`active`,`subscribed_from`,`time_created`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscription`
--

LOCK TABLES `subscription` WRITE;
/*!40000 ALTER TABLE `subscription` DISABLE KEYS */;
/*!40000 ALTER TABLE `subscription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_msgtransact`
--

DROP TABLE IF EXISTS `tbl_msgtransact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_msgtransact` (
  `ID` bigint unsigned NOT NULL AUTO_INCREMENT,
  `IN_REPLY_TO` bigint NOT NULL DEFAULT '0',
  `MSGINDEX` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `MSGTIMESTAMP` datetime DEFAULT NULL,
  `ADN` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `MSISDN` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `OPERATORID` int NOT NULL,
  `MSGDATA` varchar(160) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `MSGLASTSTATUS` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `MSGSTATUS` varchar(25) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `CLOSEREASON` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `SERVICEID` varchar(41) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `MEDIA` varchar(12) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `CHANNEL` varchar(12) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `SERVICE` varchar(25) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `PARTNER` varchar(25) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `SUBJECT` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `PRICE` float NOT NULL,
  `ISR` tinyint NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `IX_TBL_MSGTRANSACT` (`MSGINDEX`,`MSGLASTSTATUS`),
  KEY `waktu` (`MSGTIMESTAMP`),
  KEY `from` (`ADN`),
  KEY `listview` (`SERVICE`,`MSGTIMESTAMP`),
  KEY `service` (`MEDIA`,`MSGTIMESTAMP`),
  KEY `IAC` (`ISR`),
  KEY `MSGLASTSTATUS` (`MSGLASTSTATUS`),
  KEY `MSGSTATUS` (`MSGSTATUS`),
  KEY `IN_REPLY_TO` (`IN_REPLY_TO`),
  KEY `PARTNER` (`PARTNER`),
  KEY `SUBJECT` (`SUBJECT`,`MSGTIMESTAMP`,`MSGSTATUS`,`SERVICE`,`PARTNER`),
  KEY `msgto` (`MSISDN`,`ADN`,`MSGTIMESTAMP`),
  KEY `MSGDATA` (`MSGDATA`,`SERVICE`,`MSGTIMESTAMP`,`SUBJECT`,`MSGSTATUS`),
  KEY `CS_SEARCH` (`ADN`,`MSISDN`,`SERVICE`,`MSGTIMESTAMP`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_msgtransact`
--

LOCK TABLES `tbl_msgtransact` WRITE;
/*!40000 ALTER TABLE `tbl_msgtransact` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_msgtransact` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-06  9:42:28
