LINKIT-H3I
======

Installation
======

yum install -y gitlab-ce

Git global setup
======

1. git config --global user.name "Wilie Wahyu Hidayat"
2. git config --global user.email "wilie.hidayat87@gmail.com"

Link a SSH key to server
======

Check : cat ~/.ssh/id_rsa.pub

Add : ssh-keygen -t rsa -C "wilie.hidayat87@gmail.com"

Create a new repository
======

git clone git@gitlab.com:wilie.hidayat87/linkit-xldm.git

cd /

touch README.md

git add README.md

git commit -m "add README"

git push -u origin master


Push an existing folder
======

cd /

git init

git remote add origin git@gitlab.com:wilie.hidayat87/linkit-xldm.git

1. git add /app/xmp2012/interface/default/.
2. git add /app/xmp2012/interface/xlsdp/bin/.
3. git add /app/xmp2012/interface/xlsdp/broadcast/.
4. git add /app/xmp2012/interface/xlsdp/cmp/.
5. git add /app/xmp2012/interface/xlsdp/config/.
6. git add /app/xmp2012/interface/xlsdp/dr/.
7. git add /app/xmp2012/interface/xlsdp/mo/.
8. git add /app/xmp2012/interface/xlsdp/mt/.
9. git add /app/xmp2012/interface/xlsdp/service/.
10. git add /app/xmp2012/interface/xlsdp/xmp.php
11. git add /app/xmp2012/interface/xlsdp/www/cpatools/.
12. git add /app/xmp2012/interface/xlsdp/www/data/.
13. git add /app/xmp2012/interface/xlsdp/www/dr/.
14. git add /app/xmp2012/interface/xlsdp/www/mo/.
15. git add /app/xmp2012/system/core/.
16. git add /app/xmp2012/DailysummarizerProcess.sh
17. git add /app/xmp2012/summarizerProcess.sh

git commit -m "Initial commit"

git push -u origin master


Push an existing Git repository
======

cd /

git remote rename origin old-origin

git remote add origin git@gitlab.com:wilie.hidayat87/linkit-xldm.git

git push -u origin --all

git push -u origin --tags


Pull repo to production
======

git remote add origin git@gitlab.com:wilie.hidayat87/linkit-xldm.git

git fetch origin

git status

git pull -u origin master

Revert pull request
======

git revert <commit sha1>

Connect to database
======
docker exec -it <container-id> /bin/bash
mysql -uxldm -p'xldm' -dxmp -P3307

Get WSL Ip
======
ip addr | grep eth0

Get Local Address
======
cat /etc/resolv.conf