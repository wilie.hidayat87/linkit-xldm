-- MySQL dump 10.13  Distrib 8.0.23, for Linux (x86_64)
--
-- Host: localhost    Database: cmp
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `hmo`
--

DROP TABLE IF EXISTS `hmo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hmo` (
  `id` int NOT NULL AUTO_INCREMENT,
  `hset_id` int NOT NULL,
  `msisdn` varchar(20) NOT NULL,
  `hash` varchar(255) DEFAULT '',
  `date_send` date DEFAULT NULL,
  `time_send` time DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `closereason` varchar(100) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `hset_id` (`hset_id`,`date_send`,`status`),
  KEY `waktu` (`date_send`),
  KEY `hash` (`hash`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hmo`
--

LOCK TABLES `hmo` WRITE;
/*!40000 ALTER TABLE `hmo` DISABLE KEYS */;
/*!40000 ALTER TABLE `hmo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hset`
--

DROP TABLE IF EXISTS `hset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hset` (
  `id` int NOT NULL AUTO_INCREMENT,
  `counter` tinyint(1) NOT NULL DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `keyword` varchar(50) NOT NULL DEFAULT '',
  `service` varchar(20) NOT NULL DEFAULT '',
  `adn` int NOT NULL DEFAULT '0',
  `operator_name` varchar(20) NOT NULL DEFAULT '',
  `handler` text,
  `api_url` text,
  `params` text NOT NULL,
  `send_timeout` int NOT NULL DEFAULT '10',
  `inc` int DEFAULT '0',
  `send` int DEFAULT '0',
  `receive` int DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `keyword` (`keyword`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hset`
--

LOCK TABLES `hset` WRITE;
/*!40000 ALTER TABLE `hset` DISABLE KEYS */;
/*!40000 ALTER TABLE `hset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pixel_storage`
--

DROP TABLE IF EXISTS `pixel_storage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pixel_storage` (
  `id` int NOT NULL AUTO_INCREMENT,
  `pixel` text,
  `operator` varchar(50) DEFAULT NULL,
  `servicename` varchar(100) DEFAULT NULL,
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_used` int DEFAULT '0',
  `partner` varchar(50) DEFAULT NULL,
  `msisdn` varchar(100) DEFAULT NULL,
  `adn` varchar(30) DEFAULT NULL,
  `channel` varchar(30) DEFAULT NULL,
  `campurl` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pixel_storage`
--

LOCK TABLES `pixel_storage` WRITE;
/*!40000 ALTER TABLE `pixel_storage` DISABLE KEYS */;
/*!40000 ALTER TABLE `pixel_storage` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-06  9:41:33
