<?php
class quiz_base{
	
	private static $instance;
	
	const REGISTER = 1;
	
	const PUSH = 2;
	
	const QUESTION = 3;
		
	public function getInstance() {
        $log = manager_logging::getInstance ();
        $log->write(array('level' => 'debug', 'message' => 'Start'));
        
		if (! self::$instance)
			self::$instance = new self ();
		
		return self::$instance;
	}

	protected function setOwner($owner){		
		
	}
	protected function setQuiz($quiz){
	
	}

	public function setTransaction($transactionData){
        $log = manager_logging::getInstance ();
        $log->write(array('level' => 'debug', 'message' => 'Start'));
        
		$quiz = loader_model::getInstance()->load ('quiztransaction', 'connDatabase1');
		$transactionId = $quiz->insertTransaction($transactionData);	
		return $transactionId;
	}
	
	public function getWinner($ownerId,$quizId,$year,$month,$week,$periodType){
        $log = manager_logging::getInstance ();
        $log->write(array('level' => 'debug', 'message' => 'Start'));
        
		$quiz = loader_model::getInstance()->load ('quizwinner', 'connDatabase1');
		$winner = $quiz->getWinner($ownerId,$quizId,$year,$month,$week,$periodType);	
		return $winner;	
	}
	
	public function getMonthlyWinner($ownerId,$quizId,$year,$month,$periodType){
        $log = manager_logging::getInstance ();
        $log->write(array('level' => 'debug', 'message' => 'Start'));
        
		$quiz = loader_model::getInstance()->load ('quizwinner', 'connDatabase1');
		$winner = $quiz->getMonthlyWinner($ownerId,$quizId,$year,$month,$periodType);	
		return $winner;	
	}
	
	public function getScore($quizId, $msisdn){		
        $log = manager_logging::getInstance ();
        $log->write(array('level' => 'debug', 'message' => 'Start'));
        
		$transaction = loader_model::getInstance()->load ('quiztransaction', 'connDatabase1');
		$subscriberScore = $transaction->getSubscriberScore($quizId,$msisdn);
		return $subscriberScore;
	}
	
	public function getMonthlyScore($date){		
        $log = manager_logging::getInstance ();
        $log->write(array('level' => 'debug', 'message' => 'Start'));
        
		$transaction = loader_model::getInstance()->load ('quiztransaction', 'connDatabase1');
		$monthlyScore = $transaction->getMonthlyPoint($date);
		return $monthlyScore;
	}
	
	public function getWeeklyScore($weekDay,$msisdn){		
        $log = manager_logging::getInstance ();
        $log->write(array('level' => 'debug', 'message' => 'Start'));
        
		$transaction = loader_model::getInstance()->load ('quiztransaction', 'connDatabase1');
		$weeklyScore = $transaction->getWeeklyPoint($weekDay,$msisdn);
		return $weeklyScore;
	}
	
	public function getPrizeInfo(){
        $log = manager_logging::getInstance ();
        $log->write(array('level' => 'debug', 'message' => 'Start'));
        
		$quizPrize = loader_model::getInstance()->load ('quizprize', 'connDatabase1');
		$prizeData = $quizPrize->getPrize();
		return $prizeData;	
	}
	
	public function getOwnerByName($name){
        $log = manager_logging::getInstance ();
        $log->write(array('level' => 'debug', 'message' => 'Start'));
        
		$quizOwner = loader_model::getInstance()->load ('quizowner', 'connDatabase1');
		$ownerData = $quizOwner->getOwnerByName($name);
		return $ownerData;	
	}

	public function getQuiz($ownerId,$quizName){
        $log = manager_logging::getInstance ();
        $log->write(array('level' => 'debug', 'message' => 'Start'));
        
		$quiz = loader_model::getInstance()->load ('quizquiz', 'connDatabase1');
		$quizData = $quiz->getQuizData($ownerId,$quizName);
		return $quizData;	
	}

	public function getQuizIdByName($quizName){
        $log = manager_logging::getInstance ();
        $log->write(array('level' => 'debug', 'message' => 'Start'));
        
		$quiz = loader_model::getInstance()->load ('quizquiz', 'connDatabase1');
		$quizData = $quiz->getQuizIdByName($quizName);
		return $quizData;	
	}

	public function getLastTransaction($msisdn){
        $log = manager_logging::getInstance ();
        $log->write(array('level' => 'debug', 'message' => 'Start'));
        
		$transaction = loader_model::getInstance()->load ('quiztransaction', 'connDatabase1');
		$transactionData = $transaction->getLastQuestionTransaction($msisdn);	
		return $transactionData;
	}

	public function getQuestionById($questionId){
        $log = manager_logging::getInstance ();
        $log->write(array('level' => 'debug', 'message' => 'Start'));
        
		$question = loader_model::getInstance()->load ('quizquestion', 'connDatabase1');
		$questionData = $question->getQuestionById($questionId);	
		return $questionData;
	}
	
	public function getNextQuestion($ownerId,$sessionId,$questionId){
        $log = manager_logging::getInstance ();
        $log->write(array('level' => 'debug', 'message' => 'Start'));
        
		$quiz = loader_model::getInstance()->load ('quizquestion', 'connDatabase1');
		$quizQuestion = $quiz->getNextQuestion($ownerId,$sessionId,$questionId);
		return $quizQuestion;	
	}
	
	public function getQuestionAnswer($questionId){
        $log = manager_logging::getInstance ();
        $log->write(array('level' => 'debug', 'message' => 'Start'));
        
		$quiz = loader_model::getInstance()->load ('quizanswer', 'connDatabase1');
		$quizQuestion = $quiz->getAnswerByQuestion($questionId);
		return $quizQuestion;	
	}
	
	public function getQuestionSent($msisdn,$sessionId){
        $log = manager_logging::getInstance ();
        $log->write(array('level' => 'debug', 'message' => 'Start'));
        
		$quiz = loader_model::getInstance()->load ('quizquestion', 'connDatabase1');
		$maxQuestion = $quiz->getQuestionSent($msisdn,$sessionId);
		return $maxQuestion;	
	}
	
	public function getSession($ownerId,$quizId){
        $log = manager_logging::getInstance ();
        $log->write(array('level' => 'debug', 'message' => 'Start'));
        
		$quiz = loader_model::getInstance()->load ('quizsession', 'connDatabase1');
		$quizSession = $quiz->getSessionActive($ownerId,$quizId);
		return $quizSession;	
	}
	
	public function getMaxQuestion($sessionId){
        $log = manager_logging::getInstance ();
        $log->write(array('level' => 'debug', 'message' => 'Start'));
        
		$quiz = loader_model::getInstance()->load ('quizsession', 'connDatabase1');
		$maxQuestion = $quiz->getMaxQuestion($sessionId);
		return $maxQuestion;	
	}
	
	public function getPoint($questionId,$answerState){
        $log = manager_logging::getInstance ();
        $log->write(array('level' => 'debug', 'message' => 'Start'));
        
		$quiz = loader_model::getInstance()->load ('quizanswer', 'connDatabase1');
		$point = $quiz->getPoint($questionId,$answerState);
		return $point;	
	}

	protected function checkAnswer($questionId,$answer){	
        $log = manager_logging::getInstance ();
        $log->write(array('level' => 'debug', 'message' => 'Start'));
        	
		$quizAnswer = loader_model::getInstance()->load ('quizanswer', 'connDatabase1');
		$answerData = $quizAnswer->getAnswerByQuestion($questionId);
		$answer = (strtoupper($answer));
		
		$userAnswerState = ((ord(($answer))) - 65);
		
		if($answerData[$userAnswerState]['answer_state'] == '1')
		{
			return true;
		}
		else 
		{
			return false;
		}
		
	}

	protected function getAnswerPoint($questionId,$answer){	
        $log = manager_logging::getInstance ();
        $log->write(array('level' => 'debug', 'message' => 'Start'));
        	
		$quizAnswer = loader_model::getInstance()->load ('quizanswer', 'connDatabase1');
		$answerData = $quizAnswer->getAnswerByQuestion($questionId);
		$answer = (strtoupper($answer));
		
		$userAnswerState = ((ord(($answer))) - 65);
		
		return $answerData[$userAnswerState]['point'];
		
	}	

	public function updateSentCounter($transactionId,$point,$sentCounter){	
        $log = manager_logging::getInstance ();
        $log->write(array('level' => 'debug', 'message' => 'Start'));
        
		$user = loader_model::getInstance()->load ('quiztransaction', 'connDatabase1');
		$update = $user->updateSentCounter($transactionId,$point,$sentCounter);	
		return $update;		
	}
	
	public function process($mo_data){
        $log = manager_logging::getInstance ();
        $log->write(array('level' => 'debug', 'message' => 'Start'));
        
		$load_config = loader_config::getInstance ();
		$config_quiz = $load_config->getConfig ( 'quiz' );
		
		$profile = 'super'; 
		$ownerName = $config_quiz->profile [$profile] ['owner'];
		$quizName = $config_quiz->profile [$profile] ['quiz'];		
		
		$owner		  = $this -> getOwnerByName($ownerName);		
		$quiz		  = $this -> getQuiz($owner['id'] , $quizName);
		$session	  = $this -> getSession($owner['id'] , $quiz['id']);
		$question	  = $this -> getQuestion($owner['id'] , $session['id']);	
								
		$lastTransaction = $this -> getLastTransaction($mo_data->msisdn);		
		
		$extractSMS = explode(" ", $mo_data->rawSMS);
		
		$checkAnswer =  $this->checkAnswer($lastTransaction['transaction_ref'],$extractSMS['1']);
		$answerPoint =  $this->getAnswerPoint($lastTransaction['transaction_ref'],$extractSMS['1']);
		
		$msgData = '';  
			
		$maxQuestion = $this -> getMaxQuestion($session['id']);
		$questionSent = $this -> getQuestionSent($mo_data->msisdn,$session['id']);
		
		
		if($checkAnswer == false)
		{	
			
			$lastQuestion = $this->getQuestionById($lastTransaction['transaction_ref']);

			$questionAnswer	  = $this -> getQuestionAnswer($lastQuestion['id']);
			
			$rightPoint = $this -> getPoint($lastQuestion['id'],1);
			$wrongPoint = $this -> getPoint($lastQuestion['id'],2);
			
			$transaction = loader_data::get ( 'quiz_transaction' );	
		
			$transaction->ownerId = $owner['id'];
			$transaction->quizId = $quiz['id'];
			$transaction->msisdn = $mo_data->msisdn;
			$transaction->transactionType = $this::QUESTION;
			$transaction->transactionRef = $lastQuestion['id'];
			$transaction->message = 'Question-'.$lastQuestion['id'];
			$transaction->point = 0;
			if($session['special_session'] == 0)
			{ $point = $answerPoint;	}
			else if($session['special_session'] == 1)
			{ $point = $session['multiplier']*$answerPoint;	}
			$transaction->dateSend = date("Y-m-d H:i:s");
			$transaction->dateRespond = date("Y-m-d H:i:s");
			$transaction->questionSentCounter = 0;
				
			$ini_type = 'wrong_answer';	
			
			$this -> updateSentCounter($lastTransaction['id'],$point,0);			
			
			$msgData .= strtoupper($quizName).':#break';
			
			if($questionSent['total_question_send'] > $maxQuestion['max_allowed_question'])
			{
				$msgData .= 'Anda SALAH,POINT+'.$point.' #break';
			}
			else{
				$msgData .= 'Anda SALAH,POINT+'.$point.' #break'.$lastQuestion['question'].' #break';	
				$ascii = 65;
				foreach($questionAnswer as $answerData){
					$charAnswer=chr($ascii);
					$msgData .= $charAnswer.'. '.$answerData['answer'].' #break';
					$ascii++;				
				}
			
				if($session['special_session'] == 1){$msgData .= 'Kirim MUSIK A/B ke 2112. Benar +'.$rightPoint['point']*$session['multiplier'].', salah +'.$wrongPoint['point']*$session['multiplier'].'.';}
				else{$msgData .= 'Kirim MUSIK A/B ke 2112. Benar +'.$rightPoint['point'].', salah +'.$wrongPoint['point'].'.';}
				
				$lastTransactId = $this -> setTransaction($transaction);		
				
			}
					
		 	$mt_subject = strtoupper ( 'MT;PULL;' . $mo_data->channel . ';TEXT' );									
			$mt = $this->createMT($mo_data,$ini_type,$mt_subject,$msgData);
		
		}		
		
		else
		{					
			$nextQuestion	  = $this -> getNextQuestion($owner['id'] , $session['id'], $lastTransaction['transaction_ref']);
			$questionAnswer	  = $this -> getQuestionAnswer($nextQuestion['id']);	
			$ini_type = 'right_answer';		
	
			$rightPoint = $this -> getPoint($nextQuestion['id'],1);
			$wrongPoint = $this -> getPoint($nextQuestion['id'],2);		
			
			$transaction = loader_data::get ( 'quiz_transaction' );	
		
			$transaction->ownerId = $owner['id'];
			$transaction->quizId = $quiz['id'];
			$transaction->msisdn = $mo_data->msisdn;
			$transaction->transactionType = $this::QUESTION;
			$transaction->transactionRef = $nextQuestion['id'];
			$transaction->message = 'Question-'.$nextQuestion['id'];
			$transaction->point = 0;
			if($session['special_session'] == 0)
			{ $point = $answerPoint;	}
			else if($session['special_session'] == 1)
			{ $point = $session['multiplier']*$answerPoint;	}
			$transaction->dateSend = date("Y-m-d H:i:s");
			$transaction->dateRespond = date("Y-m-d H:i:s");
			$transaction->questionSentCounter = 0;
			
			$this -> updateSentCounter($lastTransaction['id'],$point,1);
			$maxQuestion = $this -> getMaxQuestion($session['id']);
			$questionSent = $this -> getQuestionSent($mo_data->msisdn,$session['id']);
			
			$msgData .= strtoupper($quizName).':#break';
					
			if($questionSent['total_question_send'] == $maxQuestion['max_allowed_question'])
			{
				$msgData .= 'Anda BENAR,POINT+'.$point.' #break';
			}
			else{
				$msgData .= 'Anda BENAR,POINT+'.$point.' #break'.$nextQuestion['question'].' #break';	
				$ascii = 65;
				foreach($questionAnswer as $answerData){
					$charAnswer=chr($ascii);
					$msgData .= $charAnswer.'. '.$answerData['answer'].' #break';
					$ascii++;				
				}
			
				if($session['special_session'] == 1){$msgData .= 'Kirim MUSIK A/B ke 2112. Benar +'.$rightPoint['point']*$session['multiplier'].', salah +'.$wrongPoint['point']*$session['multiplier'].'.';}
				else{$msgData .= 'Kirim MUSIK A/B ke 2112. Benar +'.$rightPoint['point'].', salah +'.$wrongPoint['point'].'.';}

				
				$lastTransactId = $this -> setTransaction($transaction);
								
			}			
			
						
			$mt_subject = strtoupper ( 'MT;PULL;' . $mo_data->channel . ';TEXT' );
			$mt = $this->createMT($mo_data,$ini_type,$mt_subject,$msgData);
					
		
		}
		
		return array ($mt);
	
	}
	
	public function createMTold($mo_data,$ini_type,$msgData=''){
		
		$service_config = loader_config::getInstance ()->getConfig ( 'service' );
		$ini_reader = ini_reader::getInstance ();
		$ini_data = loader_data::get ( 'ini' );
		$ini_data->file = $service_config->iniPath;
		$ini_data->type = $ini_type;
		
		$mt = loader_data::get ( 'mt' );
		$mt->inReply = $mo_data->id;
		$mt->msgId = $mo_data->msgId;
		$mt->adn = $mo_data->adn;
		
		if($msgData != '')
			{	
				$mt->msgData = $msgData; 
				$mt->msgData = str_replace("#break", "\n", $mt->msgData);
			}
		else 	
			{
				$ini_data->section = 'REPLY';					
				$mt->msgData = $ini_reader->get ( $ini_data ); 
				$mt->msgData = str_replace("#break", "\n", $mt->msgData);
			}
		$ini_data->section = 'CHARGING';
		$mt->price = $ini_reader->get ( $ini_data );
		$mt->operatorId = $mo_data->operatorId;
		$mt->operatorName = $mo_data->operatorName;
		$mt->service = $mo_data->service;
		$mt->subject = strtoupper ( 'MT;PULL;' . $mo_data->channel . ';' );
		$mt->msisdn = $mo_data->msisdn;
		$mt->channel = $mo_data->channel;
		$mt->type = 'text';
		$mt->mo = $mo_data;
		
		return $mt;	
	}
	
	public function createMT($mo_data,$ini_type,$mt_subject='',$msgData=''){
        $log = manager_logging::getInstance ();
        $log->write(array('level' => 'debug', 'message' => 'Start'));
        
		
		$service_config = loader_config::getInstance ()->getConfig ( 'service' );
		$ini_reader = ini_reader::getInstance ();
		$ini_data = loader_data::get ( 'ini' );
		$ini_data->file = $service_config->iniPath;
		$ini_data->type = $ini_type;
		
		$mt = loader_data::get ( 'mt' );
		$mt->inReply = $mo_data->id;
		$mt->msgId = $mo_data->msgId;
		$mt->adn = $mo_data->adn;
		
		if($msgData != '')
			{	
				$mt->msgData = $msgData; 
				$mt->msgData = str_replace("#break", "\n", $mt->msgData);
			}
		else 	
			{
				$ini_data->section = 'REPLY';					
				$mt->msgData = $ini_reader->get ( $ini_data ); 
				$mt->msgData = str_replace("#break", "\n", $mt->msgData);
			}
		$ini_data->section = 'CHARGING';
		$mt->price = $ini_reader->get ( $ini_data );
		$mt->operatorId = $mo_data->operatorId;
		$mt->operatorName = $mo_data->operatorName;
		$mt->service = $mo_data->service;
		if($mt_subject != '')
			{	
				$mt->subject = $mt_subject; 
			}
		else 	
			{
				$mt->subject = strtoupper ( 'MT;PULL;' . $mo_data->channel . ';' );
			}		
		$mt->msisdn = $mo_data->msisdn;
		$mt->channel = $mo_data->channel;
		$mt->type = 'mtpull';
		$mt->mo = $mo_data;
		
		return $mt;	
	}

}