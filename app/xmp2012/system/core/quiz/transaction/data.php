<?php
class quiz_transaction_data{
	public $id;
	public $ownerId;
	public $quizId;
	public $msisdn;
	public $transactionType;
	public $transactionRef;
	public $message;
	public $point;
	public $dateSend;
	public $dateRespond;
	public $questionSentCounter;
}