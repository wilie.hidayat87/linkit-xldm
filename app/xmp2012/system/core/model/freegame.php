<?php 
class model_freegame extends model_base {
	
	public function saveDLSession($data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start writing session"));
        
		$sql = sprintf("
		INSERT INTO wap_session
			(Token, InitialChargeType, SiteID, Service, Operator, Msisdn, `Status`, `Limit`, DateCreated, DateModified)
		VALUES
			('%s', '%s', '%s', '%s', '%s', '%s', '1', '%s', now(), now())
		", 
			mysql_real_escape_string($data->token), 
			mysql_real_escape_string($data->initialcharge), 
			mysql_real_escape_string($data->siteId), 
			mysql_real_escape_string($data->service), 
			mysql_real_escape_string($data->operator), 
			mysql_real_escape_string($data->msisdn), 
			mysql_real_escape_string($data->limit)
		);
		
        $query = $this->databaseObj->query($sql);
        $insert_id = $this->databaseObj->last_insert_id(); 

        return $insert_id;
	}

	public function isExist($code) {
		
        $sql = sprintf(
        	"SELECT COUNT(content_code) AS total FROM c_main WHERE content_code = '%s'", 
        	mysql_real_escape_string($code)        
        );


        $result = $this->databaseObj->fetch($sql);


        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Check game code : ". $code. ' '. $result));        

	//var_dump($sql,  $result);
        
        return $result[0]['total'];
        
	}
	
}


?>
