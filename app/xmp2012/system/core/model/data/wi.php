<?php
class model_data_wi {
    public $id;
    public $reqid;
    public $instid;
    public $first_installment;
    public $next_installment;
    public $first_installment_date;
    public $last_installment_date;
    public $next_installment_date;
    public $installment_count;
    public $service_code;
    public $content_price;
    public $content_code;
    public $query_string;
    public $msisdn;
    public $operator_id;
    public $service_id;
    public $date_created;
    public $date_modified;
}
