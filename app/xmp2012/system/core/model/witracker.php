<?php

class model_data_witracker {
    public $id;
    public $msisdn;
    public $reqid;
    public $status;
    public $trxid;
    public $instid;
    public $scode;
    public $created;
    public $ccode;
    public $operator_id;
    public $atype;
    public $inst_no;
}
