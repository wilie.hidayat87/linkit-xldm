<?php

class model_schedule extends model_base {

    public function get(broadcast_data $data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($data)));

        $sql = sprintf("SELECT * FROM dbpush.push_schedule WHERE push_time < NOW() AND operator = '%s' AND status = '%s';", mysql_real_escape_string($data->operator), mysql_real_escape_string($data->status));
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data;
        } else {
            return false;
        }
    }

    public function getall(broadcast_data $data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($data)));

        $services = "";
        foreach($data->service as $service)
        {
            $services .= "'".$service."',";
        }
        $services = rtrim($services, ",");

        $sql = sprintf("SELECT * FROM dbpush.push_schedule WHERE service IN (".$services.") AND status = %d;", mysql_real_escape_string($data->status));
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data;
        } else {
            return false;
        }
    }

	public function scheduleCheck(broadcast_data $data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($data)));

        $sql = sprintf("SELECT * FROM dbpush.push_schedule_check WHERE date(created) = current_date and service = '%s' and operator = '%s' and adn = '%s' and price = '%s';", mysql_real_escape_string($data->service), mysql_real_escape_string($data->operator), mysql_real_escape_string($data->adn), mysql_real_escape_string($data->price));
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return true;
        } else {
            return false;
        }
    }
	
	public function insertToCheckingScheduleTbl(broadcast_data $data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($data)));

        $sql = sprintf("INSERT INTO dbpush.push_schedule_check VALUE ('','%s','%s','%s','%s',now())", mysql_real_escape_string($data->service), mysql_real_escape_string($data->operator), mysql_real_escape_string($data->adn), mysql_real_escape_string($data->price));
        return $this->databaseObj->query($sql);
    }
	
    public function setStatus(broadcast_data $data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($data)));

        $sql = sprintf("UPDATE dbpush.push_schedule SET `status` = '%s', `modified` = NOW()", mysql_real_escape_string($data->status));
        if (!empty($data->id)) {
            $sql .= sprintf(" WHERE id = '%s'", mysql_real_escape_string($data->id));
        }
        return $this->databaseObj->query($sql);
    }

    public function reset(broadcast_data $data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($data)));

        if ($data->recurringType == 1)
            $interval = 1;
        if ($data->recurringType == 2)
            $interval = 7;
        $sql = "UPDATE push_schedule SET `status` = '0', `modified` = NOW()";
        if (!empty($interval))
            $sql .= sprintf(", push_time = DATE_ADD(push_time, INTERVAL %s DAY)", mysql_real_escape_string($interval));

        $sql .= sprintf(" WHERE id = '%s' AND service NOT IN ('cicilanXL1','cicilanXL2','cicilanXL3','cicilanXL4','cicilanXL5','cicilanXL6','cicilanXL7')", mysql_real_escape_string($data->id));

        return $this->databaseObj->query($sql);
    }

    public function activeSchedule(broadcast_data $data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($data)));

        $sql = sprintf("UPDATE dbpush.push_schedule SET `status` = '0', push_time = date_add(push_time, interval 1 day), `modified` = NOW() WHERE id = %d", mysql_real_escape_string($data->id));
        return $this->databaseObj->query($sql);
    }
}
