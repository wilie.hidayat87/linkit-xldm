<?php

class model_user extends model_base {

    public function populateUser($broadcast_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($broadcast_data)));

        $sql = sprintf("SELECT * 
				FROM subscription 
				WHERE service = '%s' 
				AND adn = '%s' 
				AND operator = '%s' 
				AND active = '1'
        		AND subscribed_from <= NOW() 
				AND subscribed_until >= NOW() 
        		", mysql_real_escape_string($broadcast_data->service), mysql_real_escape_string($broadcast_data->adn), mysql_real_escape_string($broadcast_data->operator));
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data;
        } else {
            return false;
        }
    }

    public function get($user_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($user_data)));

        $sql = "SELECT * FROM subscription WHERE ";
        if (!empty($user_data->service))
            $sql .= " service = '" . mysql_real_escape_string($user_data->service) . "' AND";

        if (!empty($user_data->adn))
            $sql .= " adn = '" . mysql_real_escape_string($user_data->adn) . "' AND";

        if (!empty($user_data->operator))
            $sql .= " operator = '" . mysql_real_escape_string($user_data->operator) . "' AND";

        if (!empty($user_data->active))
            $sql .= " active = '" . mysql_real_escape_string($user_data->active) . "' AND";

        if (!empty($user_data->msisdn))
            $sql .= " msisdn = '" . mysql_real_escape_string($user_data->msisdn) . "' AND";

        $sql = rtrim($sql, 'AND');
        $sql .= " ORDER BY ID DESC LIMIT 1";

        $result = $this->databaseObj->fetch($sql);
        if (count($result) > 0) {
            return $result [0];
        } else {
            return false;
        }
    }

    public function getException($user_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($user_data)));

        $active = (!empty($user_data->active)) ? $user_data->active : '0';
        $sql = "SELECT * FROM subscription WHERE `active` <> " . mysql_real_escape_string($active);

        if (!empty($user_data->service))
            $sql .= " AND service = '" . mysql_real_escape_string($user_data->service) . "'";

        if (!empty($user_data->adn))
            $sql .= " AND adn = '" . mysql_real_escape_string($user_data->adn) . "'";

        if (!empty($user_data->operator))
            $sql .= " AND operator = '" . mysql_real_escape_string($user_data->operator) . "'";

        if (!empty($user_data->msisdn))
            $sql .= " AND msisdn = '" . mysql_real_escape_string($user_data->msisdn) . "'";

        $sql .= " ORDER BY ID DESC LIMIT 1";

        $result = $this->databaseObj->fetch($sql);
        if (count($result) > 0) {
            return $result [0];
        } else {
            return false;
        }
    }

    public function add($user_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($user_data)));

        $subscribed_from = "";
        $transaction_id_subscribe = "";
        if ($user_data->active == '1') {
            $subscribed_from = date('Y-m-d H:i:s');
            $transaction_id_subscribe = $user_data->transaction_id_subscribe;
        }
        if (empty($user_data->channel_subscribe))
            $channel_subscribe = 'sms';
        else
            $channel_subscribe = $user_data->channel_subscribe;
        $sql = sprintf("INSERT INTO subscription (transaction_id_subscribe, msisdn, service, adn, operator, channel_subscribe, subscribed_from, subscribed_until, partner, active, time_created
						) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',NOW());", mysql_real_escape_string($transaction_id_subscribe), mysql_real_escape_string($user_data->msisdn), mysql_real_escape_string($user_data->service), mysql_real_escape_string($user_data->adn), mysql_real_escape_string($user_data->operator), mysql_real_escape_string($channel_subscribe), mysql_real_escape_string($subscribed_from), '9999-12-31 00:00:00', mysql_real_escape_string($user_data->partner), mysql_real_escape_string($user_data->active));
        $this->databaseObj->query($sql);
        return $this->databaseObj->last_insert_id();
    }

    public function update(user_data $user_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($user_data)));

        $sql = sprintf("UPDATE subscription SET active = '%s', time_updated = NOW()", mysql_real_escape_string($user_data->active));
        if (empty($user_data->channel_subscribe))
            $sql .= sprintf(", channel_subscribe = 'sms'");
        else
            $sql .= sprintf(", channel_subscribe = '%s'", mysql_real_escape_string($user_data->channel_subscribe));
        if (empty($user_data->channel_unsubscribe))
            $sql .= sprintf(", channel_unsubscribe = 'sms'");
        else
            $sql .= sprintf(", channel_unsubscribe = '%s'", mysql_real_escape_string($user_data->channel_unsubscribe));
        if ($user_data->active == '1')
            $sql .= sprintf(", subscribed_from = '%s', transaction_id_subscribe = '%s'", date('Y-m-d H:i:s'), mysql_real_escape_string($user_data->transaction_id_subscribe));
        if ($user_data->active != '1')
            $sql .= sprintf(", subscribed_until = '%s', transaction_id_unsubscribe = '%s'", date('Y-m-d H:i:s'), mysql_real_escape_string($user_data->transaction_id_unsubscribe));

        $sql .= sprintf(" WHERE id = '%s';", mysql_real_escape_string($user_data->id));
        $this->databaseObj->query($sql);
        return true;
    }

    public function getSpringUser($user_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($user_data)));

        $sql = "
				SELECT
					u.id AS id,
					u.adn AS adn,
					s.id AS package_id,
					u.service AS package_name,
					p.protocol AS protocol_number,
					u.active AS status,
					u.subscribed_from AS datetime_subscription,
					u.subscribed_until AS datetime_last_cancel,
					u.channel_subscribe AS channel
				FROM
					subscription AS u
				INNER JOIN
					service AS s ON u.service = s.name AND u.adn = s.adn
				INNER JOIN
					spring_protocol AS p ON p.subscription_id = u.id
				WHERE
					u.msisdn = '" . mysql_real_escape_string($user_data->msisdn) . "'";

        if (!empty($user_data->adn))
            $sql .= " AND u.adn = '" . mysql_real_escape_string($user_data->adn) . "'";

        if (!empty($user_data->protocolType))
            $sql .= " AND p.protocol_type = '" . mysql_real_escape_string($user_data->protocolType) . "'";

        $sql .= " ORDER BY u.id DESC ";

        if (!empty($user_data->limit))
            $sql .= " LIMIT " . mysql_real_escape_string($user_data->limit);

        $result = $this->databaseObj->fetch($sql);
        if (count($result) > 0) {
            return $result;
        } else {
            return false;
        }
    }

    public function getSpringUserHistory($user_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($user_data)));

        $wapDB = loader_config::getInstance()->getConfig('database')->profile ['connWap'] ['database'];

        $sql = "
                       SELECT          
                                u.id,   
                                u.subscribed_from,
                                u.subscribed_until,
                                u.adn,  
                                s.id AS package_id,
                                s.name AS package_name,
                                p.protocol,
                                dl.price tariff,
                                u.channel_subscribe, 
                                u.channel_unsubscribe,
                                u.active
                        FROM    
                                subscription u
                        INNER JOIN
                                service s ON u.adn = s.adn AND u.service = s.name
                        INNER JOIN
                                spring_protocol p ON p.subscription_id = u.id
                        LEFT JOIN
                            (SELECT wd.price, ws.DateCreated,ws.Msisdn, ws.Operator, ws.Service FROM " . mysql_real_escape_string($wapDB) . ".wap_session ws
                        INNER JOIN
                            " . mysql_real_escape_string($wapDB) . ".wap_download_log wd ON wd.SessionID = ws.ID WHERE ws.msisdn = '" . mysql_real_escape_string($user_data->msisdn) . "') dl ON dl.Msisdn = u.msisdn AND dl.Operator = u.operator AND dl.Service = u.service AND dl.DateCreated between u.subscribed_from and u.subscribed_until
                        WHERE
                                u.msisdn = '" . mysql_real_escape_string($user_data->msisdn) . "'
                        ";
        if (!empty($user_data->subscribed_from))
            $sql .= " AND date(u.subscribed_from) >= '" . mysql_real_escape_string($user_data->subscribed_from) . "'";

        if (!empty($user_data->subscribed_until))
            $sql .= " AND date(u.subscribed_until) <= '" . mysql_real_escape_string($user_data->subscribed_until) . "'";

        if (!empty($user_data->service))
            $sql .= " AND u.service = '" . mysql_real_escape_string($user_data->service) . "' ";

        if (!empty($user_data->adn))
            $sql .= " AND u.adn = '" . mysql_real_escape_string($user_data->adn) . "' ";

        $sql .= ' ORDER BY 
					u.id DESC';

        if (!empty($user_data->limit))
            $sql .= " LIMIT " . mysql_real_escape_string($user_data->limit);

        $result = $this->databaseObj->fetch($sql);
        if (count($result) > 0) {
            return $result;
        } else {
            return false;
        }
    }

    public function getSpringUserProtocol($user_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($user_data)));

        $sql = "SELECT p.id, u.transaction_id_subscribe, u.transaction_id_unsubscribe, u.adn AS large_account, s.id AS package_id, u.service AS package_name, p.protocol AS protocol_number, u.msisdn, p.protocol_type AS type_id, u.active, u.subscribed_from, u.subscribed_until, p.mt_time AS datetime_mt, p.mo_time AS datetime_mo, p.msg_data AS message_mo, wd.Price AS tariff_value, p.protocol_type AS tariff_type_id, u.channel_subscribe AS activation_channel, u.channel_unsubscribe AS cancelation_channel, wd.DateCreated AS datetime_purchase 
                FROM spring_protocol AS p
                INNER JOIN subscription AS u ON p.subscription_id = u.id 
                INNER JOIN service AS s ON u.adn = s.adn AND u.service = s.name 
                LEFT JOIN wap.wap_download_log AS wd ON p.id = wd.ProtocolID 
                WHERE p.protocol = '" . mysql_real_escape_string($user_data->protocol) . "'";

        if (!empty($user_data->adn))
            $sql .= " AND u.adn = '" . mysql_real_escape_string($user_data->adn) . "'";

        $sql .= ' LIMIT 1';
        $result = $this->databaseObj->fetch($sql);
        if (count($result) > 0) {
            return $result [0];
        } else {
            return false;
        }
    }

    public function getSummary(summarizer_data $summarizer) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($summarizer)));

        $sql = sprintf("select DATE(subscribed_from) as date_subscribed, operator, adn, service, channel_subscribe as channel, count(*) as total 
					from %s 
					where date(subscribed_from) = '%s' group by adn, service, channel_subscribe, operator", mysql_real_escape_string($summarizer->tableFrom), mysql_real_escape_string($summarizer->date));
        $result = $this->databaseObj->fetch($sql);
        if (count($result) > 0) {
            return $result;
        } else {
            return false;
        }
    }

    public function getSpringDownloadHistory($user_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($user_data)));

        $wapDB = loader_config::getInstance()->getConfig('database')->profile ['connWap'] ['database'];

        $sql = "
			SELECT
                            wd.id,
                            us.adn,
                            p.protocol, 
                            p.mo_time, 
                            p.mt_time, 
                            p.msg_data,
                            wd.price tariff,
                            us.channel_subscribe,
                            us.channel_unsubscribe,
                            wd.status_charging,
                            wc.ID contentId,
                            wc.Title contentName,
                            wc.Code,
                            wd.DateCreated purchase_time,
                            p.protocol_type tariffId
                        FROM
                            " . mysql_real_escape_string($wapDB) . ".wap_session ws
                        INNER JOIN
                            " . mysql_real_escape_string($wapDB) . ".wap_download_log wd ON wd.SessionID = ws.ID
                        INNER JOIN
                            " . mysql_real_escape_string($wapDB) . ".wap_content wc ON wc.Code = wd.ContentCode
                        LEFT JOIN
                            spring_protocol p ON p.id = wd.ProtocolID
                        INNER JOIN
                            (select * from subscription where msisdn = '" . mysql_real_escape_string($user_data->msisdn) . "' order by id desc limit 1)us
                             ON ws.Msisdn = us.msisdn AND ws.Operator = us.operator AND ws.Service = us.service
                        WHERE
                            ws.msisdn = '" . mysql_real_escape_string($user_data->msisdn) . "' AND ws.Status = '1'";

        if (!empty($user_data->subscribed_from))
            $sql .= " AND date(wd.DateCreated) >= '" . mysql_real_escape_string($user_data->subscribed_from) . "'";

        if (!empty($user_data->subscribed_until))
            $sql .= " AND date(wd.DateCreated) <= '" . mysql_real_escape_string($user_data->subscribed_until) . "'";

        if (!empty($user_data->service))
            $sql .= " AND us.service = '" . mysql_real_escape_string($user_data->service) . "' ";

        if (!empty($user_data->adn))
            $sql .= " AND us.adn = '" . mysql_real_escape_string($user_data->adn) . "' ";

        $sql .= ' ORDER BY wd.id DESC';

        if (!empty($user_data->limit))
            $sql .= " LIMIT " . mysql_real_escape_string($user_data->limit);

        $result = $this->databaseObj->fetch($sql);
        if (count($result) > 0) {
            return $result;
        } else {
            return false;
        }
    }

    public function getSpringRetry($user_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($user_data)));

        $sql = "
            SELECT * 
            FROM subscription
            WHERE msisdn = '" . mysql_real_escape_string($user_data->msisdn) . "'";

        if (!empty($user_data->adn))
            $sql .= " AND adn = '" . mysql_real_escape_string($user_data->adn) . "'";

        if (!empty($user_data->active))
            $sql .= " AND active = '" . mysql_real_escape_string($user_data->active) . "'";

        $sql .= ' ORDER BY id DESC LIMIT 1';

        $result = $this->databaseObj->fetch($sql);
        if (count($result) > 0) {
            return $result[0];
        } else {
            return false;
        }
    }

    public function execUser($broadcast_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($broadcast_data)));

        $main_config = loader_config::getInstance ()->getConfig ( 'main' );

        /*
        if(in_array($broadcast_data->service, $main_config->listServiceCicilan))
        {
            $sql = sprintf("SELECT msisdn FROM subscription WHERE 1=1 and msisdn in (592683792,591032274,20733087,1650354441,1319340855,106805531,106805805,1723253761,594647496,2012856223,1126742600,299183707,1630008967,571010621,555361411,129797442,1126137775,456582040,1586758704,2012856223,1126742600,299183707,1630008967,571010621,555361411,129797442,1126137775,456582040,1586758704,1221179825,1756548090,1614619336,160245917,410054479) AND service = '%s' AND adn = '%s' AND operator = '%s' AND active = '1' ORDER BY priority DESC", mysql_escape_string($broadcast_data->service), mysql_escape_string($broadcast_data->adn), mysql_escape_string($broadcast_data->operator));
        }
        else
        {
            */

            $limit = "";
            if(in_array($broadcast_data->service, $main_config->listServiceCicilan))
            {
                $limit = " LIMIT " . (!empty($broadcast_data->select_limit) ? $broadcast_data->select_limit : 40000);
            }

            $sql = sprintf("SELECT msisdn FROM subscription WHERE service = '%s' AND adn = '%s' AND operator = '%s' AND active = '1' ORDER BY priority DESC {$limit}", mysql_escape_string($broadcast_data->service), mysql_escape_string($broadcast_data->adn), mysql_escape_string($broadcast_data->operator));
        //}

        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data;
        } else {
            return false;
        }
    }
	
	public function checkMsisdnSubject($msisdn, $service) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($broadcast_data)));

        $sql = sprintf("SELECT DISTINCT subject FROM subscription_extend WHERE msisdn = '%s' AND service = '%s' LIMIT 1", mysql_escape_string($msisdn), mysql_escape_string($service));
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data[0];
        } else {
            return array();
        }
    }
    
	public function modifyExecUser($broadcast_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($broadcast_data)));

        $sql = sprintf("SELECT s.msisdn,se.subject FROM subscription AS s LEFT JOIN subscription_extend AS se ON s.msisdn = se.msisdn WHERE s.service = '%s' AND s.adn = '%s' AND s.operator = '%s' AND s.active = '1' ORDER BY s.priority DESC", mysql_escape_string($broadcast_data->service), mysql_escape_string($broadcast_data->adn), mysql_escape_string($broadcast_data->operator));
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data;
        } else {
            return false;
        }
    }
	
    public function getSpringexecUser($broadcast_data, $afterDay) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($broadcast_data)));

        $sql = sprintf("SELECT * FROM subscription WHERE service = '%s' AND adn = '%s' AND operator = '%s' AND active = '1' AND DATEDIFF(DATE(NOW()),DATE(subscribed_from)) = '%s' AND DATE(subscribed_from) <> DATE(NOW())", mysql_escape_string($broadcast_data->service), mysql_escape_string($broadcast_data->adn), mysql_escape_string($broadcast_data->operator), mysql_escape_string($afterDay));
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data;
        } else {
            return false;
        }
    }

    public function updateSubscription($transactionId, $msisdn) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start Transaction Id : " . $transactionId . ". msisdn : " . $msisdn));

        $sql = sprintf("UPDATE subscription SET transaction_id_subscribe = '%s'", mysql_real_escape_string($transactionId));
        $sql .= sprintf(" WHERE msisdn = '%s';", mysql_real_escape_string($msisdn));
        $this->databaseObj->query($sql);
        return true;
    }
	
	public function updateStatusSubscription($data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($data)));

        $sql = sprintf("UPDATE subscription SET active = '%s' WHERE msisdn = '%s' AND service = '%s' AND operator = '%s'", mysql_real_escape_string($data->status), mysql_real_escape_string($data->msisdn), mysql_real_escape_string($data->service), mysql_real_escape_string($data->operator));
        $this->databaseObj->query($sql);
        return true;
    }
	
	public function getMembership($data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($data)));

        $sql = sprintf("SELECT * FROM membership WHERE service = '%s' AND operator = '%s' AND msisdn = '%s' LIMIT 1", mysql_escape_string($data->service), mysql_escape_string($data->operator), mysql_escape_string($data->msisdn));
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data;
        } else {
            return array();
        }
    }
	
	public function insertMembership($data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($data)));

        $sql = sprintf("INSERT INTO membership (id,msisdn,active,created_date,modify_date,operator,service,password,sent) values ('','%s','%s','%s','%s','%s','%s','%s','%s')", mysql_real_escape_string($data->msisdn), mysql_real_escape_string($data->active), mysql_real_escape_string($data->created_date), mysql_real_escape_string($data->modify_date), mysql_real_escape_string($data->operator), mysql_real_escape_string($data->service), mysql_real_escape_string($data->password), mysql_real_escape_string($data->sent));
        $this->databaseObj->query($sql);
        return true;
    }

	public function updateActiveMembership($data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($data)));

        $sql = sprintf("UPDATE membership SET active = '%s',modify_date = '%s',sent = '%s' WHERE msisdn = '%s' and operator = '%s' and service = '%s'", mysql_real_escape_string($data->active), mysql_real_escape_string($data->modify_date), mysql_real_escape_string($data->sent), mysql_real_escape_string($data->msisdn), mysql_real_escape_string($data->operator), mysql_real_escape_string($data->service));
        $this->databaseObj->query($sql);
        return true;
    }
	
	public function updatePasswordMembership($data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($data)));

        $sql = sprintf("UPDATE membership SET active = '%s',password = '%s',modify_date = '%s',sent = '%s' WHERE msisdn = '%s' and operator = '%s' and service = '%s'", mysql_real_escape_string($data->active), mysql_real_escape_string($data->password), mysql_real_escape_string($data->modify_date), mysql_real_escape_string($data->sent), mysql_real_escape_string($data->msisdn), mysql_real_escape_string($data->operator), mysql_real_escape_string($data->service));
		//echo $sql;die;
        $this->databaseObj->query($sql);
        return true;
    }
	
	public function updateMembershipSent($data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($data)));

        $sql = sprintf("UPDATE membership SET sent = '%s',modify_date = '%s' WHERE msisdn = '%s' and operator = '%s' and service = '%s'", mysql_real_escape_string($data->sent), mysql_real_escape_string($data->modify_date), mysql_real_escape_string($data->msisdn), mysql_real_escape_string($data->operator), mysql_real_escape_string($data->service));
        $this->databaseObj->query($sql);
        return true;
    }
	
	public function getREGTransact($p) {
		$log = manager_logging::getInstance();
		$log->write(array('level' => 'debug', 'message' => "Start : " . serialize($p)));
		$sql = sprintf("SELECT MSGDATA, SUBJECT, SERVICE, ADN FROM tbl_msgtransact WHERE msisdn = '%s' AND msgindex = '%s' ORDER BY id DESC LIMIT 1;", mysql_real_escape_string($p->msisdn), mysql_real_escape_string($p->msgId));
		
		$result = $this->databaseObj->fetch($sql);
		if (count($result) > 0) {
			return $result[0];
		} else {
			return false;
		}
	}
	
	public function getSubsExtend($data) {
        $log = manager_logging::getInstance();
		$log->write(array('level' => 'debug', 'message' => "Start : " . serialize($data)));
		$sql = sprintf("SELECT * FROM subscription_extend WHERE msisdn = '%s' AND service = '%s' LIMIT 1;", mysql_real_escape_string($data['msisdn']), mysql_real_escape_string($data['service']));
		
		$result = $this->databaseObj->fetch($sql);
		if (count($result) > 0) {
			return $result;
		} else {
			return array();
		}
    }
	
	public function insertSubsExtend($data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($data)));

        $sql = sprintf("INSERT INTO subscription_extend (msisdn,service,subject) values ('%s','%s','%s')", mysql_real_escape_string($data['msisdn']), mysql_real_escape_string($data['service']), mysql_real_escape_string($data['subject']));
        $this->databaseObj->query($sql);
        return true;
    }

	public function updateSubsExtend($data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($data)));

        $sql = sprintf("UPDATE subscription_extend SET subject = '%s' WHERE msisdn = '%s' AND service = '%s'", mysql_real_escape_string($data['subject']), mysql_real_escape_string($data['msisdn']), mysql_real_escape_string($data['service']));
        $this->databaseObj->query($sql);
        return true;
    }
	
	public function saveLatinoMembership($data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($data)));

        $sql = sprintf("INSERT INTO latino_member (`user`, `password`, `status`, `created_date`, `modify_date`) VALUES ('%s', '%s', '%s', '%s', '%s') ON DUPLICATE KEY UPDATE status='%s', `password` = '%s', modify_date = '%s';", mysql_real_escape_string($data->user), mysql_real_escape_string($data->password), mysql_real_escape_string($data->status), mysql_real_escape_string($data->created_date), mysql_real_escape_string($data->modify_date), mysql_real_escape_string($data->status), mysql_real_escape_string($data->password), mysql_real_escape_string($data->modify_date));
		
        $this->databaseObj->query($sql);
        return true;
    }
	
	public function updateLatinoMembership($data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($data)));

        $sql = sprintf("UPDATE latino_member SET `status` = '%s', modify_date = '%s' WHERE `user` = '%s';", mysql_real_escape_string($data->status), mysql_real_escape_string($data->modify_date), mysql_real_escape_string($data->user));
		
        $this->databaseObj->query($sql);
        return true;
    }
	
	public function getLatinoMembership($data) {
		
		$log = manager_logging::getInstance();
		$log->write(array('level' => 'debug', 'message' => "Start : " . serialize($data)));
		$sql = sprintf("SELECT * FROM latino_member WHERE `user` = '%s' LIMIT 1;", mysql_real_escape_string($data->user));
		
		$result = $this->databaseObj->fetch($sql);
		if (count($result) > 0) {
			return $result;
		} else {
			return array();
		}
    }

    public function getSubs($user_data)
    {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($user_data)));

        $sql = "SELECT s.*, IF(se.subject IS NULL||se.subject='NULL'||se.subject=NULL,'',se.subject) AS subject FROM xmp.subscription s LEFT JOIN xmp.subscription_extend se ON s.msisdn = se.msisdn AND s.service = se.service WHERE 1=1";

        if (!empty($user_data['service']))
            $sql .= " AND s.service = '" . mysql_real_escape_string($user_data['service']) . "'";

        if (!empty($user_data['adn']))
            $sql .= " AND s.adn = '" . mysql_real_escape_string($user_data['adn']) . "'";

        if (!empty($user_data['operator']))
            $sql .= " AND s.operator = '" . mysql_real_escape_string($user_data['operator']) . "'";

        if (!empty($user_data['msisdn']))
            $sql .= " AND s.msisdn = '" . mysql_real_escape_string($user_data['msisdn']) . "'";

        $sql .= " LIMIT 1";

        $result = $this->databaseObj->fetch($sql);
        if (count($result) > 0) {
            return $result;
        } else {
            return array();
        }
    }

    public function updateSubs($data) 
    {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($data)));

        $sql = sprintf("UPDATE subscription SET active = '%s' WHERE msisdn = '%s' AND service = '%s' AND operator = '%s'", mysql_real_escape_string($data['status']), mysql_real_escape_string($data['msisdn']), mysql_real_escape_string($data['service']), mysql_real_escape_string($data['operator']));
        $this->databaseObj->query($sql);
        return true;
    }

    public function addSubs($user_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($user_data)));

        $subscribed_from = "";
        $transaction_id_subscribe = "";
        if ($user_data['active'] == '1') {
            $subscribed_from = date('Y-m-d H:i:s');
            $transaction_id_subscribe = $user_data['transaction_id_subscribe'];
        }
        if (empty($user_data['channel_subscribe']))
            $channel_subscribe = 'sms';
        else
            $channel_subscribe = $user_data['channel_subscribe'];

        $sql = sprintf("INSERT INTO subscription (transaction_id_subscribe, msisdn, service, adn, operator, channel_subscribe, subscribed_from, subscribed_until, partner, active, time_created
						) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',NOW());", mysql_real_escape_string($transaction_id_subscribe), mysql_real_escape_string($user_data['msisdn']), mysql_real_escape_string($user_data['service']), mysql_real_escape_string($user_data['adn']), mysql_real_escape_string($user_data['operator']), mysql_real_escape_string($channel_subscribe), mysql_real_escape_string($subscribed_from), '9999-12-31 00:00:00', mysql_real_escape_string($user_data['partner']), mysql_real_escape_string($user_data['active']));
        $this->databaseObj->query($sql);
        return $this->databaseObj->last_insert_id();
    }

    public function getCCLSpecialSchedule($data) 
    {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($data)));

        $sql = sprintf("SELECT * FROM xmp.ccl_special_schedule WHERE upload_date <= now() and is_pushed = '0';");
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data;
        } else {
            return array();
        }
    }

    public function updateCCLSpecialSchedule($data) 
    {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($data)));

        $set = "";
        if(!empty($data['is_pushed']))
            $set = "is_pushed = " . mysql_real_escape_string($data['is_pushed']);
    
        $condition = "";
        if(!empty($data['id']))
            $condition .= "AND id = " . mysql_real_escape_string($data['id']);

        $sql = "UPDATE xmp.ccl_special_schedule SET {$set} WHERE 1=1 {$condition};";
        $this->databaseObj->query($sql);
       
        return true;
    }

    public function updateCCLOriginalMsisdnUpload($data) 
    {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($data)));

        $set = "";
        if(!empty($data['next_upload_date'])){
            $set = "next_upload_date = IF(next_upload_date IS NULL||next_upload_date='NULL'||next_upload_date=NULL, DATE_ADD(NOW(), INTERVAL 0 DAY), DATE_ADD(next_upload_date, INTERVAL ".mysql_real_escape_string($data['next_upload_date'])." DAY))";
        }else{
            $set = "next_upload_date = DATE_ADD(NOW(), INTERVAL 0 DAY)";
        }

        $condition = "";
        if(!empty($data['msisdn']))
            $condition .= " AND msisdn = " . mysql_real_escape_string($data['msisdn']);
        if(!empty($data['service']))
            $condition .= " AND service = '" . mysql_real_escape_string($data['service']) . "'";

        $sql = "UPDATE xmp.cstools_blacklist_monthly SET {$set} WHERE 1=1 {$condition};";
        $this->databaseObj->query($sql);
       
        return true;
    }

    public function getPushSchedule($s) 
    {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($s)));

        $sql = sprintf("SELECT * FROM dbpush.push_schedule WHERE service = '%s' and status IN (%s);", mysql_real_escape_string($s['service']), mysql_real_escape_string($s['status']));
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data;
        } else {
            return array();
        }
    }

    public function addSchedule($s) 
    {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($s)));

        $sql = sprintf("INSERT INTO dbpush.push_schedule VALUE (default,'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','',NOW(),NOW())", mysql_real_escape_string($s['service']), mysql_real_escape_string($s['operator']), mysql_real_escape_string($s['adn']), mysql_real_escape_string($s['recurring_type']), mysql_real_escape_string($s['handlerfile']), mysql_real_escape_string($s['push_time']), mysql_real_escape_string($s['status']), mysql_real_escape_string($s['content_label']), mysql_real_escape_string($s['content_select']), mysql_real_escape_string($s['last_content_id']), mysql_real_escape_string($s['service_type']), mysql_real_escape_string($s['price']));
        $this->databaseObj->query($sql);
       
        return $this->databaseObj->last_insert_id();
    }

    public function updateSchedule($s) 
    {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($s)));

        $sql = sprintf("UPDATE dbpush.push_schedule SET `status` = '%s', `modified` = NOW(), `price` = '%s', push_time = '%s' WHERE id = %d", mysql_real_escape_string($s['status']), mysql_real_escape_string($s['price']), mysql_real_escape_string($s['push_time']), mysql_real_escape_string($s['id']));

        $this->databaseObj->query($sql);
        return true;
    }

    public function deleteSchedule($s) 
    {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($s)));

        $sql = sprintf("DELETE FROM dbpush.push_schedule WHERE service = '%s' AND DATE(push_time) = CURRENT_DATE", mysql_real_escape_string($s['service']));

        $this->databaseObj->query($sql);
        return true;
    }

    public function deleteScheduleCheck($s) 
    {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($s)));

        $sql = sprintf("DELETE FROM dbpush.push_schedule_check WHERE service = '%s' AND price = '%s' AND DATE(created) = CURRENT_DATE", mysql_real_escape_string($s['service']), mysql_real_escape_string($s['price']));

        $this->databaseObj->query($sql);
        return true;
    }

    public function getContentSchedule($s) 
    {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($s)));

        $sql = sprintf("SELECT * FROM dbpush.push_content WHERE service = '%s' AND DATE(datepublish) = CURRENT_DATE;", mysql_real_escape_string($s['service']));
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data;
        } else {
            return array();
        }
    }

    public function deleteContentSchedule($s) 
    {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($s)));

        $sql = sprintf("DELETE FROM dbpush.push_content WHERE service = '%s' AND DATE(datepublish) = CURRENT_DATE", mysql_real_escape_string($s['service']));

        $this->databaseObj->query($sql);
        return true;
    }

    public function autoContentSchedule($s) 
    {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($s)));

        /*
        id: 3851
        service: famcepek
    content_label: text
        content: Daripada bete, mending main kuis Family Cepek aja yuks.
    ketik *500*69# CS:021-52902182
    WA:http://bit.ly/cslinkit360
        author:
            notes:
    datepublish: 2019-12-23 07:00:00
        lastused: 0000-00-00 00:00:00
        created: 2019-12-16 11:03:43
        modified: 2019-12-16 11:03:43
        */

        $sql = sprintf("INSERT INTO dbpush.push_content VALUE (default,'%s','%s','%s','','','%s','',NOW(),NOW())", mysql_real_escape_string($s['service']), mysql_real_escape_string($s['content_label']), mysql_real_escape_string($s['content']), mysql_real_escape_string($s['datepublish']));
        $this->databaseObj->query($sql);
       
        return $this->databaseObj->last_insert_id();
    }
	
	public function getBySubjectData($msisdn, $service, $subject) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . $msisdn));

        $sql = "SELECT * FROM xmp.subscription_extend WHERE msisdn = '" . $msisdn . "' AND service = '" . $service . "' AND subject = '".$subject."' LIMIT 1";

        $result = $this->databaseObj->fetch($sql);
        if (count($result) > 0) {
            return $result;
        } else {
            return array();
        }
    }
}
