<?php

class model_kupon extends model_base {

    public function insert($data) {
                $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($data)));

                $data->kupon = $this->genkupon($data);
        $sql = sprintf("
            INSERT INTO kupon (msisdn,kupon,date_kupon,status)
            VALUES (
                '%s', '%s', NOW(), 0,
            )", mysql_real_escape_string($data->msisdn), mysql_real_escape_string($data->kupon));
        $this->databaseObj->set_charset();
        $query = $this->databaseObj->query($sql);
        if ($query) {
            return $this->databaseObj->last_insert_id();
        } else {
            return false;
        }
    }

        public function genkupon($desc) {
                $result = 'kupon tidak ada';
                $sql = sprintf("select kupon from kupon where status=0 and `desc`='%s' order by id asc limit 1",mysql_real_escape_string($desc));
                $data = $this->databaseObj->fetch($sql);
                if (count($data) > 0) {
                        $result = $data[0]['kupon'];
                }
                return $result;
        }

        public function bookDBKupon($data,$kupon) {
                $sql = sprintf("update kupon set status='1',msisdn='%s',msgindex='%s',date_kupon=now() where kupon = '%s'", mysql_real_escape_string($data->msisdn),mysql_real_escape_string($data->msgId),mysql_real_escape_string($kupon));
                $query = $this->databaseObj->query($sql);
                if ($query) {
                        return true;
                } else {
                        return false;
                }
        }

        public function updateBookDBKupon($data,$status) {
        if($status==4)
                $sql = sprintf("update kupon set status='%s',date_kupon=now() where msgindex = '%s' and msisdn='%s'", $status, mysql_real_escape_string($data->msgId),mysql_real_escape_string($data->msisdn));
        else
                $sql = sprintf("update kupon set status='%s',msisdn=NULL,msgindex='' where msgindex = '%s' and msisdn='%s'", $status, mysql_real_escape_string($data->msgId),mysql_real_escape_string($data->msisdn));

                $query = $this->databaseObj->query($sql);
                if ($query) {
                        return true;
                } else {
                        return false;
                }
        }

        public function getLimitKupon($limit) {
                $sql = sprintf("SELECT * FROM `kupon` WHERE status=4 and msisdn is not null and msgindex and date_kupon=date(now()) <> '' ");
                $data = $this->databaseObj->fetch($sql);
                if(count($data) >= $limit) {
                        return false;
                } else {
                        return true;
                }
        }

        public function getTotalKupon($data) {
                $sql = sprintf("select id from service_point_history where msisdn='%s' and service='%s' and var_1='0' order by id asc limit 1", mysql_real_escape_string($data->msisdn),mysql_real_escape_string($data->service));
                $data = $this->databaseObj->fetch($sql);
                if (isset($data[0]) && count($data) > 0) {
                        return $data;
                } else {
                        return false;
                }
        }

        public function bookKupon($data,$ids) {
                $sql = sprintf("update service_point_history set var_1='%s' where id in (%s)", mysql_real_escape_string($data->msgId),mysql_real_escape_string($ids));
                $query = $this->databaseObj->query($sql);
                if ($query) {
                        return true;
                } else {
                        return false;
                }
        }

        public function checkBookKupon($data) {
                $sql = sprintf("select id from service_point_history where var_1='%s'", mysql_real_escape_string($data->msgId));
                $data = $this->databaseObj->fetch($sql);
                if (count($data) > 0) {
                        return true;
                } else {
                        return false;
                }
        }

        public function updateBookKupon($data,$var_1) {
                $sql = sprintf("update service_point_history set var_1='%s' where var_1 = '%s' OR msgindex='%s'", mysql_real_escape_string($var_1), mysql_real_escape_string($data->msgId),mysql_real_escape_string($data->msgId));
                $data = $this->databaseObj->fetch($sql);
                if (count($data) > 0) {
                        return true;
                } else {
                        return false;
                }
        }
}
