<?php

class model_revcheck extends model_base {

    public function getMoRevByRaw($service, $subject) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($service)));

        $sql = sprintf("SELECT morev,modrev FROM xmp.rev_check WHERE service = '%s' AND subject = '%s' LIMIT 1;", mysql_real_escape_string($service), mysql_real_escape_string($subject));
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data;
        } else {
            return array();
        }
    }
	
	public function updateMoRevByRaw($service, $subject) {
        $log = manager_logging::getInstance();

        $sql = sprintf("UPDATE xmp.rev_check SET morev = ((1 * morev) + charging) WHERE service = '%s' AND subject = '%s';", mysql_real_escape_string($service), mysql_real_escape_string($subject));
        $this->databaseObj->query($sql);
        return true;
    }
	
	public function getDpRevByRaw($service, $subject) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($service)));

        $sql = sprintf("SELECT dprev,dpdrev FROM xmp.rev_check WHERE service = '%s' AND subject = '%s' LIMIT 1;", mysql_real_escape_string($service), mysql_real_escape_string($subject));
        $data = $this->databaseObj->fetch($sql);
        if (count($data) > 0) {
            return $data;
        } else {
            return array();
        }
    }
	
	public function updateDpRevByRaw($service, $subject) {
        $log = manager_logging::getInstance();

        $sql = sprintf("UPDATE xmp.rev_check SET dprev = ((1 * dprev) + charging) WHERE service = '%s' AND subject = '%s';", mysql_real_escape_string($service), mysql_real_escape_string($subject));
        $this->databaseObj->query($sql);
        return true;
    }

    public function inActive($status = '8', $service, $msisdn) {
        $log = manager_logging::getInstance();

        $sql = sprintf("UPDATE xmp.subscription SET active = '%s', time_updated = NOW() WHERE service = '%s' AND msisdn = '%s';", mysql_real_escape_string($status), mysql_real_escape_string($service), mysql_real_escape_string($msisdn));
        $this->databaseObj->query($sql);
        return true;
    }
}
