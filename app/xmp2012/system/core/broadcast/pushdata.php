<?php
class broadcast_pushdata {
	public $id;
	public $service;
	public $contentLabel;
	public $content;
	public $author;
	public $notes;
	public $datePublish;
	public $lastUsed;
	public $created;
	public $modified;
	public $url;
}