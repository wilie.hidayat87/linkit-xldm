<?php
interface broadcast_interface {
	public function push(broadcast_data $broadcast_data);
	public function populateUser(broadcast_data $broadcast_data);
	public function createMT(model_data_pushbuffer $pushbuffer_data, broadcast_data $broadcast_data);
}