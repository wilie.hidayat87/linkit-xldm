<?php

class database_mysqlx implements database_interfacex {
	
	protected $resource;
    protected $connProfile;
    public $numRows;
    
	public function connect($connProfile) {
		
		$log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $this->resource = mysql_connect($connProfile ['host'], $connProfile ['username'], $connProfile ['password']) or $log->write(array('level' => 'error', 'message' => mysql_error($this->resource)));
        if (!is_resource($this->resource)) {
            $log->write(array('level' => 'error', 'message' => "resource not found : " . print_r($this->resource, 1)));
            return false;
        }

        $selectDb = mysql_select_db($connProfile ['database'], $this->resource);
        if ($selectDb === false) {
            $log->write(array('level' => 'error', 'message' => "Database not found : " . $connProfile ['database']));
            return false;
        }

        $this->connProfile = $connProfile;
        return true;
		
	}	


    public function reconnect($connProfile) {                
		if (!mysql_ping($this->resource)) {
			echo 'a';
		    $this->connect($connProfile);
		}

        return true;
    }

    public function query($sql) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . $sql));

        $this->check_connection($this->connProfile);
        $query = mysql_query($sql, $this->resource) or $log->write(array('level' => 'error', 'message' => mysql_error($this->resource)));
        $this->numRows = $this->numRows();
        return $query;
    }

    public function fetch($sql) {
    	
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $row2 = array();
        $query = $this->query($sql);
        while ($row = mysql_fetch_array($query, MYSQL_BOTH)) {
            $row2 [] = $row;
        }
        $this->numRows = $this->numRows();
        return $row2;
    }
    
    protected function check_connection($connProfile) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        if (!mysql_ping($this->resource)) {
            $log->write(array('level' => 'debug', 'message' => "Lost connection"));
            return $this->reconnect($connProfile);
        }
    }

    public function numRows() {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        return mysql_affected_rows($this->resource);
    } 
}