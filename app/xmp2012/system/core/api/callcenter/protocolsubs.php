<?php

class api_callcenter_protocolsubs {

    public function process($GET) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($GET)));

        $check = $this->checkParams($GET);
        if ($check != 'OK') {
            return $check;
        }

        $user_data = new user_data ();
        $user_data->protocol = $GET ['protocol'];
        if (!empty($GET ['largeAccount']))
            $user_data->adn = $GET ['largeAccount'];
        $user_data->active = 1;

        $model_user = loader_model::getInstance()->load('user', 'connDatabase1');
        $spring_user = $model_user->getSpringUserProtocol($user_data);

        if ($spring_user !== false and count($spring_user) > 0)
            return $this->createXML($spring_user);
        else
            return 'NOT_FOUND';
    }

    protected function checkParams($GET) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($GET)));

        if (empty($GET ['protocol'])) {
            return 'MISSING_PARAMETERS';
        } elseif (!empty($GET ['largeAccount']) && !is_numeric($GET ['largeAccount'])) {
            return 'INVALID_PARAMETERS';
        } else {
            return 'OK';
        }
    }

    protected function createXML($user) {
        /*
         *  parameter supplied is from previoid = subscription.transaction_id
          portal-id = from config
          portal-name = from config
          large-account = subscription.adn
          package-id = service.id
          package-name = subscription.name
          protocol-number = spring_protocol.protocol
          msisdn = subscription.msisdn
          type-id = spring_protocol.protocol_type (if 'Subscription') ? '1' : '';
          status = if(active = 1) ? 'SIGNED' : 'CANCEL'
          datetime-subscription = subscription.subscribe_from
          datetime-cancel = subscription.subscribe_to (if active != 1)
          datetime-last-subscription = empty
          datetime-next-subscription = empty
          datetime-mt = spring_protocol.mt_time
          datetime-mo = spring_protocol.mo_time
          message-mo = spring_protocol.msg_data
          tariff-value = spring_service.tariff
          tariff-type-id = spring_protocol.protocol_type
          activation-channel = subscription.channel_subscribe
          cancelation-channel = subscription.channel_unsubscribe
          total-subscription = 1
          credit-remaining = 0
          credit-spent = 0
          free-period-start = empty
          free-period-end = empty
          ip = from config
          datetime-purchase = empty
          datetime-retry = empty
          content-type = empty
          content-id = empty
          content-name = empty
         */
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($user)));

        $config_spring = loader_config::getInstance()->getConfig('spring');

        #last cancel time
        if ($user ['active'] == 2)
            $last_cancel = $this->set_date($user ['subscribed_until']);
        else
            $last_cancel = '';

        # set status
        if ($user ['active'] == 1)
            $status = 'SIGNED';
        else
            $status = 'CANCEL';

        #set ip
        if (strtolower($user ['activation_channel']) == 'sms')
            $ip = $config_spring->ip;
        else
            $ip = $config_spring->ip;

        $date = date("Y-m-d") . "T" . date("H:i:sO");

        $xml = '
		<package-subscription>
				<datetime-request>' . $date . '</datetime-request>
				<package>
				<id>' . $user ['id'] . '</id>
				<portal-id>' . $config_spring->portalId . '</portal-id>
				<portal-name>' . $config_spring->portalName . '</portal-name>
				<large-account>' . $user ['large_account'] . '</large-account>
				<package-id>1</package-id>
				<package-name>' . $user ['package_name'] . '</package-name>
				<protocol-number>' . $user ['protocol_number'] . '</protocol-number>
				<msisdn>' . $user ['msisdn'] . '</msisdn>
				<type-id>1</type-id>
				<status>' . $status . '</status>
				<datetime-subscription>' . $this->set_date($user ['subscribed_from']) . '</datetime-subscription>
				<datetime-cancel>' . $last_cancel . '</datetime-cancel>
				<datetime-last-subscription></datetime-last-subscription>
				<datetime-next-subscription></datetime-next-subscription>
				<datetime-mt>' . $this->set_date($user ['datetime_mt']) . '</datetime-mt>
				<datetime-mo>' . $this->set_date($user ['datetime_mo']) . '</datetime-mo>
				<message-mo>' . $user ['message_mo'] . '</message-mo>
				<tariff-value>' . $user ['tariff_value'] . '</tariff-value>
				<tariff-type-id>' . $user ['tariff_type_id'] . '</tariff-type-id>
				<activation-channel>' . $user ['activation_channel'] . '</activation-channel>
				<cancelation-channel>' . $user ['cancelation_channel'] . '</cancelation-channel>
				<total-subscription></total-subscription>
				<credit-remaining></credit-remaining>
				<credit-spent></credit-spent>
				<free-period-start></free-period-start>
				<free-period-end></free-period-end>
				<ip>' . $ip . '</ip>
				<datetime-purchase>' . $user ['datetime_purchase'] . '</datetime-purchase>
				<datetime-retry></datetime-retry>
				<content-type></content-type>
				<content-id></content-id>
				<content-name></content-name>
		</package>
	</package-subscription>
		';

        return $xml;
    }

    protected function set_date($date) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . $date));

        $date = explode(" ", $date);
        return $date [0] . "T" . $date [1] . date("O");
    }

}