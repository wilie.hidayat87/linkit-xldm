<?php

class api_callcenter_subscription {

    public function process($GET) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($GET)));

        $check = $this->checkParams($GET);

        if ($check != 'OK')
            return $check;

        $config_spring = loader_config::getInstance()->getConfig('spring');
        $user_data = new user_data ();
        $user_data->msisdn = $GET ['phone'];
        if (!empty($GET ['largeAccount']))
            $user_data->adn = $GET ['largeAccount'];
        $user_data->active = 1;
        $user_data->limit = 1;
        $user_data->protocolType = array_search('Subscription', $config_spring->protocol_type);

        $model_user = loader_model::getInstance()->load('user', 'connDatabase1');
        $spring_user = $model_user->getSpringUser($user_data);

        if ($spring_user !== false and count($spring_user) > 0)
            return $this->createXML($spring_user);
        else
            return 'NOT_FOUND';
    }

    protected function checkParams($GET) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($GET)));

        if (empty($GET ['phone'])) {
            return 'MISSING_PARAMETERS';
        } else if (!is_numeric($GET ['phone']) || (!empty($GET ['largeAccount']) && !is_numeric($GET ['largeAccount']))) {
            return 'INVALID_PARAMETERS';
        } else {
            return 'OK';
        }
    }

    protected function createXML($users) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($users)));

        $config_spring = loader_config::getInstance()->getConfig('spring');
        $size = count($users);
        $date = date("Y-m-d") . "T" . date("H:i:sO");

        $xml = '<package-subscription>
				<datetime-request>' . $date . '</datetime-request>
				<size>' . $size . '</size>';

        foreach ($users as $user) {
            # set status
            if ($user ['status'] == 1)
                $status = 'SIGNED';
            else
                $status = 'CANCEL';

            #last cancel time
            if ($user ['status'] == 2)
                $last_cancel = $this->set_date($user ['datetime_last_cancel']);
            else
                $last_cancel = '';

            #set ip2011-08-06
            if (strtolower($user ['channel']) == 'sms')
                $ip = $config_spring->ip;
            else
                $ip = $config_spring->ip;

            $xml .= '
				<package>
					<id>' . $user ['id'] . '</id>
					<portal-id>' . $config_spring->portalId . '</portal-id>
					<portal-name>' . $config_spring->portalName . '</portal-name>
					<large-account>' . $user ['adn'] . '</large-account>
					<package-id>' . $user ['package_id'] . '</package-id>
					<package-name>' . $user ['package_name'] . '</package-name>
					<protocol-number>' . $user ['protocol_number'] . '</protocol-number>
					<status>' . $status . '</status>
					<model-id></model-id>
					<datetime-subscription>' . $this->set_date($user ['datetime_subscription']) . '</datetime-subscription>
					<datetime-last-cancel>' . $last_cancel . '</datetime-last-cancel>
					<datetime-last-subscription></datetime-last-subscription>
					<datetime-next-subscription></datetime-next-subscription>
					<channel>' . strtoupper($user ['channel']) . '</channel>
					<total-subscription></total-subscription>
					<credit-remaining></credit-remaining>
					<credit-spent></credit-spent>
					<free-period-start></free-period-start>
					<free-period-end></free-period-end>
					<ip>' . $ip . '</ip>
				</package>
				';
        }

        $xml .= '</package-subscription>';
        /*
          id = subscription.id
          portal-id = from config
          portal-name = from config
          large-account = service.adn
          package-id = service.id
          package-name = subscription.service
          protocol-number = protocol.name
          status = if (subscription.active = 1) ? 'SIGNED' : 'CANCEL'
          model-id = empty
          datetime-subscription = subscription.subscribe_from
          datetime-last-cancel = subscription.subscribe_to (if subscription.status = 2)
          datetime-next-subscription = empty
          channel = subscription.channel_subscribe
          total-subscription = 1
          credit-remaining = 0
          credit-spent = 0
          free-period-start = empty
          free-period-end = empty
          ip = server ip from config (if channel is SMS), (if web ??? to do list to save user ip)
         */
        /* sample valid XML
          <package-subscription>
          <datetime-request>2007-11-09T16:43:09-0300</datetime-request>
          <size>2</size>
          <package>
          <id>900</id>
          <portal-id>1</portal-id>
          <portal-name>Portal Spring Wireless</portal-name>
          <large-account>123456</large-account>
          <package-id>1</package-id>
          <package-name>RingTones</package-name>
          <protocol-number>548755744875666741</protocol-number>
          <status>RENEWAL</status>
          <model-id>123</model-id>
          <datetime-subscription>2007-11-08T15:57:19-0300</datetime-subscription>
          <datetime-last-cancel>2007-11-02T09:12:51-0300</datetime-last-cancel>
          <datetime-last-subscription>2007-11-08T15:57:19-0300</datetime-last-subscription>
          <datetime-next-subscription>2007-11-15T15:57:19-0300</datetime-next-subscription>
          <channel>WEB</channel>
          <total-subscription>1</total-subscription>
          <credit-remaining>10</credit-remaining>
          <credit-spent>0</credit-spent>
          <free-period-start>2007-11-19</free-period-start>
          <free-period-end>2007-11-23</free-period-end>
          <ip>127.0.0.1</ip>
          </package>
          </package-subscription>
          ';
          /* */
        return $xml;
    }

    protected function set_date($date) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($date)));

        $date = explode(" ", $date);
        return $date [0] . "T" . $date [1] . date("O");
    }

}