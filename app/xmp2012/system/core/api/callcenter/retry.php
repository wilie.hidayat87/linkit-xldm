<?php

class api_callcenter_retry {

    public function process($GET) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($GET)));

        $check = $this->checkParams($GET);
        if ($check != 'OK') {
            return $check;
        }
        $user_data = new user_data ();
        $user_data->msisdn = $GET ['phone'];
        if (!empty($GET ['largeAccount']))
            $user_data->adn = $GET ['largeAccount'];
        if (!empty($GET ['id']))
            $user_data->transaction_id = $GET ['id'];

        //$GET ['contentId'];
        //$GET ['tariff'];
        //$GET ['channel'];
        //$GET ['portal'];

        $user_data->active = 1;

        $model = loader_model::getInstance();
        $model_user = $model->load('user', 'connDatabase1');
        $model_operator = $model->load('operator', 'connDatabase1');
        $service_config = loader_config::getInstance()->getConfig('service');

        $get_data = $model_user->get($user_data);
        if ($get_data === false) {
            return 'NOT_FOUND';
        }

        $ini_reader = ini_reader::getInstance();
        $ini_data = loader_data::get('ini');
        $ini_data->file = $service_config->iniPath . "clubfun.ini";

        $content = loader_data::get('content');

        $user = loader_data::get('user');
        $user->service = $get_data['service'];
        $user->msisdn = $get_data['msisdn'];
        $user->adn = $get_data['adn'];
        $user->operator = $get_data['operator'];

        $content->handler = 'indirect';
        $content->price = 0;
        $content->limit = 1;
        $content->userObj = $user;
        $content->generatorType = 'spring';
        $content->initialChargeType = 'charged';

        $ini_data->type = 'wap_name_499';
        $ini_data->section = 'REPLY';
        $content->wapName = $ini_reader->get($ini_data);

        $contentNew = content_manager::getInstance()->getContent($content);

        $mt = loader_data::get('mt');
        $mt->msgId = date('YmdHis') . str_replace('.', '', microtime(true));
        $mt->adn = $user->adn;
        $mt->subject = strtoupper('MT;WAPPUSH;' . $get_data['channel_subscribe'] . ';RETRYCALLCENTER');
        $mt->msisdn = $user->msisdn;
        $mt->type = 'wappush';
        $mt->content_data = $contentNew;
        $mt->channel = $get_data['channel_subscribe'];
        $mt->operatorId = $model_operator->getOperatorId($user->operator);
        $mt->operatorName = $user->operator;
        $mt->service = $user->service;
        $mt->msgData = "CLUBFUN: Clique no link para baixar jogos de acordo com o preco no Wap site.";
        $mt->price = "0";

        $charging_manager = charging_manager::getInstance();
        $charging_data = $charging_manager->getCharging($mt);

        $mt->serviceId = $charging_data->chargingId;
        $mt->price = $charging_data->gross;
        $mt->charging = $charging_data;

        $queue = $this->saveToQueue($mt);

        if ($queue == true)
            return 'OK';
        else
            return 'NOT_FOUND';
    }

    private function saveToQueue($mt_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($mt_data)));

        $profile = "wappush";
        $loader_queue = loader_queue::getInstance();
        $queue = $loader_queue->load($profile);

        $data = loader_data::get('queue');
        $data->channel = $mt_data->operatorName . "MTWapPush1";
        $data->value = serialize($mt_data);

        if ($queue) {
            return $queue->put($data);
        } else {
            $config_retry = loader_config::getInstance()->getConfig('retry');

            $filename = uniqid() . ".queue";
            $path = $config_retry->bufferPathQueue . "/" . $filename;
            $content = $data;

            $buffer = buffer_file::getInstance();

            return $buffer->save($path, $content);
        }
    }

    private function checkParams($GET) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($GET)));

        if (empty($GET ['phone']) && empty($GET ['id'])) {
            return 'MISSING_PARAMETERS';
        } else if (!is_numeric($GET ['phone']) || (!empty($GET ['largeAccount']) && !is_numeric($GET ['largeAccount'])) || (!empty($GET ['tariff']) && !is_numeric($GET ['tariff']))) {
            return 'INVALID_PARAMETERS';
        }
        return 'OK';
    }

}
