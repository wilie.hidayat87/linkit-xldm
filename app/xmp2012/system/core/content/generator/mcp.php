<?php

class content_generator_mcp implements content_generator_interface {

    private static $instance;

    private function __construct() {
        
    }

    public static function getInstance() {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        if (!self::$instance)
            self::$instance = new self ();

        return self::$instance;
    }

    public function generate($mo_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($mo_data)));

        $url = str_replace("@TRXID@", $mo_data->msgId, loader_config::getInstance()->getConfig('content')->telkomhotspotURL);

        $hit = http_request::get($url);

        $response = explode("|", $hit);

        if ($response[0] == 0) {
            $status = array('status' => $response[0], 'username' => $response[1], 'password' => $response[2], 'timestamp' => $response[3], 'trxid' => $response[4]);
            return $status;
        } else {
            return false;
        }
    }

}