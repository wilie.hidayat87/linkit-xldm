<?php

class tools_convert {

    public function objectToArray($object) {
        $log = manager_logging::getInstance ();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($object)));
        
        $array = array();
        foreach ($object as $member => $data) {
            $array [$member] = $data;
        }
        return $array;
    }

}