<?php

class mo_parser implements mo_parser_interface {

    private static $instance;

    public static function getInstance() {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        if (!self::$instance) {
            self::$instance = new self ();
        }

        return self::$instance;
    }

    public function parseMessage($mo_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($mo_data)));

        $allow_request_type = loader_config::getInstance()->getConfig('main')->request_type;
        $words = $mo_data->rawSMS;
        $word = explode(" ", $words, 2);

        if (!empty($word [0]))
            $tmp_request_type = strtolower(trim($word [0]));
        else
            $tmp_request_type = "";

        if (!empty($word [1]))
            $mo_data->keyword = strtolower(trim($word [1]));
        else
            $mo_data->keyword = $words;

        foreach ($allow_request_type as $reqKey => $reqValue) {
            if (in_array($tmp_request_type, $reqValue)) {
                $mo_data->requestType = $reqKey;
            }
        }

        $channel_list = loader_config::getInstance()->getConfig('main')->channel;

        $is_have_channel = false;
        foreach ($channel_list as $channel) {
            $pattern = '/' . $channel . '/';
            if (preg_match($pattern, $mo_data->keyword)) {
                $mo_data->channel = $channel;
                $is_have_channel = true;
                break;
            }
        }
        
        $partner_list = loader_config::getInstance()->getConfig('partner')->partner;
        foreach ($partner_list as $partner) {
            $pattern = '/' . $partner . '/';
            if (preg_match($pattern, $mo_data->keyword)) {
                $mo_data->partner = $partner;
                break;
            }
        }

        // if no channel set default to sms
        if (!$is_have_channel)
            $mo_data->channel = 'sms';

        //check parameter if exist
        $param = explode(" ", $mo_data->keyword);

        if (!empty($param [1]))
            $parameter = trim($param [1]);

        if (!empty($parameter))
            $mo_data->parameter = $parameter;

        $mo_data->msgData = $mo_data->rawSMS;
        return $mo_data;
    }

    public static function getParser($charObj) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $config_main = loader_config::getInstance()->getConfig('main');
        $class_name = $config_main->operator . '_' . $charObj . '_parser';
        $class_name_default = 'default_' . $charObj . '_parser';
        if (class_exists($class_name)) {
            return new $class_name ();
        } elseif (class_exists($class_name)) {
            $log->write(array('level' => 'info', 'message' => "Class Doesn't Exist : " . $class_name));
            return new $class_name_default ();
        } else {
            $log->write(array('level' => 'error', 'message' => "Class Doesn't Exist : " . $class_name . " & " . $class_name_default));
            return false;
        }
    }

}
