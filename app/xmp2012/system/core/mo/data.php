<?php

/*
 * MO Data 
 */

class mo_data {

    public $inReply;
    public $msgId;
    public $adn;
    public $msisdn;
    public $msgData;
    public $msgLastStatus;
    public $retry;
    public $msgStatus;
    public $closeReason;
    public $serviceId;
    public $price;
    public $operatorId;
    public $media;
    public $channel;
    public $service;
    public $partner;
    public $keyword;
    public $operatorName;
    public $rawSMS;
    public $requestType;
    public $incomingDate;
    public $parameter;
    public $type;
    public $patternId;
    public $id;
    public $charging;
    public $subject;
    public $userData;
    public $wapSession;
    public $autoPin;

}