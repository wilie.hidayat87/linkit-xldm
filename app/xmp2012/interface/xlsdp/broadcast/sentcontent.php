<?php
class xlsdp_broadcast_sentcontent {
	
	public function process(){
    	//test
        $log_profile = 'broadcast';
        $log = manager_logging::getInstance ();
        $log->setProfile($log_profile);
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $config_main = loader_config::getInstance ()->getConfig('main');
        $slot = loader_config::getInstance ()->getConfig('contentbuffer')->bufferSlot;

        //forking process slot

        for ($i = 0; $i < $slot; $i++) {
            if ($config_main->use_forking) {
                switch ($pid = pcntl_fork ()) {
                    case - 1 :
                        $log->write(array('level' => 'error', 'message' => "Forking failed"));
                        die('Forking failed');
                        break;
                    case 0 :
                        $this->processSentContent($i);
                        exit ();
                        break;
                    default :
                        //pcntl_waitpid ( $pid, $status );
                        break;
                }
            } else {
                $this->processSentContent($i);
            }
        }
        return true;
    }
	
	public function processSentContent($slot) 
	{
        $log = manager_logging::getInstance ();
		$log->write(array('level' => 'debug', 'message' => 'Start : ' . $slot));

		$load_config = loader_config::getInstance();
		$buffer_file = buffer_file::getInstance();

		$contentbuffer = $load_config->getConfig('contentbuffer');

		$path = $contentbuffer->bufferPath . '/' . $slot;
		$limit = $contentbuffer->bufferThrottle;
		$result = $buffer_file->read($path, $limit, 'cb');

		if ($result !== false) {
			foreach ($result as $val) {
				foreach ($val as $crDataPath => $data) {
					$log->write(array('level' => 'debug', 'message' => "path : " . $crDataPath . " content : " . serialize($data)));

					if (is_object($data)) 
					{
						$buffer_file->delete($crDataPath);
						
						$crSave = $this->sent($data);
						
					} else {
						$log->write(array('level' => 'error', 'message' => "buffer is not an object"));
						$buffer_file->delete($crDataPath);
					}
				}
			}
		}
    }
	
	public function sentResponse($rawdata)
	{	
		if (isset($rawdata) && count($rawdata) != 0) 
		{
			$log = manager_logging::getInstance ();
			$log->write(array('level' => 'debug', 'message' => "Start"));
			
			$mainConfig = loader_config::getInstance ()->getConfig('main');
				
			$loader_model = loader_model::getInstance();
			$user = $loader_model->load('user', 'connDatabase1');
			
			$trxdata = new stdClass();
			$trxdata->msisdn=$rawdata['msisdn'];
			$trxdata->operator=$rawdata['operator'];
			$trxdata->service=$rawdata['service'];
					
			$rec = $user->getMembership($trxdata);
			
			$api = new manager_api();
			
			if(count($rec) > 0)
				return array('status' => 'OK', 'q' => $api->__encode($rec[0]));
			else
				return array('status' => 'NOK');
		}
		else return array('status' => 'NOK');
	}
	
	public function sent($data, $timeout = 10) {
		
		$log = manager_logging::getInstance ();
		$log->write(array('level' => 'debug', 'message' => "Start"));
		
		$mainConfig = loader_config::getInstance ()->getConfig('main');
		$loader_model = loader_model::getInstance();
		$user = $loader_model->load('user', 'connDatabase1');
			
		$start_hit = time();
		
		$trxdata = new stdClass();
		$trxdata->msisdn=$data->msisdn;
		$trxdata->operator=$data->operator;
		$trxdata->service=$data->service;
		$trxdata->modify_date=date("Y-m-d H:i:s");
		$trxdata->sent=1;
				
		$user->updateMembershipSent($trxdata);
		
		$rec = $user->getMembership($trxdata);
		
		$data = array(
			  'msisdn'=>$trxdata->msisdn,
			  'modify_date'=>$trxdata->modify_date,
			  'operator'=>$trxdata->operator,
			  'service'=>$trxdata->service,
			  'active'=>$data->active,
			  'sent'=>1,
			  'password'=>$rec[0]['password']
			  );
		
		$api = new manager_api();
		
		//$dataArray = $api->__encode($data);
		//$dataArray = "?q=" . $dataArray;
			
		$url = $mainConfig->videorazziURL.'&'.http_build_query($data);
		
        $ch = @curl_init();
        @curl_setopt($ch, CURLOPT_URL, $url);
        @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        @curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        $output = @curl_exec($ch);
        $header = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
        @curl_close($ch);

		//print_r($output);die;
        $end_hit = time();
		
		$log->write(array('level' => 'debug', 'message' => "Hit Sent Content : " . $url . ", Header : " . $header . ", RequestHit(".$start_hit."), ResponseHit(".$end_hit."), TimeHit=".((int)$end_hit-(int)$start_hit)));
		
        return $output;
    }
	
	public function processUnreg($mo_data){
		
		$log = manager_logging::getInstance ();
		$log->write(array('level' => 'debug', 'message' => "Start"));
		
		$mainConfig = loader_config::getInstance ()->getConfig('main');
		$loader_model = loader_model::getInstance();
		$user = $loader_model->load('user', 'connDatabase1');
		
		if(strtolower($mo_data->keyword) == "videorazzi")
		{
			if($mainConfig->openCustomVideorazzi)
			//if(true)
			{						
				if(
					$mo_data->channel == 'sms' 
				 && $mo_data->adn == '99876' 
				 && $mo_data->msgData == 'unreg videorazzi' 
				 && $mo_data->keyword == 'videorazzi' 
				 && $mo_data->operatorName == 'xlsdp' 
				 && $mo_data->requestType == 'unreg' 
					)
				{
					$trxdata = new stdClass();
					$trxdata->msisdn=$mo_data->msisdn;
					$trxdata->modify_date=date("Y-m-d H:i:s");
					$trxdata->operator=$mo_data->operatorName;
					$trxdata->service= (strpos($mo_data->keyword, "register") !== FALSE) ? $mo_data->keyword : $mo_data->keyword . "_register";
					$trxdata->active=0; 
					$trxdata->sent=0;
					
					$user->updateActiveMembership($trxdata);
					
					$buffer_file = buffer_file::getInstance ();
											
					$path = $buffer_file->generate_file_name ( $trxdata, "contentbuffer", "cb" );
					
					if ($buffer_file->save ( $path, $trxdata )) {
						$log->write(array('level' => 'debug', 'message' => "Buffer OK"));
						//return array('level' => 'debug', 'message' => "Buffer OK");
					} else {
						$log->write(array('level' => 'debug', 'message' => "Buffer NOK"));
						//return array('level' => 'debug', 'message' => "Buffer NOK");
					}
				}
			}
		}
	}

	public function autoschedule()
	{
		$log_profile = 'broadcast';
        $log = manager_logging::getInstance ();
        $log->setProfile($log_profile);
        $log->write(array('level' => 'debug', 'message' => "Start"));

		$pushschedule = loader_model::getInstance()->load('schedule', 'crossDB');
		$pushcontent = loader_model::getInstance()->load('pushcontent', 'crossDB');

		// GET ALL SCHEDULE BY STATUS = 2 OR DONE
		$broadcast = new broadcast_data ();
        $broadcast->service = array("memberpush","Videorazzi_register","latino","famcepek");
        $broadcast->status = 2;

		$data = $pushschedule->getall($broadcast);
		$count_data = count($data);
		
		if($count_data > 0)
		{
			for($x=0; $x<$count_data; $x++)
			{
				// GET ALL CONTENT STORE BY SELECTED SERVICE
				$bd = new broadcast_data ();
				$bd->service = $data[$x]['service'];
				$allContentStorePerService = $pushcontent->getContentStore($bd);
				$countStoreContent = count($allContentStorePerService);

				// UPDATE ROLLING CONTENT FOR NEXT TOTAL CONTENT PER SERVICE 
				$bdUpd = new broadcast_data ();
				$bdUpd->id = $allContentStorePerService[0]['id'];
				$bdUpd->service = $bd->service;
				//datepublish
				$bdUpd->pushTime = date("Y-m-d", strtotime("+".($countStoreContent+1)." day"));
				//lastused
				$bdUpd->modified = $allContentStorePerService[0]['datepublish'];
				$pushcontent->updateContentStore($bdUpd);
				
				// GET FIRST CONTENT STORE ONLY
				$getFirstContent = $allContentStorePerService[0]['content'];

				// INSERT INTO PUSH CONTENT BY NEXT DAY
				$pushContentData = array(
					 'service' 			=> $bd->service
					,'content_label'	=> 'text'
					,'content'			=> trim($getFirstContent)
					,'author'			=> ''
					,'notes'			=> ''
					,'datepublish'		=> date("Y-m-d H:i:s", strtotime("+1 day"))
					,'lastused'			=> '0000-00-00 00:00:00'
					,'created'			=> '0000-00-00 00:00:00'
					,'modified'			=> '0000-00-00 00:00:00'
				);
				$pushcontent->insertContent($pushContentData);

				// UPDATE PUSH SCHEDULE INTO STATUS = 0 AND NEXT DAY PUSH TIME
				$bdSUpd = new broadcast_data ();
				$bdSUpd->id = $data[$x]['id'];
				$pushschedule->activeSchedule($bdSUpd);
			}
		}
		
		return true;
	}

	public function retryFailedDR($limit = 500, $closereason = '3101')
	{
		$log_profile = 'broadcast';
        $log = manager_logging::getInstance ();
        $log->setProfile($log_profile);
        $log->write(array('level' => 'debug', 'message' => "Start Retry Dailypush With Limit : " . $limit));

		$t = loader_model::getInstance()->load('tblmsgtransact', 'crossDB');
		$pb = loader_model::getInstance()->load('pushbuffer', 'crossDB');

		// GET ALL DATA FAILED WITH CLOSEREASON ('3101','3320','30000000000513','30000000003108')
		// INSERT INTO TABLE xmp.tbl_msgtransact_retry 
		$trx = $t->getFailedData(
			array( 
			"status" 		=> "FAILED",
			"closereason" 	=> (!empty($closereason) ? $closereason : '3101'),
			"limit"			=> (!empty($limit) ? $limit : 500),
			)
		);

		if(count($trx) > 0)
		{
			for($x=0; $x<count($trx); $x++)
			{
				$id = $trx[$x]['ID'];
				$msisdn = $trx[$x]['MSISDN'];
				$service = $trx[$x]['SERVICE'];
				$closereason = $trx[$x]['CLOSEREASON'];
				$subject = str_replace(";","_",$trx[$x]['SUBJECT']);

				if(!in_array(strtolower($service), array('cicilanxl1')))
				{
					$logwrite = '[msisdn - closereason - service] : [' . $msisdn . ' - ' . $closereason . ' - ' . $service . ']';
					echo $logwrite . "\n\r";
					$log->write(array('level' => 'info', 'message' => $logwrite));

					// REMOVE SELECTED MSISDN WITH FAILED CLOSEREASON
					$t->removeSelectedTRX(array("id" => $id));

					// REMOVE SELECTED MSISDN AND SERVICE IN TABLE tbl_msgtransact_retry IF ANY
					$t->removeSelectedTRXInRetryBackupTable(
						array(
							"msisdn" => $msisdn, 
							"service" => $service, 
							"subject" => $subject
						)
					);

					// REMOVE LOCK MSISDN
					$path_msisdn_lock = "/Logs2/xlsdp/lock_dr/lock_".date("Ymd")."/".$msisdn."_".$service."_".$subject.".lock";
					unlink($path_msisdn_lock);
					
					// UPDATE INTO PUSH BUFFER STAT 'ON_QUEUE' ON SELECTED SERVICE AND MSISDN
					$pb->updateStatPushBuffer(
						array(
							"stat" 		=> "ON_QUEUE", 
							"service" 	=> $service, 
							"msisdn" 	=> $msisdn,
							)
					);
				}
			}
		}

		return true;
	}
}
?>
