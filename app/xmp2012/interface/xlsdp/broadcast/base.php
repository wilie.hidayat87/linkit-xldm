<?php
class xlsdp_broadcast_base extends default_broadcast_base {
	/**
	 * @param $broadcast_data
	 */
	public function push(broadcast_data $broadcast_data) {

		$log = manager_logging::getInstance ();
		$log->write ( array ('level' => 'debug', 'message' => "Start" ) );
		
		$main_config = loader_config::getInstance ()->getConfig ( 'main' );

		// Check locking DR service cicilan 1 - 6
		/*
		if(in_array($broadcast_data->service,$main_config->listServiceCicilan))
		{
			$lockdr = '/app/xmp2012/buffers/xlsdp/lock_dr_'.$broadcast_data->service.'*';
			touch($lockdr);
		}
		*/

		if ($broadcast_data->contentSelect == "custom") {
			$custombase = new default_broadcast_custombase ( $broadcast_data );
			return $custombase->push ( $broadcast_data );
		}
		
		$content_manager = content_manager::getInstance ();
		$broadcast_content = $content_manager->getBroadcastContent ( $broadcast_data );
		
		if ($broadcast_content == false) {
			return false;
		}
		
		$model_operator = loader_model::getInstance ()->load ( 'operator', 'connDatabase1' );
		$operator_name = $broadcast_data->operator;
		$operator_id = $model_operator->getOperatorId ( $operator_name );
		
		$users = $this->populateUser ( $broadcast_data );
		
		if ($users !== false) {
			$pushproject_data = new model_data_pushproject ();
			$pushproject_data->sid = $broadcast_data->id;
			$pushproject_data->src = $broadcast_data->adn;
			$pushproject_data->oprid = $operator_id;
			$pushproject_data->service = $broadcast_data->service;
			$pushproject_data->subject = strtoupper ( "MT;PUSH;SMS;DAILYPUSH" );
			$pushproject_data->message = $broadcast_content->content;
			$pushproject_data->price = $broadcast_data->price;
			
			$mPushProject = loader_model::getInstance ()->load ( 'pushproject', 'connBroadcast' );
			$pid = $mPushProject->save ( $pushproject_data );
			
			$amount = 0;
			foreach ( $users as $users_data ) {
				$pushbuffer_data = new model_data_pushbuffer ();
				$pushbuffer_data->pid = $pid;
				$pushbuffer_data->src = $broadcast_data->adn;
				$pushbuffer_data->dest = $users_data ['msisdn'];
				$pushbuffer_data->oprid = $operator_id;
				$pushbuffer_data->service = $broadcast_data->service;
				
				$subs_extend = $this->checkMsisdnSubject($users_data ['msisdn'], $broadcast_data->service);
				
				$subkeyword = "";
				
				if(!empty($subs_extend))
				{
					if($subs_extend ['subject'] != 'NORMAL')
						$subkeyword = ";" . $subs_extend ['subject'];
				}
					
				$pushbuffer_data->subject = strtoupper ( "MT;PUSH;SMS;DAILYPUSH" . $subkeyword );
				
				$pushbuffer_data->message = $broadcast_content->content;
				$pushbuffer_data->price = $broadcast_data->price;
				$pushbuffer_data->stat = "ON_QUEUE";
				$pushbuffer_data->tid = date ( "YmdHis" ) . str_replace ( '.', '', microtime ( true ) );
				$pushbuffer_data->thread_id = substr ( $pushbuffer_data->dest, strlen ( $pushproject_data->dest ) - 1, 1 );
				;
				
				$mt_data = $this->createMT ( $pushbuffer_data, $broadcast_data );
				$pushbuffer_data->obj = serialize ( $mt_data );
				
				$mPushBuffer = loader_model::getInstance ()->load ( 'pushbuffer', 'connBroadcast' );
				
				if ($mPushBuffer->save ( $pushbuffer_data )) {
					$amount ++;
				}
			}

			$pushproject_data = new model_data_pushproject ();
			$pushproject_data->pid = $pid;
			$pushproject_data->amount = $amount;
			$mPushProject->update ( $pushproject_data );
		}
		
		return true;
	}
	
	/**
	 * @param $broadcast_data
	 */
	public function populateUser(broadcast_data $broadcast_data) {
		$log = manager_logging::getInstance ();
		$log->write ( array ('level' => 'debug', 'message' => "Start" ) );
		
		$model_user = loader_model::getInstance ()->load ( 'user', 'connDatabase1' );
		return $model_user->execUser ( $broadcast_data );
		//return $model_user->modifyExecUser ( $broadcast_data );
	}
	
	/**
	 * @param $broadcast_data
	 */
	public function checkMsisdnSubject($msisdn, $service) {
		$log = manager_logging::getInstance ();
		$log->write ( array ('level' => 'debug', 'message' => "Start" ) );
		
		$model_user = loader_model::getInstance ()->load ( 'user', 'connDatabase1' );
		return $model_user->checkMsisdnSubject ( $msisdn, $service );
		//return $model_user->modifyExecUser ( $broadcast_data );
	}
	
	/**
	 * @param $broadcast_data
	 * @param $user_data
	 */
	public function createMT(model_data_pushbuffer $pushbuffer_data, broadcast_data $broadcast_data) {
		$log = manager_logging::getInstance ();
		
		$log->write ( array ('level' => 'debug', 'message' => "Start" ) );
		
		$mt_data = loader_data::get ( 'mt' );
		$mt_data->inReply = NULL;
		$mt_data->msgId = date ( "YmdHis" ) . str_replace ( '.', '', microtime ( true ) );
		$mt_data->adn = $pushbuffer_data->src;
		$mt_data->msgData = $pushbuffer_data->message;
		$mt_data->price = $pushbuffer_data->price;
		$mt_data->operatorId = $pushbuffer_data->oprid;
		$mt_data->channel = "sms";
		$mt_data->service = $pushbuffer_data->service;
		$mt_data->subject = $pushbuffer_data->subject;
		$mt_data->operatorName = $broadcast_data->operator;
		$mt_data->msisdn = $pushbuffer_data->dest;
		$mt_data->type = "dailypush";
		
		return $mt_data;
	}
	
	// this is called from broadcast manager
	public function sendtoQueue() {
		global $params;
		
		$service = $params['s'];
		$thread_id = $params['t'];
		if ($params['n']) $limit = $params['n']; else {
			$broadcast_config = loader_config::getInstance ()->getConfig ( 'broadcast' );
			$limit = $broadcast_config->limit;
		}

		$log = manager_logging::getInstance ();
		$log->write ( array ('level' => 'debug', 'message' => "Start" ) );
		
		$main_config = loader_config::getInstance ()->getConfig ( 'main' );
		$operator_name = $main_config->operator;
		
		$model_operator = loader_model::getInstance ()->load ( 'operator', 'connDatabase1' );
		$operatorId = $model_operator->getOperatorId ( $operator_name );
		
		$mPushProject = loader_model::getInstance ()->load ( 'pushproject', 'connBroadcast' );
		$pushproject_data = new model_data_pushproject ();
		//$pushproject_data->status = '0';
		$pushproject_data->created = date ( 'Y-m-d' );
		$pushproject_data->oprid = $operatorId;
		$pushProjects = $mPushProject->get ( $pushproject_data );
		
		if ($pushProjects === false) {
			$log->write ( array ('level' => 'info', 'message' => "Data project not found" ) );
			return false;
		}
		
		// push projects
		$services = array();
		$pid_services = array();		
		foreach ( $pushProjects as $pushProject ) {
			array_push($services, $pushProject ['service']);
			$pid_services[$pushProject['service']] = $pushProject ['pid'];	
		}
		
		if (in_array($service, $services)){
			$log->write ( array ('level' => 'error', 'message' => "Worker #{$thread_id} is spawing..." ) );
			//$this->processSendToQueue ( $operator_name, $pushProject ['pid'], $service, true, $thread_id );
			//below line changed by fajar, include in model push_buffer to respect number of message limit
			$this->processSendToQueue ( $operator_name, $pid_services[$service], $service, true, $thread_id, $limit );								
		}		
		return true;
	}
	
	public function processSendToQueue($operator_name, $pushProject_pid, $service_name, $forking = false, $thread_id = 0, $limit=1000) {
		
		$main_config = loader_config::getInstance ()->getConfig ( 'main' );

		$log = manager_logging::getInstance ();
		
		if ($forking) {
			$lockPath = '/tmp/lock_default_broadcast_queue_' . $operator_name . '_' . $pushProject_pid . '_' . $service_name . '_' . $thread_id;
		} else {
			$lockPath = '/tmp/lock_default_broadcast_queue_' . $operator_name . '_' . $pushProject_pid . '_' . $service_name;
		}
		
		if (file_exists ( $lockPath )) {
			$log->write ( array ('level' => 'error', 'message' => "Lock File Exist on : " . $lockPath ) );
			echo "NOK - Lock File Exist on $lockPath \n";
			exit ( 0 );
		} else {
			touch ( $lockPath );
		}
		
		$model_operator = loader_model::getInstance ()->load ( 'operator', 'connDatabase1' );
		$operatorId = $model_operator->getOperatorId ( $operator_name );
		
		$mPushProject = loader_model::getInstance ()->load ( 'pushproject', 'connBroadcast' );
		
		$log->write ( array ('level' => 'debug', 'message' => " Slot [$operator_name - $operatorId] : [" . $pushProject_pid . "][" . $service_name . "][" . $thread_id . "]" ) );
		$model_user = loader_model::getInstance ()->load ( 'pushbuffer', 'connBroadcast' );
		$buffers = $model_user->execPushbufferWithThread ( $pushProject_pid, $service_name, $operatorId, $thread_id, $limit );
		
		// Create & check locking DR service cicilan 1 - 6
		if(in_array($service_name,$main_config->listServiceCicilan))
		{
			$revCheck = loader_model::getInstance()->load('revcheck', 'connDatabase1');

			// Set variable lock DR name 
			$lockdr = '/app/xmp2012/buffers/xlsdp/lock_dr_'.$service_name;

			// Execute using shell function checking locking DR file
			$checkLockDR = shell_exec('ll '.$lockdr.'* | tail -1');

			// If it not empty then exit 'this' process
			if(!empty($checkLockDR)) {
				$log->write( array ('level' => 'error', 'message' => "Lock File Exist on : " . $checkLockDR ) );
				echo "NOK - Lock File Exist on $checkLockDR \n";
				exit ( 0 );
			} else {
				// Get last msisdn from push_buffer getter limit using array data
				$getLastMsisdn = $buffers[count($buffers)-1]['dest'];
				// Then create locking following with last msisdn
				if(!file_exists($lockdr.'_'.$getLastMsisdn)) touch($lockdr.'_'.$getLastMsisdn);
			}
		}

		if ($buffers !== false) {
			$processed = 0;
			foreach ( $buffers as $pushBuffer ) {
				$log->write ( array ('level' => 'debug', 'message' => " Update stat : ON_PROCESS" ) );
				$pushbuffer_data = new model_data_pushbuffer ();
				$pushbuffer_data->id = $pushBuffer ['id'];
				
				/*				
				$pushbuffer_data->stat = "ON_PROCESS";
				$model_user->update ( $pushbuffer_data );
				*/
				
				$mt_data = unserialize ( $pushBuffer ['obj'] );
				$mt_data->pushBufferId = $pushBuffer ['id'];

				// Check Revenue Limit On Service cicilan 1 - 6
				$over_limit = false;

				// Get & check list service cicilan 1 - 6
				if(in_array($service_name,$main_config->listServiceCicilan))
				{
					// Check instance model revcheck
					if($revCheck === false)
					{
						$revCheck = loader_model::getInstance()->load('revcheck', 'connDatabase1');
					}
//echo $pushbuffer_data['subject'];
					// Split & get current subject
					$splitSubject = explode(";", $pushBuffer['subject']);
				
					$mainSubject = "NORMAL";
					if(count($splitSubject) > 4) $mainSubject = strtoupper($splitSubject[4]);

					// Check default & current revenue current subject
					$cur_revenue = $revCheck->getDpRevByRaw($service_name, $mainSubject);
					$default_revenue = (int)$cur_revenue[0]['dpdrev'];
					$current_dp_revenue = (int)$cur_revenue[0]['dprev'];

					// Decide is it overlimit or not
					if($current_dp_revenue >= $default_revenue) $over_limit = true;

					// If its not overlimit then hit to telco
					if(!$over_limit)
					{
						$mt_processor = new manager_mt_processor ();
						$queue = $mt_processor->saveToQueue ( $mt_data );
					}
				}
				else
				{
					$mt_processor = new manager_mt_processor ();
					$queue = $mt_processor->saveToQueue ( $mt_data );
				}

				// default over_limit variable for service cicilan 1 - 6 and 
				// if it overlimit then update stat to 'OVERLIMIT' value

				if($over_limit)
				{
					$log->write ( array ('level' => 'debug', 'message' => " Update stat : OVERLIMIT" ) );
					$pushbuffer_data->stat = "OVERLIMIT";
					if ($model_user->update ( $pushbuffer_data )) {
						$processed ++;
					}
				}
				else
				{
					if ($queue === false) {
						$log->write ( array ('level' => 'debug', 'message' => " Update stat : PENDING" ) );
						$pushbuffer_data->stat = "PENDING";
						$model_user->update ( $pushbuffer_data );
					} else {
						$log->write ( array ('level' => 'debug', 'message' => " Update stat : PUSHED" ) );
						$pushbuffer_data->stat = "PUSHED";
						if ($model_user->update ( $pushbuffer_data )) {
							$processed ++;
						}
					}
				}
			}
			
			$pushproject_data = new model_data_pushproject ();
			$pushproject_data->pid = $pushProject_pid;
			$pushproject_data->processed = $processed;
			$pushproject_data->status = '1';
			
			$mPushProject->update ( $pushproject_data );
		}
		unlink ( $lockPath );
		return true;
	}
	
	public function resetSchedule() {
		$log = manager_logging::getInstance ();
		$log->write ( array ('level' => 'debug', 'message' => "Start" ) );
		
		$broadcast = new manager_broadcast ();
		$schedules = $broadcast->populateSchedule ( '2' );
		
		if (count ( $schedules ) > 0) {
			foreach ( $schedules as $broadcast_data ) {
				$model_schedule = loader_model::getInstance ()->load ( 'schedule', 'connBroadcast' );
				if ($broadcast_data->recurringType > 0)
					$model_schedule->reset ( $broadcast_data );
			}
		}
		return true;
	}

}
