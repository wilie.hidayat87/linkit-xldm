<?php

class config_logging {

    public $timeDigit = 8;
    //public $lineFormat = '{datetime} {operator} {msgId} {msisdn} {message} {subject} {threadId} {exectime} {response} {level} {file} {class} {function} {line}'; //full
    public $lineFormat = "{uniqueId} {level} {datetime} {exectime} {class} {function} {message} {response}";
//    public $loglevel = 0; //2;
    public $loglevel = 2;
    
    public $profile = array(
        'default' => array(
            'path' => '/Logs2/xlsdp/default',
            'type' => 'file',
            'filename' => 'default'
        ),
        'mo_receiver' => array(
            'path' => '/Logs2/xlsdp/mo_receiver',
            'type' => 'file',
            'filename' => 'mo_receiver'
        ),
        'mo_processor' => array(
            'path' => '/Logs2/xlsdp/mo_processor',
            'type' => 'file',
            'filename' => 'mo_processor'
        ),
        'mt_processor' => array(
            'path' => '/Logs2/xlsdp/mt_processor',
            'type' => 'file',
            'filename' => 'mt_processor'
        ),
	'mt_processor_2' => array(
            'path' => '/Logs/xlsdp/mt_processor',
            'type' => 'file',
            'filename' => 'mt_processor_2'
        ),
        'mo_subscriber' => array(
            'path' => '/Logs/xlsdp/mo_subscriber',
            'type' => 'file',
            'filename' => 'mo_subscriber'
        ),
        'broadcast' => array(
            'path' => '/Logs2/xlsdp/broadcast',
            'type' => 'file',
            'filename' => 'broadcast'
        ),
        'call_center' => array(
            'path' => '/Logs/xlsdp/callcenter',
            'type' => 'file',
            'filename' => 'callcenter'
        ),
        'dr_receiver' => array(
            'path' => '/Logs2/xlsdp/dr_receiver',
            'type' => 'file',
            'filename' => 'dr_receiver'
        ),
        'dr_processor' => array(
            'path' => '/Logs2/xlsdp/dr_processor',
            'type' => 'file',
            'filename' => 'dr_processor'
        ),
        'dr_updater' => array(
            'path' => '/Logs/xlsdp/dr_updater',
            'type' => 'file',
            'filename' => 'dr_updater'
        ),
		'retry_processor' => array(
            'path' => '/Logs/xlsdp/retry_processor',
            'type' => 'file',
            'filename' => 'retry_processor'
        ),
    	'sdp_request'=> array(
            'path' => '/Logs/xlsdp/sdp_request',
            'type' => 'file',
            'filename' => 'sdp_request'
    	),
        'cmp_processor'=> array(
            'path' => '/Logs/xlsdp/cmp_processor',
            'type' => 'file',
            'filename' => 'cmp_processor'
	)
    );
    
}
