<?php

class config_database {

    public $profile = array(
		'cmp' =>
        array(
            'username' => 'cmp',
            'password' => 'c123p',
            'host' => 'DB',
            'database' => 'cmp',
            'driver' => 'mysql'
        ),
        'connDatabase1' =>
        array(
            'username' => 'xmp',
            'password' => 'password',
            'host' => 'DB',
            'database' => 'xmp',
            'driver' => 'mysql'
        ),
        'connBroadcast' =>
        array(
            'username' => 'push',
            'password' => 'password',
            'host' => 'DB',
            'database' => 'dbpush',
            'driver' => 'mysql'
        ),
        'push' =>
        array(
            'username' => 'push',
            'password' => 'password',
            'host' => 'DB',
            'database' => 'dbpush',
            'driver' => 'mysql'
        ),
        'reports' =>
        array(
            'username' => 'reports',
            'password' => 'r3p0rts',
            'host' => 'DB',
            'database' => 'reports',
            'driver' => 'mysql'
        ),
        'connWap' =>
        array(
            'username' => 'wap',
            'password' => '123456',
            'host' => 'DB',
            'database' => 'wap',
            'driver' => 'mysql'
        ),
        'connDr' =>
        array(
            'username' => 'cdr',
            'password' => '123456',
            'host' => 'DB',
            'database' => 'cdr',
            'driver' => 'mysql'
        ),
        'connCrepo' =>
        array(
            'username' => 'newcrepo',
            'password' => 'p455cr3p0',
            'host' => 'DB',
            'database' => 'contentrepo',
            'driver' => 'mysql'
        ),
        'connHadoop' =>
        array(
            'driver' => 'hadoop'
        ),
        'crossDB' =>
        array(
            'username' => 'rootall',
            'password' => 'pu5k0mxxx',
            'host' => 'DB',
            'database' => 'xmp',
            'driver' => 'mysql'
        )
    );

}
