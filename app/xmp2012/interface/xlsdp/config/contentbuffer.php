<?php
class config_contentbuffer {
	public $use_forking = 1;
	public $bufferPath = '/app/xmp2012/buffers/xlsdp/contentBuffer';
	public $bufferSlot = 10;
	public $returnCode = array ('OK' => 'OK', 'NOK' => 'NOK' );
	public $bufferThrottle = 10;
}
?>
