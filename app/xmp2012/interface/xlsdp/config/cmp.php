<?php
	class config_cmp {
		public $use_forking = 0;
		public $partner = array('mobusi'=>1,'artofclick'=>1,'kimia'=>1,'cd'=>1,'adsterra'=>1,'kissmyads'=>1,'mobipium'=>1,'pocketmedia'=>1,'mobvista'=>1,'mobok'=>1,'bmb'=>1,'sechuan'=>1,'avenue'=>1,'slimspot'=>1,'avazu'=>1,'dlt'=>1,'isj'=>1,'kece'=>1);
		public $bufferPath = '/app/xmp2012/buffers/xlsdp/cmpBuffer';
    		public $bufferThrottle = '10';
 		public $bufferSlot = 10;
 		public $pixelLength = array('mobusi'=>23,
 					     'artofclick'=>0,
					     'kimia'=>55,
					     'cd'=>1,
					     'adsterra'=>156,
					     'kissmyads'=>30,
					     'mobipium'=>0);
		public $returnCode = array("OK"=>"OK", "NOK"=>"NOK");
	}

?>
