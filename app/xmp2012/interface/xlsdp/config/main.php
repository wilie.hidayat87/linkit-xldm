<?php

class config_main {

    public $operator = 'xlsdp';
    public $adn = '99876'; //'3143';
    public $channel = array('sms', 'web', 'wap', 'umb');
    public $partner = array('l7');
    public $prefix = 'xl';
    public $use_forking = true;
    public $request_type = array(
        'confirm' => array('confirm'),
        'reg' => array('reg'),
        'unreg' => array('unreg')
    );

	public $customSMSContent = array(
		'services' => array(
			'cicilanxl1',
			'cicilanxl2',
			'cicilanxl3',
			'videorazzi_register',
			'yoga_for_you',
			'cerpen',
			'cerpen_pull',
			'latino'
		),
		'urls' => array(
			'cicilanxl1'				=> "http://xlcicilan1.slypee.com/subscription_new?user=#USER#&pass=#PASSWORD#",
			'cicilanxl2'				=> "http://xlcicilan1.slypee.com/subscription_new?user=#USER#&pass=#PASSWORD#",
			'cicilanxl3'				=> "http://xlcicilan1.slypee.com/subscription_new?user=#USER#&pass=#PASSWORD#",
			'videorazzi_register'		=> "http://xl3.videorazzi.mobi/subscribe/subscribe/add_user?key=9737577deb7dd2fa49babab5b25b78a8",
			'yoga_for_you'				=> "http://callbacks.vastraf.tools/api/credentials/receive/#MSISDN#?secret=7e0b77764c624e67f8323688fc2ec111",
			'cerpen'					=> "http://xl.cerpen.mobi/subscription?user=#USER#&pass=#PASSWORD#&key=c26e78dfcb0801dd38ab11fb1eadfd4b&type=#TYPE#",
			'cerpen_pull'				=> "http://xl.cerpen.mobi/subscription?user=#USER#&pass=#PASSWORD#&key=c26e78dfcb0801dd38ab11fb1eadfd4b&type=#TYPE#",
			'latino'					=> "http://xlatino.slypee.com/mobile/notif?key=9737577deb7dd2fa49babab5b25b78a8&user=#USER#&status=#STATUS#&pass=#PASSWORD#",
			'videorazzi_register_tfl'	=> "http://149.129.252.221:8028/app/api/mofwd.php?sms=#ALLKEYWORD#&msisdn=#MSISDN#&op=xl&sdc=#SDC#",
			'videorazzi_register_tlf'	=> "http://149.129.252.221:8028/app/api/mofwd.php?sms=#ALLKEYWORD#&msisdn=#MSISDN#&op=xl&sdc=#SDC#"
		)
	);
	
	public $listServiceCicilan = array('cicilanXL1','cicilanXL2','cicilanXL3','cicilanXL4','cicilanXL5','cicilanXL6','cicilanXL7');
}
