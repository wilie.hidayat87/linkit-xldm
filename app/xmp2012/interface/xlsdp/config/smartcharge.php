<?php 
	class config_smartcharge {
		
		// number of attempt to send another charge
		public $allowed_attempt = 1; // only 1 attempt to send mt now .. 
		
		// dr response filtering for valid smartcharging
		public $dr_filtered = array(
			/*
			0 => 'NACK/0x00000500/Unknown/Reserved',
			1280 => 'NACK/0x00000500/Unknown/Reserved',
			*/
			1280,
		);
	}
	
?>
