<?php

class config_mt {
    /* server config */

    public $profile = array(
        'default' => array(
            'type' => 'activemq',
            'protocol' => 'tcp',
            'server' => '127.0.0.1',
            'port' => '61613',
            'prefix' => 'xlsdpMT',
            'slot' => '3',
            'retry' => '5',
            'sendUrl' => array('http://182.16.255.55:5013/cgi-bin/sendsms'),
            //'sendUrl' => array('http://149.129.247.220:5013/cgi-bin/sendsms'),
            'dr' => array (
            'dlr-url' => 'http://103.58.100.200:39791/dr/index.php',
            'dlr-mask' => 3
            ),
            'SendTimeOut' => '10',
            'throttle' => 3
        ),
        'text' => array(
            'type' => 'activemq',
            'protocol' => 'tcp',
            'server' => '127.0.0.1',
            'port' => '61613',
            'prefix' => 'xlsdpText',
            'slot' => '3',
            'retry' => '5',
            //'sendUrl' => array('http://182.16.255.55:5013/cgi-bin/sendsms'),
            'sendUrl' => array('http://149.129.247.220:5013/cgi-bin/sendsms'),
            'dr' => array (
            'dlr-url' => 'http://103.58.100.200:39791/dr/index.php',
   	    'dlr-mask' => 3
            ),
            'SendTimeOut' => '10',
            'throttle' => 3
        ),
        'push' => array(
            'type' => 'activemq',
            'protocol' => 'tcp',
            'server' => '127.0.0.1',
            'port' => '61613',
            'prefix' => 'xlsdpMTPush',
            'slot' => '3',
            'retry' => '5',
            'sendUrl' => array('http://182.16.255.55:5013/cgi-bin/sendsms'),
            'dr' => array (
            'dlr-url' => 'http://103.58.100.200:39791/dr/index.php',
            'dlr-mask' => 3
            ),
            'SendTimeOut' => '10',
            'throttle' => 3
        ),
        'wappush' => array(
            'type' => 'activemq',
            'protocol' => 'tcp',
            'server' => '127.0.0.1',
            'port' => '61613',
            'prefix' => 'xlsdpMTWapPush',
            'slot' => '3',
            'retry' => '5',
            'sendUrl' => array('http://182.16.255.55:5013/cgi-bin/sendsms'),
            'dr' => array (
            'dlr-url' => 'http://103.58.100.200:39791/dr/index.php',
            'dlr-mask' => 33
            ),
            'SendTimeOut' => '10',
            'throttle' => 3
        ),
        'optin' => array(
            'type' => 'activemq',
            'protocol' => 'tcp',
            'server' => '127.0.0.1',
            'port' => '61613',
            'prefix' => 'xlsdpMTOptin',
            'slot' => '3',
            'retry' => '5',
            'sendUrl' => array('http://182.16.255.55:5013/cgi-bin/sendsms'),
            'dr' => array (
	    'dlr-url' => 'http://103.58.100.200:39791/dr/index.php',
            'dlr-mask' => 31
            ),
            'SendTimeOut' => '10',
            'throttle' => 3
        ),
        'dailypush' => array(
            'type' => 'activemq',
            'protocol' => 'tcp',
            'server' => '127.0.0.1',
            'port' => '61613',
            'prefix' => 'xlsdpMTDailyPush',
            'slot' => '1',
            'retry' => '5',
            'sendUrl' => array('http://182.16.255.55:5013/cgi-bin/sendsms'),
            'dr' => array (
            'dlr-url' => 'http://103.58.100.200:39791/dr/index.php',
	    'dlr-mask' => 3
            ),
            'SendTimeOut' => '10',
            'throttle' => 3
        ),
        'delaypush' => array(
            'type' => 'activemq',
            'protocol' => 'tcp',
            'server' => '127.0.0.1',
            'port' => '61613',
            'prefix' => 'xlsdpMTDelayPush',
            'slot' => '3',
            'retry' => '5',
            'sendUrl' => array('http://182.16.255.55:5013/cgi-bin/sendsms'),
            'dr' => array (
            'dlr-url' => 'http://103.58.100.200:39791/dr/index.php',
            'dlr-mask' => 3
            ),
            'SendTimeOut' => '10',
            'throttle' => 3
        ),
        'delaywappush' => array(
            'type' => 'activemq',
            'protocol' => 'tcp',
            'server' => '127.0.0.1',
            'port' => '61613',
            'prefix' => 'xlsdpDelayWapPush',
            'slot' => '3',
            'retry' => '5',
            'sendUrl' => array('http://182.16.255.55:5013/cgi-bin/sendsms'),
            'dr' => array (
            'dlr-url' => 'http://103.58.100.200:39791/dr/index.php',
            'dlr-mask' => 3
            ),
            'SendTimeOut' => '10',
            'throttle' => 3
        )
    );

  public $service2map = array(
	//'fitri2_register',
	//'game'
	'push_test',
	'pull_test'
    );

   public $service3map = array(
	//'game1_register',
	//'SUKA_PULL',
	//'yatta',
	//'reg yatta'
	'push_test',
	'pull_test'
    );

   public $service4map = array(
	//'yatta'
	'push_test',
	'pull_test'
   );

}
