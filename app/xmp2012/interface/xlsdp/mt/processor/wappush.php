<?php

class proxl_mt_processor_wappush extends default_mt_processor_wappush {

    protected static $instance;

    private function __construct() {

    }

    public static function getInstance() {
        $log = manager_logging::getInstance ();
        $log->write(array('level' => 'debug', 'message' => "Start"));
        
        if (!self::$instance) {
            self::$instance = new self ();
        }
        return self::$instance;
    }

    public function saveToQueue($mt_data) {
        $log = manager_logging::getInstance ();
        $log->write(array('level' => 'debug', 'message' => "Start"));
        
        $shortname_model = loader_model::getInstance ()->load('shortname', 'connDatabase1');
        $shortname_return = $shortname_model->get($mt_data);
        $loader_config = loader_config::getInstance ();
        $tblmsgtransact_model = loader_model::getInstance ()->load('tblmsgtransact', 'connDatabase1');

        $mt_data->shortname = $shortname_return ['value'];
        $loader_queue = loader_queue::getInstance ();
        $content = $loader_config->getConfig('content');

        $udh = "\x06\x05\x04\x0B\x84\x23\xF0";
        $body = "\x1B\x06\x01\xAE\x02\x05\x6A\x00\x45\xC6\x0C\x03{$content->urlDownload}\x00\x01\x03{$mt_data->msgData}\x00\x01\x01";
        $binnary = tools_hexa::hex_encode($udh, $body);
        $main_config = $loader_config->getConfig('main');
        $mt_config = $loader_config->getConfig('mt');

        $profile = 'wappush';
        $slot = $mt_config->profile [$profile] ['slot'];
        $lengslot = strlen($slot);
        $last_num = substr($mt_data->msisdn, - $lengslot, $lengslot);
        $num_channel = $last_num % $slot;

        $queue_data = $mt_data->mo->msgId . "<TERRY>";
        $queue_data .= $mt_data->msisdn . "<TERRY>";
        $queue_data .= "+" . $mt_data->charging->chargingId . "<TERRY>";
        $queue_data .= $binnary . "<TERRY>";
        $queue_data .= $mt_data->shortname . "<TERRY><TERRY>";
        $queue_data .= $mt_data->mo->msgId;

        $queue = $loader_queue->load($profile);
        $data = new queue_data ();
        $data->channel = $mt_config->profile [$profile] ['prefix'] . $num_channel;
        $data->value = $queue_data;

        $result = $queue->put($data);

        if ($result === FALSE) {
            $mt_data->msgLastStatus = "failed";
            $mt_data->msgStatus = "failed";
        } else {
            $mt_data->msgLastStatus = "success";
            $mt_data->msgStatus = "success";
        }

        $this->saveMTToTransact($mt_data);

        return $result;
    }

}