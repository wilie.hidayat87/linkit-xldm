<?php

class xlsdp_mt_processor_text extends default_mt_processor_text {
	
	private static $instance;
	
	private function __construct() {
	
	}
	
	public static function getInstance() {
		if (! self::$instance) {
			self::$instance = new self ();
		}
		return self::$instance;
	}
	
	private function hex2bin($str) {
		$bin = "";
		$i = 0;
		do {
			$bin .= chr ( hexdec ( $str {$i} . $str {($i + 1)} ) );
			$i += 2;
		} while ( $i < strlen ( $str ) );
		return $bin;
	}
	
	public function saveToQueue($mt_data) {
		$log = manager_logging::getInstance ();
		$log->write ( array ('level' => 'debug', 'message' => "Start : " . serialize ( $mt_data ) ) );
		
		$shortname_model = loader_model::getInstance ()->load ( 'shortname', 'connDatabase1' );
		$shortname_return = $shortname_model->get( $mt_data );

		
		// If it's a Push Message or no TrxID found in MO , then the TRXID must be generated
		if ($mt_data->charging->messageType == 'mtpush' || (! isset ( $mt_data->mo->msgId ) && empty ( $mt_data->mo->msgId ))) {
			if (empty ( $mt_data->msgId ))
				$mt_data->msgId = date ( "YmdHis" ) . str_replace ( '.', '', microtime ( true ) );
		} // Otherwise, take from MO      
		else
			$mt_data->msgId = $mt_data->mo->msgId;
		
		//$mt_data->serviceid = $serviceid['serviceid'];
		$mt_data->shortname = $shortname_return ['value'];
		
		return $this->process ( $mt_data );
	}
	
	public function process($mt) {
		
		// check if mt define as smartcharge-mt
		$smartcharge_f = new smartcharge_functions ();
		$isSmartCharge = $smartcharge_f->isSmartCharge ( $mt );
		if ($isSmartCharge) {
			$this->smartcharge_mt ( $mt, $isSmartCharge );
		} else {
			$this->non_smartcharge ( $mt );
		}
	
	}
	
	public function smartcharge_mt($mt_data, $smartchargeInfo) {
		
		// store in sc table
		$smartcharge_data = new model_data_smartchargetransact ();
		$smartcharge_data->msisdn = $mt_data->msisdn;
		$smartcharge_data->tid = $mt_data->msgId;
		$smartcharge_data->sid = $mt_data->serviceId;
		$smartcharge_data->next_sid = $smartchargeInfo ['next_sid'];
		$smartcharge_data->data = serialize ( $mt_data );
		$smartcharge_data->service = $smartchargeInfo ['service'];
		$smartcharge_data->operator_id = $smartchargeInfo ['operator_id'];
		$smartcharge_data->subject = $smartchargeInfo ['subject'];
		$smartcharge_data->attempt = 0;
		$smartcharge_data->status = 1;
		
		// save
		$smartcharge_f = new smartcharge_functions ();
		$isSmartCharge = $smartcharge_f->save_smartcharge_mt ( $smartcharge_data );
		
		// send mt
		$mt_data->subject = $smartchargeInfo ['subject'];
		$this->send_mt ( $mt_data, true, $smartchargeInfo );
	}
	
	public function non_smartcharge($mt) {
		if($mt->service != 'ERROR')
		$this->send_mt ( $mt );
	}
	
	public function send_mt($mt, $smartcharge = false, $smartcharge_info = false) {
		$log = manager_logging::getInstance ();
		if ($smartcharge)
			$log->write ( array ('level' => 'debug', 'message' => "Smartcharge MT." ) );
		else
			$log->write ( array ('level' => 'debug', 'message' => "Non Smartcharge MT." ) );
		
		$loader_config = loader_config::getInstance ();
		$configMT = $loader_config->getConfig ( 'mt' );
		$configMain = $loader_config->getConfig ( 'main' );
		$profile = 'text';
		
		$param = 'username=user';
		$param .= '&password=password';
		$param .= '&smsc=XLSDP';
		$param .= '&from=' . $mt->charging->chargingId;
		//$param .= '&charset=UCS-2';
		//$param .= '&coding=2';
		$param .= '&to=' . $mt->msisdn;
		
		//$param .= '&text=' . urlencode ( $mt->msgData );
		//$sms = urlencode($mt->msgData);
		$sms = $mt->msgData;
		
		
		//******* VIDEORAZZI CUSTOM SMS **********//
		//******* Dev By : Wilie Wahyu H
		//******* Created: 2017-03-30
		$sms = $this->hitCustomSMS($mt, $sms);
		//******* END ****************************//
		
		$sms = urlencode($sms);
		$param .= '&text=' . $sms;		
		
		$metadata = '';
		
		if ($mt->shortname)
			$metadata .= 'Shortname=' . urlencode ( $mt->shortname ) . '&';
		
		//if ($mt->msgId) $metadata .= 'TX_ID='.urlencode($this->hex2bin($mt->msgId));
		
		if ($mt->msgId && strpos ( strtoupper ( $mt->subject ), 'MT;PULL;' ) !== FALSE) {
			$metadata .= 'TX_ID=' . urlencode ($mt->msgId );
			//$metadata .= '&serviceid=' . urlencode ($mt->serviceId);
		}
		
		if (strpos ( strtoupper ( $mt->subject ), 'MT;PUSH;' ) !== FALSE) {
			//$metadata .= 'serviceid=' . urlencode ($mt->serviceid);
		}
		
		if ($metadata)
			$param .= '&meta-data=' . urlencode ( '?smpp?' . $metadata );
		
		$param .= '&dlr-mask=' . $configMT->profile [$profile] ['dr'] ['dlr-mask'];
		$param .= '&dlr-url=' . urlencode ( $configMT->profile [$profile] ['dr'] ['dlr-url'] . '?txid=' . $mt->msgId . '&status=%d&answer=%A&ccode=%P&msisdn=%p&ts=%t&adn=' . $mt->adn .'&meta-data=%D&mdlr=%B&ErrorCode=%B&ErrorSource=%B' );
		if ($smartcharge)
			$param .= urlencode ( '&sc=1&nc=' . $smartcharge_info ['next_sid'] );
		
		$url = $configMT->profile [$profile] ['sendUrl'] [0];
		
		if(strpos(strtoupper($mt->subject), 'MT;PUSH') !== FALSE)
		{
			$hit = http_request::get ( $url, $param, $configMT->profile [$profile] ['SendTimeOut'] );
			$log->write ( array ('level' => 'debug', 'message' => "Kannel Url:" . $url . '?' . $param . ', Result:' . $hit ) );
			
			$response = explode ( ':', trim ( $hit ) );
			
			$mt->msgLastStatus = 'DELIVERED';
			$configDr = loader_config::getInstance ()->getConfig ( 'dr' );
			if ($configDr->synchrounous === TRUE) {
				if ($response [0] == '1') {
					$mt->msgStatus = 'DELIVERED';
				} else {
					$mt->msgStatus = 'FAILED';
				}
				$mt->closeReason = $hit;
			} else {
				if ($response [0] != '1') {
					$mt->closeReason = $hit;
				}
			}
		}
		
		// if (!$smartcharge) $this->saveMTToTransact ( $mt ); // karena write ke rpt_mo juga jadi ga jadi krn nanti ga imbang, instead subject di rubah 
		$this->saveMTToTransact ( $mt );
		
		/*
		// sdp ada delay jadi kudu delat juga dari sini ngirim nya
		$mt_delay = new mt_delay_data ();
		$mt_delay->service = $mt->service;
		$mt_delay->adn = $mt->adn;
		$mt_delay->msisdn = $mt->msisdn;
		$mt_delay->obj = serialize($mt);
		
	
		$model_mtdelay = loader_model::getInstance()->load('mtdelay', 'connDatabase1');
		$model_mtdelay->add($mt_delay);	
		*/
		return true;
	
	}

	public function hitCustomSMS($mt, $sms)
	{
		$log = manager_logging::getInstance ();
		
		$loader_config = loader_config::getInstance ();
		$configMT = $loader_config->getConfig ( 'mt' );
		$configMain = $loader_config->getConfig ( 'main' );
		
		if(
			(strpos(strtoupper($mt->subject), 'MT;PULL') !== FALSE || 
			strpos(strtoupper($mt->subject), 'MT;PUSH') !== FALSE) && 
			in_array(strtolower($mt->service), $configMain->customSMSContent['services']))
		{
			$loader_model = loader_model::getInstance();
			$user = $loader_model->load('user', 'connDatabase1');
			
			$trxdata = new stdClass();
			$trxdata->msisdn=$mt->msisdn;
			$trxdata->active=0;
			$trxdata->created_date=date("Y-m-d H:i:s");
			$trxdata->modify_date=date("Y-m-d H:i:s");
			$trxdata->operator=$configMain->operator;
			$trxdata->service=$mt->service;
			$trxdata->password=rand(0001, 9999);
			$trxdata->sent=0;
			
			$rec = $user->getMembership($trxdata);
			
			if(count($rec) > 0)
				$user->updatePasswordMembership($trxdata);
			else
				$user->insertMembership($trxdata);
			
			if(strpos($mt->msgData, "userpass") !== FALSE)
			{
				$sms = str_replace("#userpass#", "user:".$trxdata->msisdn.",pwd:".$trxdata->password, $mt->msgData);
			}
			else
			{
				$sms = $mt->msgData;
			}

			$log->write(array('level' => 'debug', 'message' => 'Response hit password generator ['.$trxdata->msisdn.']: ' . $trxdata->password));
		}

		/*
		if(strpos(strtoupper($mt->subject), 'MT;PULL;SMS;TEXT') !== FALSE || strpos(strtoupper($mt->subject), 'MT;PUSH;SMS;TEXT') !== FALSE || strpos(strtoupper($mt->subject), 'MT;PUSH;SMS;DAILYPUSH') !== FALSE)
		{
			
				if($configMain->openCustomCerpen && (strtolower($mt->service) == "cerpen" ||  strtolower($mt->service) == "cerpen_pull"))
				{ 
					$loader_model = loader_model::getInstance();
					$user = $loader_model->load('user', 'connDatabase1');
					
					$trxdata = new stdClass();
					$trxdata->msisdn=$mt->msisdn;
					$trxdata->active=0;
					$trxdata->created_date=date("Y-m-d H:i:s");
					$trxdata->modify_date=date("Y-m-d H:i:s");
					$trxdata->operator=$configMain->operator;
					$trxdata->service=$mt->service;
					$trxdata->password=rand(0001, 9999);
					$trxdata->sent=0;
					
					$rec = $user->getMembership($trxdata);
					
					if(count($rec) > 0)
						$user->updatePasswordMembership($trxdata);
					else
						$user->insertMembership($trxdata);
					
					$sms = str_replace("#userpass#", "user:".$trxdata->msisdn.",pwd:".$trxdata->password, $mt->msgData);
					
					$log->write(array('level' => 'debug', 'message' => 'Response hit password generator ['.$trxdata->msisdn.']: ' . $trxdata->password));
				}

				if($configMain->openCustomVideorazzi && strtolower($mt->service) == "videorazzi_register" && (strpos(strtoupper($mt->subject), 'MT;PUSH;SMS;TEXT') !== FALSE || strpos(strtoupper($mt->subject), 'MT;PUSH;SMS;DAILYPUSH') !== FALSE))
				{
					$loader_model = loader_model::getInstance();
					$user = $loader_model->load('user', 'connDatabase1');
					
					$trxdata = new stdClass();
					$trxdata->msisdn=$mt->msisdn;
					$trxdata->active=0;
					$trxdata->created_date=date("Y-m-d H:i:s");
					$trxdata->modify_date=date("Y-m-d H:i:s");
					$trxdata->operator=$configMain->operator;
					$trxdata->service=$mt->service;
					$trxdata->password=rand(0001, 9999);
					$trxdata->sent=0;
					
					$rec = $user->getMembership($trxdata);
					
					if(strpos(strtoupper($mt->subject), 'MT;PUSH;') !== FALSE)
					{
						if(count($rec) > 0)
							$user->updatePasswordMembership($trxdata);
						else
							$user->insertMembership($trxdata);
						
						$sms = str_replace("#userpass#", "user:".$trxdata->msisdn.",pwd:".$trxdata->password, $mt->msgData);
					}
					
					$log->write(array('level' => 'debug', 'message' => 'Response hit password generator ['.$trxdata->msisdn.']: ' . $trxdata->password));
				}
				
				if($configMain->openCustomYGF && strtolower($mt->service) == "yoga_for_you" && (strpos(strtoupper($mt->subject), 'MT;PUSH;SMS;TEXT') !== FALSE || strpos(strtoupper($mt->subject), 'MT;PUSH;SMS;DAILYPUSH') !== FALSE))
				{
					$loader_model = loader_model::getInstance();
					$user = $loader_model->load('user', 'connDatabase1');
					
					$trxdata = new stdClass();
					$trxdata->msisdn=$mt->msisdn;
					$trxdata->active=0;
					$trxdata->created_date=date("Y-m-d H:i:s");
					$trxdata->modify_date=date("Y-m-d H:i:s");
					$trxdata->operator=$configMain->operator;
					$trxdata->service=$mt->service;
					$trxdata->password=rand(0001, 9999);
					$trxdata->sent=0;
					
					$rec = $user->getMembership($trxdata);
					
					if(strpos(strtoupper($mt->subject), 'MT;PUSH;') !== FALSE)
					{
						if(count($rec) > 0)
							$user->updatePasswordMembership($trxdata);
						else
							$user->insertMembership($trxdata);
						
						$sms = str_replace("#userpass#", "user:".$trxdata->msisdn.",pwd:".$trxdata->password, $mt->msgData);
					}
					
					$log->write(array('level' => 'debug', 'message' => 'Response hit password generator ['.$trxdata->msisdn.']: ' . $trxdata->password));
				}
				
				if(strtolower($mt->service) == "latino" && (strpos(strtoupper($mt->subject), 'MT;PUSH;SMS;TEXT') !== FALSE || strpos(strtoupper($mt->subject), 'MT;PUSH;SMS;DAILYPUSH') !== FALSE))
				{					
						if($configMain->latinoMember)
						{
							$loader_model = loader_model::getInstance();
							$user = $loader_model->load('user', 'connDatabase1');
							
							$now = date("Y-m-d H:i:s");
			
							$trxdata = new stdClass();
							$trxdata->user = $mt->msisdn;
							$trxdata->password = rand(0001, 9999);
							$trxdata->created_date = $now;
							$trxdata->modify_date = $now;
							$trxdata->status = 0;
							
							$user->saveLatinoMembership($trxdata);
							
							$sms = str_replace("#userpass#", "user:".$trxdata->user.",pwd:".$trxdata->password, $mt->msgData);
						}
					//}
				}
		}
		*/
		
		return $sms;
	}
}
