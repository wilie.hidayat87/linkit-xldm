<?php
$subject = (isset($_GET['subject']) ? $_GET['subject'] : "rpt_subject");
$sumdate = (isset($_GET['sumdate']) ? urldecode($_GET['sumdate']) : date("Y-m-d"));
$by = (isset($_GET['by']) ? $_GET['by'] : "UNREG");
$trx_backup_table = (isset($_GET['trx_backup_table']) ? $_GET['trx_backup_table'] : "tbl_msgtransact");

$rpt = mysql_connect('DB','reports','r3p0rts');
$db_selected1 = mysql_select_db('reports',$rpt);

$xmp = mysql_connect('DB','xmp','password');
$db_selected2 = mysql_select_db('xmp',$xmp);

$cmp = mysql_connect('DB','cmp','c123p');
$db_selected3 = mysql_select_db('cmp',$cmp);

switch($subject)
{
	case "rpt_operator" :
	
		$sql = "SELECT o.operator_alias AS Operator, s.sumdate AS SumDate, SUM(s.total) AS Total, SUM(s.total)*s.gross AS Revenue, DATE_FORMAT(NOW(), '%d/%m/%Y %h.%i %p') AS last_update FROM rpt_service2 s INNER JOIN tbl_operator o ON s.operator = o.id WHERE s.sumdate = '{$sumdate}' AND s.msgstatus = 'DELIVERED' AND s.subject <> 'MO;PULL;SMS;ERROR' AND s.gross <> 0;";

		$query = mysql_query($sql, $rpt);
		while($row = mysql_fetch_array($query)){
			$data[] = array(
				"operator" => $row['Operator']
			   ,"sumdate" => $row['SumDate']
			   ,"total" => $row['Total']
			   ,"revenue" => $row['Revenue']
			   ,"last_update" => $row['last_update']
			   );
		}
	
	break;
	
	case "rpt_service" :
	
		$sql = "SELECT o.operator_alias AS Operator, s.sumdate AS SumDate, o.shortcode AS SDC, s.service, CASE WHEN UPPER(SPLIT_STRING(s.subject,';',4)) = 'TEXT' THEN 'REG' ELSE UPPER(SPLIT_STRING(s.subject,';',4)) END AS MainSubject, CASE WHEN SPLIT_STRING(s.subject,';',5) IS NULL THEN SPLIT_STRING(s.subject,';',4) ELSE SPLIT_STRING(s.subject,';',5) END AS SubKeyword, s.gross AS price, SUM(s.total) AS Total, SUM(s.total)*s.gross AS Revenue, DATE_FORMAT(NOW(), '%d/%m/%Y %h.%i %p') AS last_update FROM rpt_service2 s INNER JOIN tbl_operator o ON s.operator = o.id WHERE s.sumdate = '{$sumdate}' AND s.msgstatus = 'DELIVERED' AND s.subject <> 'MO;PULL;SMS;ERROR' GROUP BY s.service, MainSubject, SubKeyword;";
	
		$query = mysql_query($sql, $rpt);
		while($row = mysql_fetch_array($query)){
			
			$service = $row['service'];
			$Sub_Keyword = strtoupper((string)$row['SubKeyword']);
			
			if($Sub_Keyword == 'TEXT')
				$subkeyword = "NORMAL";
			else
				$subkeyword = $row['SubKeyword'];
			
			$sql = "SELECT COUNT(DISTINCT s.msisdn) AS sub_active FROM subscription_extend ms JOIN subscription s ON ms.msisdn=s.msisdn WHERE s.active='1' AND s.service = '{$service}' AND ms.subject = '{$subkeyword}';";
			
			$sql = mysql_query($sql, $xmp);
			$subactive = mysql_fetch_array($sql);
			
			$data[] = array(
				"operator" => $row['Operator']
			   ,"sumdate" => $row['SumDate']
			   ,"sdc" => $row['SDC']
			   ,"service" => $service
			   ,"mainsubject" => $row['MainSubject']
			   ,"subkeyword" => ($subkeyword == 'NORMAL') ? '' : $subkeyword
			   ,"price" => $row['price']
			   ,"subactive" => (count($subactive) > 0) ? $subactive['sub_active'] : 0
			   ,"total" => $row['Total']
			   ,"revenue" => $row['Revenue']
			   ,"last_update" => $row['last_update']
			   );
		}
	
	break;
	
	case "rpt_subject" :
	
		if($by == "REG")
		{
			//$sql = "SELECT DISTINCT o.operator_alias AS operator, d.subject, IF(SPLIT_STRING(d.subject,';',4)='TEXT','REG','') as mainsubject, SPLIT_STRING(d.subject,';',5) as subkeyword, d.service, d.msgstatus, d.closereason, d.total, s.gross AS price, IF(d.msgstatus='DELIVERED',d.total*s.gross,0) AS revenue FROM rpt_detail d INNER JOIN tbl_operator o ON o.ID = d.operator INNER JOIN rpt_service2 s ON s.service = d.service AND s.subject = d.subject WHERE d.sumdate = '{$sumdate}' AND d.subject LIKE 'MT;PUSH;SMS;TEXT%' OR d.subject LIKE 'MT;PUSH;UMB;TEXT%';";
			
			$sql = "select s.sumdate, o.operator_alias AS operator, 'REG' AS mainsubject, SPLIT_STRING(s.subject,';',5) as subkeyword, s.service, s.msgstatus, d.closereason, IF(s.msgstatus='DELIVERED',s.total,d.total) AS total, s.gross AS price, IF(s.msgstatus='DELIVERED',(s.gross*d.total),'') AS revenue from rpt_service2 s INNER JOIN rpt_detail d ON s.sumdate = d.sumdate and s.subject = d.subject and s.service = d.service and d.msgstatus = s.msgstatus INNER JOIN tbl_operator o ON o.ID = d.operator WHERE s.sumdate = '{$sumdate}' and left(s.subject, 7) = 'MT;PUSH' AND SPLIT_STRING(s.subject,';',4) = 'TEXT';";
			
			//echo $sql;
			
			$query = mysql_query($sql, $rpt);
			if(mysql_num_rows($query) > 0)
			{
				while($row = mysql_fetch_array($query)){
					
					$data[] = array(
					   "sumdate" => $sumdate
					   ,"operator" => $row['operator']
					   ,"mainsubject" => $row['mainsubject']
					   ,"subkeyword" => $row['subkeyword']
					   ,"service" => $row['service']
					   ,"msgstatus" => $row['msgstatus']
					   ,"closereason" => $row['closereason']
					   ,"price" => $row['price']
					   ,"revenue" => $row['revenue']
					   ,"total" => $row['total']
					   );
				}
			}
		}
		
		if($by == "UNREG")
		{			
			//$sql = "SELECT IF(UPPER(o.name)='INDOSAT','ISAT','XLSDP') AS operator, 'UNREG' AS mainsubject, ms.subject As subkeyword, s.name AS service, t.msgstatus AS msgstatus, t.closereason AS closereason, count(distinct t.msisdn) as total FROM reply r INNER JOIN charging c ON r.charging_id = c.id INNER JOIN mechanism m ON m.id = r.mechanism_id INNER JOIN service s ON s.id = m.service_id INNER JOIN {$trx_backup_table} t ON t.serviceid = c.charging_id INNER JOIN subscription_extend ms ON ms.msisdn = t.msisdn INNER JOIN operator o ON o.id = t.operatorid WHERE t.subject='MO;PULL;SMS;HANDLERCREATOR' AND t.msgdata regexp 'UNREG|Unreg|unreg' AND date(t.msgtimestamp) = '{$sumdate}' GROUP BY operator, mainsubject, subkeyword, service, msgstatus, closereason;";
			
			$sql = "SELECT IF(UPPER(o.name)='INDOSAT','ISAT','XLSDP') AS operator, 'UNREG' AS mainsubject, ms.subject As subkeyword, t.service, t.msgstatus AS msgstatus, t.closereason AS closereason, count(distinct t.msisdn) as total FROM {$trx_backup_table} t INNER JOIN subscription_extend ms ON ms.msisdn = t.msisdn INNER JOIN operator o ON o.id = t.operatorid WHERE t.subject='MO;PULL;SMS;HANDLERCREATOR' AND t.msgdata regexp 'UNREG|Unreg|unreg' AND date(t.msgtimestamp) = '{$sumdate}' GROUP BY operator, mainsubject, subkeyword, service, msgstatus, closereason;";
			//echo $sql;die;
			$query = mysql_query($sql, $xmp);
			
			if(mysql_num_rows($query) > 0)
			{
				while($row = mysql_fetch_array($query)){
					
					$data[] = array(
					   "sumdate" => $sumdate
					   ,"operator" => $row['operator']
					   ,"mainsubject" => $row['mainsubject']
					   ,"subkeyword" => $row['subkeyword']
					   ,"service" => $row['service']
					   ,"msgstatus" => $row['msgstatus']
					   ,"closereason" => $row['closereason']
					   ,"price" => 0
					   ,"revenue" => 0
					   ,"total" => $row['total']
					   );
				}
			}
		}
		
		if($by == "DAILYPUSH")
		{
			//$sql = "SELECT subject, service, msgstatus, closereason, COUNT(id) AS total FROM tbl_msgtransact WHERE DATE(msgtimestamp) = '{$sumdate}' AND subject LIKE 'MT;PUSH;SMS;DAILYPUSH%' GROUP BY subject, service, msgstatus, closereason;";
			
			//$sql = "SELECT o.operator_alias AS operator, IF(SPLIT_STRING(d.subject,';',4)='DAILYPUSH','DAILYPUSH','') as mainsubject, SPLIT_STRING(d.subject,';',5) as subkeyword, d.service, d.msgstatus, d.closereason, d.total, s.gross AS price, IF(d.msgstatus='DELIVERED',d.total*s.gross,0) AS revenue  FROM rpt_detail d INNER JOIN tbl_operator o ON o.ID = d.operator INNER JOIN rpt_service2 s ON s.service = d.service AND s.subject = d.subject WHERE d.sumdate = '{$sumdate}' AND d.subject LIKE 'MT;PUSH;SMS;DAILYPUSH%' OR d.subject LIKE 'MT;PUSH;UMB;DAILYPUSH%';";
			
			$sql = "select s.sumdate, o.operator_alias AS operator, 'DAILYPUSH' AS mainsubject, SPLIT_STRING(s.subject,';',5) as subkeyword, s.service, s.msgstatus, d.closereason, IF(s.msgstatus='DELIVERED',s.total,d.total) AS total, s.gross AS price, IF(s.msgstatus='DELIVERED',(s.gross*d.total),'') AS revenue from rpt_service2 s INNER JOIN rpt_detail d ON s.sumdate = d.sumdate and s.subject = d.subject and s.service = d.service and d.msgstatus = s.msgstatus INNER JOIN tbl_operator o ON o.ID = d.operator WHERE s.sumdate = '{$sumdate}' and left(s.subject, 7) = 'MT;PUSH' AND SPLIT_STRING(s.subject,';',4) = 'DAILYPUSH';";
			
			$query = mysql_query($sql, $rpt);
			if(mysql_num_rows($query) > 0)
			{
				while($row = mysql_fetch_array($query)){
					
					$data[] = array(
					   "sumdate" => $sumdate
					   ,"operator" => $row['operator']
					   ,"mainsubject" => $row['mainsubject']
					   ,"subkeyword" => $row['subkeyword']
					   ,"service" => $row['service']
					   ,"msgstatus" => $row['msgstatus']
					   ,"closereason" => $row['closereason']
					   ,"price" => $row['price']
					   ,"revenue" => $row['revenue']
					   ,"total" => $row['total']
					   );
				}
			}
		}
		
		if($by == "LANDING")
		{
			$sql = "SELECT servicename, UPPER(operator) AS operator, COUNT(id) AS total FROM pixel_storage WHERE DATE(date_time) = '{$sumdate}' GROUP BY servicename, operator;";
		
			$query = mysql_query($sql, $cmp);
			if(mysql_num_rows($query) > 0)
			{
				while($row = mysql_fetch_array($query)){
					
					$data[] = array(
					   "sumdate" => $sumdate
					   ,"operator" => "XLSDP"
					   ,"mainsubject" => $by
					   ,"subkeyword" => ""
					   ,"service" => $row['servicename']
					   ,"msgstatus" => ''
					   ,"closereason" => ''
					   ,"price" => $row['price']
					   ,"revenue" => $row['Revenue']
					   ,"total" => $row['total']
					   );
				}
			}
		}
	
	break;
}

mysql_close($rpt);
mysql_close($xmp);
mysql_close($cmp);

echo json_encode($data);
exit();