<?php

require_once '/app/xmp2012/interface/xlsdp/xmp.php';

if (isset($_GET) && count($_GET) != 0) {
	$_GET['channel'] = 'wap';
    $moProcessor = new manager_mo_processor ( );
    echo $moProcessor->saveToFile($_GET);
	
	$sms = $_GET['sms'];
	//sendToCmpStorage($_GET);
	
	
	if(substr(strtolower($sms), 0, 3) == "reg")
	{
		sendToCmpStorage($_GET);
	/*
		$stringData = explode(' ', $_GET['sms']);
		$pixelcode_index = (intval(count($stringData))-1);
		$position_pc_index = $stringData[$pixelcode_index];
		if(strlen($position_pc_index) > 15) // DEFINE CPA FILTER THAT HAVE PIXEL CODE
			sendToCmp($_GET, $stringData);
	*/
	}
	

} else {
    echo 'NOK';
}

//http://182.16.255.18:39791/mo/index.php?smscid=XLMOMTXLSDP&msisdn=1549060757&to=99228&sms=reg+gosip+1293812983912839123&trx_date=2015-09-15+05:14:20&ktx_id=1442294060&trx_id=%X&meta=%3Fsmpp%3FShortname%3D0047192000014705%26

function sendToCmpStorage($GET) {
  if(strpos(strtolower($GET['sms']), "unreg") === false || strpos(strtolower($GET['sms']), "confirm") === false){
    list($serv,$servicename) = explode(" ", trim($GET['sms']));
    
    $data = array(
    	 "msisdn"	=> $GET['msisdn']
	,"service"	=> strtolower($servicename)
	,"operator"	=> "xlsdp"	
    );
    
    ob_start();
    print_r($data);
    $log = ob_get_clean();

    error_log("/mo/index.php " . date("Y-m-d H:i:s") . " Start CMP : " . $log . PHP_EOL, 3, "/app/xmp2012/logs/xlsdp/cpa/cpa-" . date("Y-m-d"));

    $sPixel = new xlsdp_cmp_manager_keyword();
	$sPixel->process_pixel($data);
  }
}

function sendToCmp($GET, $stringData) {
	if(count($arrmbid = $stringData) > 2) {
		
		$count = count($stringData);
		
		$plain_service = "";
		for($x=0;$x<$count;$x++)
		{
			if($x != (intval($count)-1))
				$plain_service .= $stringData[$x] . " ";
		}
		$plain_service = trim($plain_service);

		$objSrv = loader_model::getInstance()->load('service', 'connDatabase1');
		
		// $log = manager_logging::getInstance ();
		// $log->write(array('level' => 'debug', 'message' => "Start"));
		// $log->write(array('level' => 'info', 'message' => "Info : ". $plain_service));
				
		$service = $objSrv->getServices($plain_service);

		// if(strpos(strtolower($GET['sms']),'reg gosip')!==FALSE) {
		if($service[0]['name'] != "") {
			$config_cmp = loader_config::getInstance()->getConfig('cmp');

			/* script ini untuk validasi kimia */
			// if(preg_match('/^g/',strtolower($arrmbid[2]))){
			
			//error_log($_SERVER['REQUEST_URI']." : ".$arrmbid[2] . PHP_EOL,"3","/tmp/log_uri_mobipium");

			if(strpos($arrmbid[2], "§") != false) // underscore char
					$arrmbid[2] = str_replace("§","_",$arrmbid[2]);

			if(strpos($arrmbid[2], "_") !== false) {
				$data['partner']='mobipium';
			}
			else
			{
				if(strlen($arrmbid[2]) == $config_cmp->pixelLength['kimia']) {
					$data['partner']='kimia';
				}
				else if(strlen($arrmbid[2]) == $config_cmp->pixelLength['adsterra']) {
					$data['partner']='adsterra';
				}
				else if(strlen($arrmbid[2]) == $config_cmp->pixelLength['kissmyads']) {
					$data['partner']='kissmyads';
				}
				else if(strlen($arrmbid[2]) == $config_cmp->pixelLength['mobusi']) {
					$data['partner']='mobusi';
				}
				else {
					if(preg_match('/^cd/',strtolower($arrmbid[2]))) {
						$data['partner']='cd';
						$arrmbid[2] = str_replace('cd','',$arrmbid[2]);
					}
				}
			}
				$data['id']=$arrmbid[2];
				$data['msisdn'] = $GET['msisdn'];
				$data['instid']=$GET['msisdn'];
				$data['channel']=$GET['channel'];
				$data['meta']=$GET['meta'];
				
				if(isset($config_cmp->partner[$data['partner']]) && $config_cmp->partner[$data['partner']]==1) {
						$cmp_manager = new manager_cmp_processor();
						// $data['service']='gosip';
						//echo($service[0]['name']);
						$data['service'] = strtolower($service[0]['name']);
						$data['adn']=$GET['to'];

						//$cmp_manager->process($data); // OLD
						$cmp_manager->saveToBuffer($data); // NEW using buffer
				}
		}
	}

}
