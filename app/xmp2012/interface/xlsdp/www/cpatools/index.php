<?php
	//header('Access-Control-Allow-Origin: http://50.57.69.113');  
//	header('Access-Control-Allow-Origin: *');
//	header('Access-Control-Allow-Methods: GET, POST');
//	header("Access-Control-Allow-Headers: X-Requested-With");
// ----------------------------WAKI-------------------------
	$action = $_GET['act'];
	// CONNECT TO DB   
	require_once('db.php');
	if(isset($_GET['limit'])){
		if(trim($_GET['limit'])!='' && is_numeric($_GET['limit'])){
			define('LIMIT',$_GET['limit']);	
		} else {
			define('LIMIT',30);	
		}
	} else {
		define('LIMIT',30);	
	}
	
	switch($action){
		case 'operator_list':
			$sql = "select distinct operator_name from hset ";
			$result = mysql_query($sql);
			$info = array();
			while($d=mysql_fetch_array($result,MYSQL_ASSOC)){
				$info[] = $d;	
			}
			echo json_encode($info);
		break;
		case 'publisher_list':
			if(isset($_GET['operator'])){
				$operator = mysql_escape_string($_GET['operator']);
				$sql = "select distinct keyword as publisher,operator_name from hset where operator_name='$operator' ";
			} else {
				$sql = "select distinct keyword as publisher,operator_name from hset";	
			}
			
			$result = mysql_query($sql);
			$info = array();
			while($d=mysql_fetch_array($result,MYSQL_ASSOC)){
				$info[] = $d;	
			}
			echo json_encode($info);
		break;
		case 'service_list':
			$operator = mysql_escape_string($_GET['operator']);
			$publisher = mysql_escape_string($_GET['publisher']);
			$sql = "select distinct keyword as publisher,operator_name,service from hset where operator_name='$operator' 
					and keyword='$publisher'";
			$result = mysql_query($sql);
			$info = array();
			while($d=mysql_fetch_array($result,MYSQL_ASSOC)){
				$info[] = $d;	
			}
			echo json_encode($info);
		break;
		case 'view_hset':
		
			$where = "";
			
			$operator = mysql_escape_string(trim($_GET['operator']));
			$publisher = mysql_escape_string(trim($_GET['publisher']));
			$service = mysql_escape_string(trim($_GET['service']));
			
			if($operator!=''){
				$where .= " and operator_name='$operator' ";	
			}
			
			if($publisher!=''){
				$where .= " and keyword='$publisher' ";	
			}
			
			if($service!=''){
				$where .= " and service='$service' ";	
			}
			
			$sql = "select * from hset where active=1 $where order by keyword asc";
			$result = mysql_query($sql);
			$info = array();
			while($d=mysql_fetch_array($result,MYSQL_ASSOC)){
				
				// HSET URL
				$sql = "select * from hset_url where hsetId='{$d['id']}'";
				$result2 = mysql_query($sql);
				$info2 = mysql_fetch_array($result2,MYSQL_ASSOC);
				if($info2){
					$d['url'] = $info2['url'];
				} else {
					$d['url'] = '';	
				}
				$info[] = $d;	
			}
			echo json_encode($info);
		break;
		case 'hset_info':
			$id = mysql_escape_string($_GET['id']);
			$sql = "select * from hset where active=1 and id='$id'";
			$result = mysql_query($sql);
			$info = mysql_fetch_array($result,MYSQL_ASSOC);
			
			// HSET URL
			$sql = "select * from hset_url where hsetId='$id'";
			$result2 = mysql_query($sql);
			$info2 = mysql_fetch_array($result2,MYSQL_ASSOC);
			$info['url'] = $info2['url'];
			
			echo json_encode($info);
		break;
		case 'edit_url':
			$id = mysql_escape_string($_GET['id']);
			$url = mysql_escape_string($_GET['_url']);
			 
			$sql = "delete from hset_url where hsetId='$id'";
			mysql_query($sql);
			
			$sql = "insert into hset_url(hsetId,url)values('$id','$url')";
			echo $sql;
			mysql_query($sql);
			
		break;
		case 'edit_ratio':
			$id = mysql_escape_string($_GET['id']);
			$ratio = mysql_escape_string($_GET['ratio']);
			$sql = "update hset set counter='$ratio',inc=0 where id='$id' and active=1 ";
			
			if(mysql_query($sql)) {
				echo json_encode(array('status'=>'OK','reason'=>''));
			} else {
				echo json_encode(array('status'=>'NOK','reason'=>mysql_error()));	
			}
			break;
		break;
		case 'view_report':
			$page = $_GET['page'];
			$start = $_GET['start'];
			$end = $_GET['end'];
			$startTime = $_GET['startTime'];
			$endTime = $_GET['endTime'];
			
			$where = "";
			
			if($startTime!=''){
				$startTime .= ':00';	
			}
			if($endTime!=''){
				$endTime .= ':59';	
			}
			
			$operator = mysql_escape_string(trim($_GET['operator']));
			$publisher = mysql_escape_string(trim($_GET['publisher']));
			$service = mysql_escape_string(trim($_GET['service']));
			
			if($operator!=''){
				$where .= " and operator_name='$operator' ";	
			}
			
			if($publisher!=''){
				$where .= " and keyword='$publisher' ";	
			}
			
			if($service!=''){
				$where .= " and service='$service' ";	
			}
			 
			if(!isset($page)){
				$page = 1;	
			}
			
			// CONSTRUCT DATE INTO MYSQL FORMAT dd/mm/yyyy => yyyy/mm/dd
			if($start!=''){
				$arrStart = explode('/',$start);
				$start = mysql_escape_string($arrStart[2].'-'.$arrStart[1].'-'.$arrStart[0]);
			}
			
			if($end!=''){
				$arrEnd = explode('/',$end);
				$end = mysql_escape_string($arrEnd[2].'-'.$arrEnd[1].'-'.$arrEnd[0]);
			}
			$info = array();
			
			if($start=='' && $end==''){
				$sql = "select a.hset_id,
						b.keyword,
						b.service,b.adn,a.date_send,operator_name,
						DATE_FORMAT(a.date_send,'%e/%m/%Y') as formatted_date_send,COUNT(1) as received,sum(if(status=1,1,0)) send 
						from hmo a join hset b on a.hset_id=b.id where closereason<>'not unique' " . $where;	
				$sqlCount = "select b.id from hmo a join hset b on a.hset_id=b.id where closereason<>'not unique' " . $where;		
			} else
			if($start=='' && $end!=''){
				$queryDate = " STR_TO_DATE(CONCAT(a.date_send,' ',a.time_send),'%Y-%m-%d %T')<=
							   STR_TO_DATE(CONCAT('$end',' ','$endTime'),'%Y-%m-%d %T') ";
							   
				$sql = "select a.hset_id,
						b.keyword,
						b.service,b.adn,a.date_send,operator_name,
						DATE_FORMAT(a.date_send,'%e/%m/%Y') as formatted_date_send,COUNT(1) as received,sum(if(status=1,1,0)) send 
						from hmo a join hset b on a.hset_id=b.id where closereason<>'not unique' and
						$queryDate " . $where;
				$sqlCount = "select b.id N from hmo a join hset b on a.hset_id=b.id where closereason<>'not unique' and
							$queryDate " . $where;			
			} else
			if($start!='' && $end==''){
				$queryDate = " STR_TO_DATE(CONCAT(a.date_send,' ',a.time_send),'%Y-%m-%d %T')>=
							   STR_TO_DATE(CONCAT('$start',' ','$startTime'),'%Y-%m-%d %T') ";
							   
				$sql = "select a.hset_id,
						b.keyword,
						b.service,b.adn,a.date_send,operator_name,
						DATE_FORMAT(a.date_send,'%e/%m/%Y') as formatted_date_send,COUNT(1) as received,sum(if(status=1,1,0)) send 
						from hmo a join hset b on a.hset_id=b.id where closereason<>'not unique' and
						$queryDate " . $where;
				$sqlCount = "select b.id from hmo a join hset b on a.hset_id=b.id where closereason<>'not unique' and
						$queryDate " . $where;		
			} else
			if($start!='' && $end!=''){
				$querydateindex = " and a.date_send between '$start' and '$end' ";
				$queryDate = " STR_TO_DATE(CONCAT(a.date_send,' ',a.time_send),'%Y-%m-%d %T')>=
							   STR_TO_DATE(CONCAT('$start',' ','$startTime'),'%Y-%m-%d %T') AND
							   STR_TO_DATE(CONCAT(a.date_send,' ',a.time_send),'%Y-%m-%d %T')<=
							   STR_TO_DATE(CONCAT('$end',' ','$endTime'),'%Y-%m-%d %T')".$querydateindex;
							   
				$sql = "select a.hset_id,
						b.keyword,
						b.service,b.adn,a.date_send,operator_name,
						DATE_FORMAT(a.date_send,'%e/%m/%Y') as formatted_date_send,COUNT(1) as received,sum(if(status=1,1,0)) send 
						from hmo a join hset b on a.hset_id=b.id where closereason<>'not unique' and
						$queryDate " . $where;
				$sqlCount = "select b.id from hmo a join hset b on a.hset_id=b.id where closereason<>'not unique' and
						$queryDate " . $where;				
			}
			
			$sqlCount = "select COUNT(1) N from($sqlCount group by a.hset_id,a.date_send) T";
			$r = mysql_query($sqlCount);
			$d = mysql_fetch_array($r,MYSQL_ASSOC);
			$count = $d['N'];
			
			$limit = LIMIT;
			$sqlLimit = 'limit '.(($page-1)*$limit).','.$limit;
			
			$sql1 = $sql . " group by a.hset_id,a.date_send order by a.date_send desc " . $sqlLimit;
			$r = mysql_query($sql1);

			while($d=mysql_fetch_array($r,MYSQL_ASSOC)){
				
				$received = $d['received'];
				$send = $d['send'];
						
				$info[] = array(
					'hset_id' => $d['hset_id'],
					'keyword' => $d['keyword'],
					'service' => $d['service'],
					'operator' => $d['operator_name'],
					'adn' => $d['adn'],
					'received' => $received,
					'send' => $send,
					'date_send' => $d['formatted_date_send']
				);	
			}		
			echo json_encode(array('result'=>$info,'total_page'=>ceil($count/$limit),'page'=>$page,'count'=>$count,'perPage'=>$limit));
		break;
		
		// 2016-18-04 14:50
		case 'view_pixel':
			$page = $_GET['page'];
			$start = $_GET['start'];
			$end = $_GET['end'];
			$startTime = $_GET['startTime'];
			$endTime = $_GET['endTime'];
			$pixel = trim($_GET['pixel']);
			$msisdn = trim($_GET['msisdn']);
			$where = "";
			
			if($startTime!=''){
				$startTime .= ':00';	
			}
			if($endTime!=''){
				$endTime .= ':59';	
			}
			
			if(!isset($page)){
				$page = 1;	
			}
			
			// CONSTRUCT DATE INTO MYSQL FORMAT dd/mm/yyyy => yyyy/mm/dd
			if($start!=''){
				$arrStart = explode('/',$start);
				$start = mysql_escape_string($arrStart[2].'-'.$arrStart[1].'-'.$arrStart[0]);
			}
			
			if($end!=''){
				$arrEnd = explode('/',$end);
				$end = mysql_escape_string($arrEnd[2].'-'.$arrEnd[1].'-'.$arrEnd[0]);
			}
			
			$info = array();
			
			if($start=='' && $end==''){
				$sql = "select a.hset_id,a.msisdn,
						b.keyword,
						b.service,b.adn,a.date_send,operator_name,
						CONCAT(DATE_FORMAT(a.date_send,'%e/%m/%Y'),' ',a.time_send) as formatted_date_send,
						if(status=1,'SEND','NOT SEND') send,a.hash,a.closereason 
						from hmo a join hset b on a.hset_id=b.id where 1 
						AND `hash` like'%$pixel%'
						AND a.msisdn like'%$msisdn%' " . $where;
				$sqlCount = "select COUNT(1) N from hmo a join hset b on a.hset_id=b.id where 1 
							 AND `hash` like'%$pixel%'
							 AND a.msisdn like'%$msisdn%' " . $where;		
			} else
			if($start=='' && $end!=''){
				$queryDate = " STR_TO_DATE(CONCAT(a.date_send,' ',a.time_send),'%Y-%m-%d %T')<=
							   STR_TO_DATE(CONCAT('$end',' ','$endTime'),'%Y-%m-%d %T') ";
							   
				$sql = "select a.hset_id,a.msisdn,
						b.keyword,
						b.service,b.adn,a.date_send,operator_name,
						CONCAT(DATE_FORMAT(a.date_send,'%e/%m/%Y'),' ',a.time_send) as formatted_date_send,
						if(status=1,'SEND','NOT SEND') send,a.hash,a.closereason 
						from hmo a join hset b on a.hset_id=b.id where 1 
						AND $queryDate
						AND `hash` like'%$pixel%'
						AND a.msisdn like'%$msisdn%' " . $where;
				$sqlCount = "select COUNT(1) N from hmo a join hset b on a.hset_id=b.id where 1 
							 AND $queryDate
							 AND `hash` like'%$pixel%'
							 AND a.msisdn like'%$msisdn%' " . $where;	
			} else
			if($start!='' && $end==''){
				$queryDate = " STR_TO_DATE(CONCAT(a.date_send,' ',a.time_send),'%Y-%m-%d %T')>=
							   STR_TO_DATE(CONCAT('$start',' ','$startTime'),'%Y-%m-%d %T') ";
							   
				$sql = "select a.hset_id,a.msisdn,
						b.keyword,
						b.service,b.adn,a.date_send,operator_name,
						CONCAT(DATE_FORMAT(a.date_send,'%e/%m/%Y'),' ',a.time_send) as formatted_date_send,
						if(status=1,'SEND','NOT SEND') send,a.hash,a.closereason 
						from hmo a join hset b on a.hset_id=b.id where 1 
						AND $queryDate
						AND `hash` like'%$pixel%'
						AND a.msisdn like'%$msisdn%' " . $where;
				$sqlCount = "select COUNT(1) N from hmo a join hset b on a.hset_id=b.id where 1 
							 AND $queryDate
							 AND `hash` like'%$pixel%'
							 AND a.msisdn like'%$msisdn%' " . $where;	
			} else
			if($start!='' && $end!=''){
				$queryDate = " STR_TO_DATE(CONCAT(a.date_send,' ',a.time_send),'%Y-%m-%d %T')>=
							   STR_TO_DATE(CONCAT('$start',' ','$startTime'),'%Y-%m-%d %T') AND
							   STR_TO_DATE(CONCAT(a.date_send,' ',a.time_send),'%Y-%m-%d %T')<=
							   STR_TO_DATE(CONCAT('$end',' ','$endTime'),'%Y-%m-%d %T')";
							   
				$sql = "select a.hset_id,a.msisdn,
						b.keyword,
						b.service,b.adn,a.date_send,operator_name,
						CONCAT(DATE_FORMAT(a.date_send,'%e/%m/%Y'),' ',a.time_send) as formatted_date_send,
						if(status=1,'SEND','NOT SEND') send,a.hash,a.closereason 
						from hmo a join hset b on a.hset_id=b.id where 1 
						AND $queryDate
						AND `hash` like'%$pixel%'
						AND a.msisdn like'%$msisdn%' " . $where;
				$sqlCount = "select COUNT(1) N from hmo a join hset b on a.hset_id=b.id where 1 
							 AND $queryDate
							 AND `hash` like'%$pixel%'
							 AND a.msisdn like'%$msisdn%' " . $where;	
							 
							 //closereason<>'not unique'	 	
			}
			
			$r = mysql_query($sqlCount);
			$d = mysql_fetch_array($r,MYSQL_ASSOC);
			$count = $d['N'];
			
			$limit = LIMIT;
			$sqlLimit = 'limit '.(($page-1)*$limit).','.$limit;
			
			$sql1 = $sql . " order by a.date_send desc " . $sqlLimit;
			$r = mysql_query($sql1);
			
			while($d=mysql_fetch_array($r,MYSQL_ASSOC)){
				$info[] = $d;	
			}
			
			echo json_encode(array('result'=>$info,'total_page'=>ceil($count/$limit),
							'page'=>$page,'count'=>$count,'perPage'=>$limit,'perPage'=>$limit));
		break;
		
		case 'view_lp_report':
			$_GET['start'] = str_replace('-','/',$_GET['start']);
			$_GET['end'] = str_replace('-','/',$_GET['end']);
			$params = array('start'=>$_GET['start'],'end'=>$_GET['end'],'page'=>$_GET['page'],'service'=>$_GET['service'],'limit'=>$_GET['limit']);
			$url = 'http://kiosapp.info/landingcount/getLandingCount.php?'.http_build_query($params);
			$result = file_get_contents($url);
			echo $result;
		break;
	}
?>
