<?php

class xlsdp_dr_processor extends default_dr_processor {

    private function _setStatus($drData) 
    {
		$main_config = loader_config::getInstance ()->getConfig ( 'main' );

        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => 'Start : ' . serialize($drData)));
        
        $mtData = loader_data::get('mt');
        $mtData->closeReason = $drData->statusCode;
        $mtData->msgId = $drData->msgId;
        $mtData->adn = $drData->adn;
        $mtData->msisdn = $drData->msisdn;
        $mtData->status = $drData->statusInternal;
        $mtData->serviceId = $drData->serviceId;
	
		/* if((strpos(strtoupper($drData->subject), "MT;PUSH;SMS;TEXT") !== FALSE || strpos(strtoupper($drData->subject), "MT;PUSH;SMS;DAILYPUSH") !== FALSE) && in_array($drData->serviceId, array('9987605','9987606')))
		{
			$load_model = loader_model::getInstance();
			$revCheck = $load_model->load('revcheck', 'connDatabase1');
			$user = $load_model->load('user', 'connDatabase1');

			if(strtoupper($mtData->status) == 'DELIVERED')
			{
				$filename = $drData->msisdn . "_" . $drData->service . "_" . str_replace(";","_",$drData->subject);
				$path_msisdn_lock = "/Logs2/xlsdp/lock_dr/locksuccess_" . date("Ymd") . "/" . $filename . ".lock";

				if (!file_exists ( $path_msisdn_lock )) 
				{
					touch($path_msisdn_lock);
					
					$splitSubject = explode(";", $drData->subject);
				
					$mainSubject = "NORMAL";
					if(count($splitSubject) > 4) $mainSubject = strtoupper($splitSubject[4]);

					// Update revenue on revcheck table
					if(strpos(strtoupper($drData->subject), "MT;PUSH;SMS;TEXT") !== FALSE)
						$revCheck->updateMoRevByRaw($drData->service, $mainSubject);
					else
						$revCheck->updateDpRevByRaw($drData->service, $mainSubject);
				}
				else
				{
					$log->write(array('level' => 'debug', 'message' => 'Rejected : Failed Update Rev Check MSISDN ['.$dr_data->msisdn.']'));
				}
			}

			// Update status to active = 8 & 9 in cicilan 1 - 7
			if(in_array($drData->service,$main_config->listServiceCicilan))
			{
				if(strtoupper($mtData->status) == 'DELIVERED'){
					$active = '8'; $next_upload_date = 30;
				}
				else if(strtoupper($mtData->status) == 'FAILED'){
					$active = '9'; $next_upload_date = 0;
				}

				$revCheck->inActive($active, $drData->service, $drData->msisdn);
			}
		} */
		
		//setStatusDownCharge
		if($dc = $this->getDownCharge($drData->extStatus)) {
			$mtData->price = $dc['price'];
			$mtData->closeReason = $mtData->closeReason.'|DC|'.$drData->serviceId;
			return loader_model::getInstance()->load('tblmsgtransact', 'connDatabase1')->setStatusDownCharge($mtData,$dc['sid']);
		} else {
				return loader_model::getInstance()->load('tblmsgtransact', 'connDatabase1')->setStatus($mtData);
		}
	
		// Remove locking DR service cicilan 1 - 6
		if(in_array($drData->service,$main_config->listServiceCicilan))
		{
			$lockdr = '/app/xmp2012/buffers/xlsdp/lock_dr_'.$drData->service.'_'.$drData->msisdn;
			
			if(file_exists($lockdr)) unlink($lockdr);
		}

    }

    /**
     * update from cdr table to transact
     *
     * @param 
     * 		array
     * 			-q : from hour
     * 			-w : to hour
     * 			-c : conn DB used to updating
     * 			-f : from database.table
     * 			-t : to database.table
     * 			
     */
    public function updateTransact($arr) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => 'Start : ' . serialize($arr)));

        if (empty($arr['c'])) {
            $log->write(array('level' => 'debug', 'message' => 'Parameter missing, exiting script'));
            return false;
        }

        $configDr = loader_config::getInstance()->getConfig('dr');
        $parameter = array(
            'q' => date('H'),
            'w' => date('H', strtotime('-' . $configDr->defaultHour . ' hour')),
            'f' => 'cdr.cdr_' . date('Ymd'),
            't' => 'tbl_msgtransact'
        );

        foreach ($parameter as $params => $value) {
            if (!isset($arr[$params])) {
                $arr[$params] = $value;
            }
        }
        return loader_model::getInstance()->load('cdr', $arr['c'])->updateTransact($arr);
    }

    public function saveToDb($str) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => 'Start : ' . serialize($str)));

        $drData = $this->getDRData($str);
        //print_r($drData);

        $load_config = loader_config::getInstance();
        $config_dr = $load_config->getConfig('dr');
		$configMain = loader_config::getInstance ()->getConfig('main');
        $type = 'text';
        $drData->extStatus = $str->closeReason; 
        
		$log->write(array('level' => 'debug', 'message' => 'Before mapping => msisdn = '.$str->msisdn.', msgstatus [ '.$str->msgStatus.' ] - closereason [ '.$str->closeReason.' ]'));

        if ($config_dr->responseMap [$type] [$str->msgStatus]) {        	        	
            //$str->closeReason = $config_dr->responseMap [$type] [$str->msgStatus];
            //$str->closeReason = $config_dr->responseMap [$type] [$str->closeReason];            
        	switch ($str->msgStatus) {
			case '16': case 16 :

				$str->statusCode = $str->msgStatus;
				$str->closeReason = $config_dr->responseMap [$type] [$str->msgStatus];
				
				$log->write(array('level' => 'debug', 'message' => 'Processing mapping => msisdn = '.$str->msisdn.', msgstatus [ '.$str->msgStatus.' ] - closereason [ '.$str->closeReason.' ]'));

			break;
			default:
				$str->closeReason = $config_dr->responseMap [$type] [$str->msgStatus];
			break;
		}        	
        } else {
            $str->closeReason = "UNKNOWN";
        }
        
		$log->write(array('level' => 'debug', 'message' => 'After mapping => msisdn = '.$str->msisdn.', msgstatus [ '.$str->msgStatus.' ] - closereason [ '.$str->closeReason.' ]'));

        $drData->closeReason = $str->closeReason;
        $drData->statusText = $str->statusText;
        $drData->statusCode = $str->statusCode;
        $drData->statusInternal = $str->closeReason;
        $drData->serviceId = $str->serviceId;
        $drData->cdrHour = date('G');	
        
		$drData->smartcharge = $str->smartcharge;
        $drData->next_sid = $str->next_sid;
                
        // branching for smartcharge here        
		if ((int)$drData->smartcharge) {
			$smartcharge_f = new smartcharge_functions();
			$SmartCharge = $smartcharge_f->process_on_dr($drData);
		}
		// -----------

        $save = loader_model::getInstance()->load('cdr', 'connDr')->create($drData);
        $log->write(array('level' => 'debug', 'message' => 'Return Value for Save is ' . $save));

        $this->_setStatus($drData);

		if(in_array(strtolower($drData->service),$configMain->customSMSContent['services']))
		{
			$notifyPortal = true;
			if(strpos(strtoupper($drData->subject), "MT;PUSH;SMS;DAILYPUSH") !== FALSE)
			{
				if($drData->statusInternal == "FAILED") $notifyPortal = false;
			}

			if($notifyPortal)
			{
				$this->membership($drData);
			}
		}
		
		// REPORTING AIRPAY {
		// FIRSTPUSH
		
		$model_transact = loader_model::getInstance()->load('tblmsgtransact', 'connDatabase1');
        $data = $model_transact->getDRTransactByKeyword(
			array(
				 'msisdn' 	=> $drData->msisdn
				,'adn' 		=> '99876'
				,'keyword' 	=> 'REG MEMBER'
				,'subject' 	=> 'MO;PULL;SMS;HANDLERCREATOR'
				,'service' 	=> 'memberpush'
			)
		);
		
		if(count($data) > 0)
		{
			//DN: http://149.129.252.221:8028/app/api/waki_dr.php?trx_id=53534535350986325264&status=1&statusdesc=SUCCESS&operator=3&msisdn=6281232323&sdc=99876&service=REG+Member
			
			$params = array(
				"trx_id=".$drData->msgId
			   ,"status=".$drData->statusCode
			   ,"statusdesc=".(($drData->statusInternal == "DELIVERED") ? "Success" : "Failed")
			   ,"operator=3"
			   ,"msidsn=".$drData->msisdn
			   ,"sdc=99876"
			   ,"service=".urlencode("REG Member")
			   ,"type=firstpush"
			);

			$fullurl = "http://149.129.252.221:8028/app/api/waki_dr.php";

			$hit = http_request::get ( $fullurl, implode("&", $params), 10 );
		}
		
		// DAILYPUSH
		if(strpos(strtoupper($drData->subject), 'MT;PUSH;SMS;DAILYPUSH') !== FALSE && $drData->service == 'memberpush')
		{
			$model_user = loader_model::getInstance ()->load ( 'user', 'connDatabase1' );
			$userData = $model_user->getBySubjectData($drData->msisdn, $drData->service, 'SAM');
			
			if(count($userData) > 0)
			{
				$log->write(array('level' => 'debug', 'message' => "Data : " . print_r($userData, true)));
				
				//DN: http://149.129.252.221:8028/app/api/waki_dr.php?trx_id=53534535350986325264&status=1&statusdesc=SUCCESS&operator=2&msisdn=6281232323&sdc=99876&service=REG+QUIZY+SAM+Dynamic
				
				$params = array(
					"trx_id=".$drData->msgId
				   ,"status=".$drData->statusCode
				   ,"statusdesc=".(($drData->statusInternal == "DELIVERED") ? "Success" : "Failed")
				   ,"operator=3"
				   ,"msidsn=".$drData->msisdn
				   ,"sdc=99876"
				   ,"service=".urlencode("REG Member")
				   ,"type=dailypush"
				);

				$fullurl = "http://149.129.252.221:8028/app/api/waki_dr.php";

				$hit = http_request::get ( $fullurl, implode("&", $params), 10 );
			}
		}
		
		//}
		
        return true;
    }

    public function saveToBuffer($str) 
    {
        /**
         *  Sample of DR XL received from Kannel
         *  /dr/index.php?txid=053f7996&status=8&answer=ACK%2F&ccode=922806&msisdn=538911816&ts=2013-01-07+08:30:01&adn=9228 
         **/

        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => 'Start : ' . serialize($_GET)));

        $main_config = loader_config::getInstance()->getConfig('main');
        $config_dr = loader_config::getInstance()->getConfig('dr');
        $dr_data = loader_data::get('dr');
        $buffer_file = buffer_file::getInstance();
        
        $dr_data->msisdn = (isset($_GET['msisdn'])) ? $_GET['msisdn'] : '';
        $dr_data->msgId = (isset($_GET['txid'])) ? $_GET['txid'] : '';
        $dr_data->adn = (isset($_GET['adn'])) ? $_GET['adn'] : '';
        $dr_data->dateCreated = (isset($_GET['ts'])) ? $_GET['ts'] : date('Y-m-d H:i:s');
        //$dr_data->statusCode = (isset($_GET['status'])) ? $_GET['status'] : '';
        $dr_data->serviceId = (isset($_GET['ccode'])) ? $_GET['ccode'] : '';
        $dr_data->smartcharge = (isset($_GET['sc'])) ? $_GET['sc'] : '';
		$dr_data->next_sid = (isset($_GET['nc'])) ? $_GET['nc'] : '';
	$mdataArr = $this->getVarMetaData($_GET['meta-data']);
	$dr_data->statusCode = (isset( $mdataArr['ErrorCode'])) ? $mdataArr['ErrorCode'] : '';
	//$dr_data->statusCode = (isset( $mdataArr['ErrCode'])) ? $mdataArr['ErrCode'] : '';

        /*
		$dr_data->closeReason = (isset($_GET['answer'])) ? $_GET['answer'] : '';
        $dr_data->msgStatus = (isset($_GET['status'])) ? $_GET['status'] : '';
		*/
		
		$log->write(array('level' => 'debug', 'message' => 'Before Processing mapping status ('.$_GET['status'].') => msisdn = '.$dr_data->msisdn.', msgstatus [ '.$dr_data->msgStatus.' ] - closereason [ '.$dr_data->closeReason.' ]'));

		switch ($_GET['status']) {
			case '16': case 16 :
				$answer = explode('/',urldecode($_GET['answer']));
				//$dr_data->closeReason = hexdec($answer[1]);
				//$dr_data->msgStatus = hexdec($answer[1]);

				$dr_data->closeReason = 16;
				$dr_data->msgStatus = 16;

				$log->write(array('level' => 'debug', 'message' => 'Processing mapping => msisdn = '.$dr_data->msisdn.', msgstatus [ '.$dr_data->msgStatus.' ] - closereason [ '.$dr_data->closeReason.' ]'));
				
			break;
			default:
				$dr_data->closeReason = (isset($_GET['answer'])) ? $_GET['answer'] : '';
				//$dr_data->closeReason = (isset($mdataArr['ErrorSource'])) ? $mdataArr['ErrorSource'] : '';
				$dr_data->msgStatus = (isset($_GET['status'])) ? $_GET['status'] : '';
			break;
		}

	if(isset($mdataArr['SID']) && !empty($mdataArr['SID'])) { //checking SID Dowcharging
		//Add SID to closereason
		$model_charging = loader_model::getInstance()->load('charging', 'connDatabase1');
		$prch = $model_charging->getPriceByChargingId($mdataArr['SID']);
		if($prch) {
			$objDc = json_encode(array('sid'=>$mdataArr['SID'],'price'=>$prch['gross']));
			$dr_data->closeReason = $dr_data->closeReason.'|DOWNCHARGING|'.$objDc;
		}
        }
		
		$model_transact = loader_model::getInstance()->load('tblmsgtransact', 'connDatabase1');
        $dataTansact =  $model_transact->getDRTransact($dr_data);
		
		$raw_subject = $dataTansact[0]['SUBJECT'];
		$serviceid = $dataTansact[0]['SERVICEID'];
		$subject = str_replace(";","_",$raw_subject);
		$service = $dataTansact[0]['SERVICE'];
		
		$path_msisdn_lock = "/Logs2/xlsdp/lock_dr/lock_".date("Ymd")."/".$dr_data->msisdn."_".$service."_".$subject.".lock";

		if (!file_exists ( $path_msisdn_lock ) && in_array($dr_data->serviceId, array('9987605','9987606'))) 
		{
			if((strpos(strtoupper($raw_subject), "MT;PUSH;SMS;TEXT") !== FALSE || strpos(strtoupper($raw_subject), "MT;PUSH;SMS;DAILYPUSH") !== FALSE) && in_array($serviceid, array('9987605','9987606')))
			{
				$load_model = loader_model::getInstance();
				$revCheck = $load_model->load('revcheck', 'connDatabase1');
				$user = $load_model->load('user', 'connDatabase1');

				$type = 'text';
				$status = $config_dr->responseMap [$type] [$dr_data->msgStatus];
				
				if(empty($status)) $status = 'FAILED';
				
				if(strtoupper($status) == 'DELIVERED')
				{
					$filename = $dr_data->msisdn . "_" . $service . "_" . $subject;
					$path_msisdn_lock_success = "/Logs2/xlsdp/lock_dr/locksuccess_" . date("Ymd") . "/" . $filename . ".lock";

					if (!file_exists ( $path_msisdn_lock_success )) 
					{
						touch($path_msisdn_lock_success);
						
						$splitSubject = explode(";", $raw_subject);
					
						$mainSubject = "NORMAL";
						if(count($splitSubject) > 4) $mainSubject = strtoupper($splitSubject[4]);

						// Update revenue on revcheck table
						if(strpos(strtoupper($raw_subject), "MT;PUSH;SMS;TEXT") !== FALSE)
							$revCheck->updateMoRevByRaw($service, $mainSubject);
						else
							$revCheck->updateDpRevByRaw($service, $mainSubject);
					}
					else
					{
						$log->write(array('level' => 'debug', 'message' => 'Rejected : Failed Update Rev Check MSISDN ['.$dr_data->msisdn.']'));
					}
				}

				// Update status to active = 8 & 9 in cicilan 1 - 7
				if(in_array($service,$main_config->listServiceCicilan))
				{
					if(strtoupper($status) == 'DELIVERED'){
						$active = '8'; $next_upload_date = 30;
					}
					else if(strtoupper($status) == 'FAILED'){
						$active = '9'; $next_upload_date = 0;
					}

					if(isset($active) && isset($next_upload_date))
						$revCheck->inActive($active, $service, $dr_data->msisdn);
				}
			}
		
			touch($path_msisdn_lock);
			
			file_put_contents($path_msisdn_lock, serialize($str), LOCK_EX);

			$path = $buffer_file->generate_file_name($dr_data, 'dr', 'dr');

			if ($buffer_file->save($path, $dr_data)) {
				return $config_dr->returnCode ['OK'];
			} else {
				return $config_dr->returnCode ['NOK'];
			}
		}
		else
		{
			$log->write(array('level' => 'debug', 'message' => 'Rejected : MSISDN ['.$dr_data->msisdn.'] failed to create a buffer'));
			
			return $config_dr->returnCode ['OK'];
		}
    }

    protected function getDRData($mt_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => 'Start : ' . serialize($mt_data)));

        $model_transact = loader_model::getInstance()->load('tblmsgtransact', 'connDatabase1');
        $dataTansact =  $model_transact->getDRTransact($mt_data);
	//$dataTansact = $model_transact->getDRTransactByMsgId($mt_data);
	//error_log(date('Ymd His')."MSISDN1 : ".print_r($dataTansact[0]['MSISDN'],1)."\nMSISDN2".print_r($dataTansact2[0]['MSISDN'],1)."\n",3,"/tmp/dr-xlsdp");

        $chargingData = loader_data::get('charging');
        $chargingData->chargingId = $dataTansact[0]['chargingId'];
        $chargingData->senderType = $dataTansact[0]['sender_type'];

        $drData = loader_data::get('dr');
        $drData->msgId = $dataTansact[0]['MSGINDEX'];
        $drData->operatorId = $dataTansact[0]['OPERATORID'];
        $drData->subject = $dataTansact[0]['SUBJECT'];
        $drData->charging = $chargingData;
        $drData->adn = $dataTansact[0]['ADN'];
        $drData->msisdn = $dataTansact[0]['MSISDN'];
		$drData->service = $dataTansact[0]['SERVICE'];

        return $drData;
    }

    protected function getVarMetaData($mdatastr) {

		$log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => 'In getVarMetaData start : ' . serialize($mdatastr)));

		$var = array();
		if(isset($mdatastr)) 
		{
			$mdata = str_replace('?smpp?','',$mdatastr);
			$mdataArr = explode('&',$mdata);

			$log->write(array('level' => 'debug', 'message' => 'Parsing Meta-Data : ' . print_r($mdataArr,1)));

			foreach($mdataArr as $row) 
			{
				$result = explode('=',$row);
				if(isset($result[1]))
					$var[$result[0]] = (int)$result[1];
			}
		}

		$log->write(array('level' => 'debug', 'message' => 'Result parsing Meta-Data : ' . print_r($var,1)));

		return $var;
    }

    protected function getDownCharge($closereason) {
	   $result = false;
	   $needle = 'DOWNCHARGING';
	   if(strpos($closereason,$needle)!==FALSE) {
		$dc = explode('|',$closereason);
		foreach($dc as $row) {
			if($ds = json_decode($row)){
				if(isset($ds->sid) && !empty($ds->sid)) {
					$result['sid'] = $ds->sid;
					$result['price'] = $ds->price;
				}
			}
		}
	   }
	   return $result;
     }

	public function filterMembership($dataTansact, $drData)
	{
		$result = false;
		
		if(strpos(strtoupper($dataTansact['SUBJECT']), "MT;PULL;SMS;TEXT") !== FALSE && strtoupper($drData->statusInternal) == "FALSE") $result = false;

		if(strpos(strtoupper($dataTansact['SUBJECT']), "MT;PULL;SMS;TEXT") !== FALSE && strtoupper($drData->statusInternal) == "DELIVERED") $result = true;

		if(strpos(strtoupper($dataTansact['SUBJECT']), "MT;PUSH;SMS;TEXT") !== FALSE && strtoupper($drData->statusInternal) == "FAILED") $result = false;

		if(strpos(strtoupper($dataTansact['SUBJECT']), "MT;PUSH;SMS;DAILYPUSH") !== FALSE && strtoupper($drData->statusInternal) == "FAILED") $result = false;

		if(strpos(strtoupper($dataTansact['SUBJECT']), "MT;PUSH;SMS;TEXT") !== FALSE && strtoupper($drData->statusInternal) == "DELIVERED") $result = true; 

		if(strpos(strtoupper($dataTansact['SUBJECT']), "MT;PUSH;SMS;DAILYPUSH") !== FALSE && strtoupper($drData->statusInternal) == "DELIVERED") $result = true;
		
		return $result;
	}
	
	public function membership($drData)
    {	
		$configMain = loader_config::getInstance ()->getConfig('main');
		
		$log = manager_logging::getInstance();
		$log->write(array('level' => 'debug', 'message' => 'Start : ' . serialize($drData)));
		
		$loader_model = loader_model::getInstance();
		$user = $loader_model->load('user', 'connDatabase1');
	
		$trxdata = new stdClass();
		$trxdata->msisdn=$drData->msisdn;
		$trxdata->modify_date=date("Y-m-d H:i:s");
		$trxdata->operator=$configMain->operator;
		$trxdata->service=$drData->service;
		$trxdata->sent=1; 
		
		$dataTansact = $user->getREGTransact($drData);
		
		$filterMembership = $this->filterMembership($dataTansact, $drData);
		
		$trxdata->active = ($filterMembership) ? 1 : 0;
		
		$user->updateActiveMembership($trxdata);
		
		$rec = $user->getMembership($trxdata);
		
		$data = array(
		  'msisdn'=>$trxdata->msisdn,
		  'modify_date'=>$trxdata->modify_date,
		  'operator'=>$trxdata->operator,
		  'service'=>$trxdata->service,
		  'active'=>$trxdata->active,
		  'sent'=>1,
		  'password'=>$rec[0]['password']
		);
		
		switch(strtolower($trxdata->service))
		{
			case "cicilanxl1" : 
			case "cicilanxl2" : 
			case "cicilanxl3" : 
			
				$raw = array("#USER#", "#PASSWORD#");
				$real = array($trxdata->msisdn, $rec[0]['password']);

				$urlPortal = str_replace($raw, $real, $configMain->customSMSContent['urls'][strtolower($trxdata->service)]);

				http_request::hitPortal($urlPortal);
				//$this->hitPortal($urlPortal);
					
			break;

			case "videorazzi_register" : 
			
				http_request::hitPortal($configMain->customSMSContent['urls'][strtolower($trxdata->service)].'&'.http_build_query($data));
				//$this->hitPortal($configMain->customSMSContent['urls'][strtolower($trxdata->service)].'&'.http_build_query($data));
					
			break;

			case "latino" : 
			
				$raw = array("#USER#", "#STATUS#", "#PASSWORD#");
				$real = array($trxdata->msisdn, $trxdata->active, $rec[0]['password']);

				$urlPortal = str_replace($raw, $real, $configMain->customSMSContent['urls'][strtolower($trxdata->service)]);

				http_request::hitPortal($urlPortal);
				//$this->hitPortal($urlPortal);
					
			break;
			
			case "yoga_for_you" : 
			
				//http_request::hitPortal(str_replace("#MSISDN#",$trxdata->msisdn,$configMain->customSMSContent['urls'][strtolower($trxdata->service)].'&password=' . $rec[0]['password']));
				http_request::hitPortal(str_replace("#MSISDN#",$trxdata->msisdn,$configMain->customSMSContent['urls'][strtolower($trxdata->service)].'&password=' . $rec[0]['password']));
				
			break;

			case "cerpen" :
			case "cerpen_pull" : 
			
				$type = ((strpos(strtoupper($dataTansact['SUBJECT']), "MT;PULL;SMS;TEXT") !== FALSE) ? 'pull' : 'reg');
				$raw = array("#USER#", "#PASSWORD#", "#TYPE#");
				$real = array($trxdata->msisdn, $rec[0]['password'], $type);

				$urlPortal = str_replace($raw, $real, $configMain->customSMSContent['urls'][strtolower($trxdata->service)]);

				http_request::hitPortal($urlPortal);
				//$this->hitPortal($urlPortal);
				
			break;
		}
		
		return true;
    }
	
	public function hitPortal($url)
	{
		$start_hit = time();
		
		$log = manager_logging::getInstance();
		$log->write(array('level' => 'debug', 'message' => 'Start : ' . $url));
		
		$ch = @curl_init();
		@curl_setopt($ch, CURLOPT_URL, $url);
		@curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		@curl_setopt($ch, CURLOPT_TIMEOUT, 2);
		$output = @curl_exec($ch);
		$header = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
		
		@curl_close($ch);

		$end_hit = time();
		
		$log->write(array('level' => 'debug', 'message' => "Hit : " . $url . ", Header : " . $header . ", RequestHit(".$start_hit."), ResponseHit(".$end_hit."), TimeHit=".((int)$end_hit-(int)$start_hit)));
		
		return true;
	}
	
	public function latinoMembership($drData)
	{
		$configMain = loader_config::getInstance ()->getConfig('main');
		
		if($configMain->latinoMember)
		{
			/* if($drData->msisdn == "1650354441" 
				|| $drData->msisdn == "591032274" 
				|| $drData->msisdn == "20733087"
				|| $drData->msisdn == "129292961"
			)
			{ */
				$log = manager_logging::getInstance();
				$log->write(array('level' => 'debug', 'message' => 'Start : ' . serialize($drData)));
				
				$loader_model = loader_model::getInstance();
				$user = $loader_model->load('user', 'connDatabase1');
			
				$now = date("Y-m-d H:i:s");
				
				$trxdata = new stdClass();
				$trxdata->user = $drData->msisdn;
				$trxdata->modify_date = $now;
				
				$dataTansact = $user->getREGTransact($drData);
				
				$filterMembership = $this->filterMembership($dataTansact, $drData);
				
				$trxdata->status = ($filterMembership) ? 1 : 0;
				
				$user->updateLatinoMembership($trxdata);
				
				$param = 'user=' . $trxdata->user;
				$param .= '&status=' . $trxdata->status;
				$param .= '&key=' . $configMain->latinoMemberKeyApi;
				
				$getLatinoMembership = $user->getLatinoMembership($trxdata);
				
				if($trxdata->status > 0)
				{
					$param .= '&pass=' . $getLatinoMembership[0]['password'];
					$hit = http_request::get ( $configMain->latinoApiURL, $param, 30 );
				}
			//}
		}
		
		return true;
	}
}
