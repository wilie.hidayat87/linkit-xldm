<?php

class proxl_service_custom_pullgame implements service_listener {

    public function notify($mo_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($mo_data)));

        $mt_processor = new manager_mt_processor ();
        $mo_data->subject = strtoupper('MO;PULL;' . $mo_data->channel . ';TEXT');
        $mo_data->msgStatus = "DELIVERED";

        $mo_data->id = $mt_processor->saveMOToTransact($mo_data);

        $service_config = loader_config::getInstance()->getConfig('service');
        $ini_reader = ini_reader::getInstance();
        $ini_data = loader_data::get('ini');
        $ini_data->file = $service_config->iniPath . "pullgame.ini";
        $ini_data->type = 'ack';

        $mt = loader_data::get('mt');
        $mt->inReply = $mo_data->id;
        $mt->msgId = $mo_data->msgId;
        $mt->adn = $mo_data->adn;
                

	// check content code.
        $model_check_game = loader_model::getInstance ()->load('freegame', 'connCrepo');
        if (empty($mo_data->parameter)) {
        	$isCodeExist = $model_check_game->isExist($mo_data->keyword);	
		$game_code = $mo_data->keyword;
        } else {
        	$code = explode(" ", $mo_data->keyword);
		$game_code = $code[0];
        	$isCodeExist = $model_check_game->isExist($game_code);
        	$mo_data->media = $code[1];
        }
		
	//var_dump($code, $mo_data, $isCodeExist);	

	if ($isCodeExist) {

	        // wap session
        	$dlSession = $this->rand_string();
	        $wap_session = new model_data_wapsession();

	        $wap_session->token = $dlSession;
	        $wap_session->siteId = 1;
        	$wap_session->service = $mo_data->service;
	        $wap_session->operator = $mo_data->operatorName;
        	$wap_session->msisdn = $mo_data->msisdn;
	        $wap_session->status = 1;
        	$wap_session->initialcharge = 'not charge';
	        $wap_session->limit = 4;
        	$wap_session->dateCreated = date("Y-m-d H:i:s");
	        $wap_session->dateModified = date("Y-m-d H:i:s");


		// wap session	        
	        $model_freegame = loader_model::getInstance ()->load('freegame', 'connWap');
		$model_freegame->saveDLSession($wap_session);
		// wap session	        
        
	        // reply
	        $ini_data->section = 'REPLY';
	        $ini_data->type = 'reply';
	        $msgReply = $ini_reader->get($ini_data)."?c=".$game_code."&s=".$dlSession;
	        $mt->msgData = $msgReply; 
	        
	        // charging        
	        $ini_data->section = 'CHARGING';
	        $ini_data->type = 'reply';
	        $mt->price = $ini_reader->get($ini_data);
			
	} else {

	        // reply
	        $ini_data->section = 'REPLY';
	        $ini_data->type = 'invalid';
	        $mt->msgData = $ini_reader->get($ini_data); 

	        // charging        
	        $ini_data->section = 'CHARGING';
	        $ini_data->type = 'invalid';
	        $mt->price = $ini_reader->get($ini_data);
	        
	}
	

        $mt->operatorId = $mo_data->operatorId;
        $mt->operatorName = $mo_data->operatorName;
        $mt->service = $mo_data->service;
        $mt->subject = strtoupper('MT;PULL;' . $mo_data->channel . ';TEXT');
        $mt->msisdn = $mo_data->msisdn;
        $mt->channel = $mo_data->channel;
        $mt->partner = $mo_data->partner;
        $mt->type = 'mtpull';
        $mt->mo = $mo_data;
        
        //var_dump($isCodeExist);
        
        $mt_processor->saveToQueue($mt);
        return $mo_data;
        
    }
    
    /**
     * @param $str_length, default = 6;
     */
	protected function rand_string( $length=6 ) {
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";	
	
		$str = '';
		$size = strlen( $chars );
		for( $i = 0; $i < $length; $i++ ) {
			$str .= $chars[ rand( 0, $size - 1 ) ];
		}
	
		return $str;
	}       
}


?>
