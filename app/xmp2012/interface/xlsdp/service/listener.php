<?php

class xlsdp_service_listener extends default_service_listener {

    private static $instance;

    private function __construct() {

    }

    //--------------------------------------------------------------------

    public static function getInstance() {

        if (!self::$instance) {
            self::$instance = new self ( );
        }

        return self::$instance;
    }

    public function notify($mo_data) {
        $log = manager_logging::getInstance ();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $mo_parser = mo_parser::getInstance()->getParser('mo');
        $parse_message_mo_data = $mo_parser->parseMessage($mo_data);

        $manager_service_generator = manager_service_generator::getInstance();

        $modata = $manager_service_generator->runHandler($parse_message_mo_data);

        return $modata;
    }

}
