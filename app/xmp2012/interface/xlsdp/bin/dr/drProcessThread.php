#!/usr/bin/php
<?php
require_once '/app/xmp2012/interface/xlsdp/xmp.php';

$arr = getopt('b:');
//var_dump($arr);exit;

if(isset($arr['b'])) {
$buffer = $arr['b'];
$lockPath = '/tmp/lock_xlsdp_dr_'.$arr['b'].'_process';

if (file_exists($lockPath)) {
    echo "NOK - Lock File Exist on $lockPath \n";
    exit;
} else {
    touch($lockPath);
}

$dr = new manager_dr_processor ();
$result = $dr->processbythread($buffer);

if ($result) {
    echo "OK \n";
} else {
    echo "NOK \n";
}

unlink($lockPath);


}

