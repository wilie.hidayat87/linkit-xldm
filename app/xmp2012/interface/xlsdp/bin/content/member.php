#!/usr/bin/php
<?php
require_once '/app/xmp2012/interface/xlsdp/xmp.php';

$lockPath = '/tmp/lock_xlsdp_membership_credential';

if (file_exists($lockPath)) {
    echo "NOK - Lock File Exist on $lockPath \n";
    exit;
} else {
    touch($lockPath);
}

$content = new xlsdp_broadcast_sentcontent ();

$result = $content->process();

if ($result) {
    echo "OK \n";
} else {
    echo "NOK \n";
}

unlink($lockPath);


