#!/usr/bin/php
<?php

$params = getopt('l:c:');
$limit = $params['l'];
$closereason = $params['c'];

if(!isset($limit)) {
	echo 'Incomplete parameter. Usage' . "\n";
	echo 'broadcast_retry.php -l 500' . "\n";
	exit(0);
}

require_once '/app/xmp2012/interface/xlsdp/xmp.php';
$broadcast = new xlsdp_broadcast_sentcontent ();
$result = $broadcast->retryFailedDR ($limit, $closereason);
if ($result) {
	echo "OK \n";
} else {
	echo "NOK \n";
}

exit(0);

