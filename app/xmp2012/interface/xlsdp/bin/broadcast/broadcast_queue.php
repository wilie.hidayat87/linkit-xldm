#!/usr/bin/php
<?php
/*
require_once '/app/xmp2012/interface/proxl/xmp.php';
$broadcast = new manager_broadcast ();
$result = $broadcast->process ();
if ($result) {
	echo "OK \n";
} else {
	echo "NOK \n";
}
*/

require_once '/app/xmp2012/interface/xlsdp/xmp.php';
$lockPath = '/tmp/lock_default_broadcast_queue_xlsdp_main';

if (file_exists($lockPath)) {
	echo "NOK - Lock File Exist on $lockPath \n";
	exit(0);
} else {
	touch($lockPath);
	$broadcast = new manager_broadcast ();
	$result = $broadcast->process ();
	if ($result) {
		echo "OK \n";
	} else {
		echo "NOK \n";
	}
	unlink($lockPath);
	exit(0);
}
