#!/usr/bin/php
<?php
require_once '/app/xmp2012/interface/xlsdp/xmp.php';

//$lockPath = '/tmp/lock_proxl_broadcast_process';
$lockPath = '/tmp/lock_xlsdp_broadcast_auto_schedule';
if(file_exists($lockPath)) {
	echo "NOK - Lock File Exist on $lockPath \n";
	exit;
} else {
	touch($lockPath);
}

$broadcast = new xlsdp_broadcast_sentcontent ();
$result = $broadcast->autoschedule ();
if ($result) {
	echo "OK \n";
} else {
	echo "NOK \n";
}

unlink($lockPath);
