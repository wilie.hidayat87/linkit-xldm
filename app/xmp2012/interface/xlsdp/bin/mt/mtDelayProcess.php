#!/usr/bin/php
<?php
require_once '/app/xmp2012/interface/xlsdp/xmp.php';

$lockPath = '/tmp/lock_xlsdp_mt_delay_process';

if(file_exists($lockPath)) {
	echo "NOK - Lock File Exist on $lockPath \n";
	exit;
} else {
	touch($lockPath);
}

//$mt = new xlsdp_mt_forwarder ();
//require_once('/app/xmp2012/interface/xlsdp/mt/forwarder.php');
$mt = new xlsdp_mt_forwarder ();
$result = $mt->process ();
if ($result) {
	echo "OK\n";
} else {
	echo "NOK \n";
}

unlink($lockPath);

exit();
