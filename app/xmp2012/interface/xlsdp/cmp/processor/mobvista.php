<?php

class xlsdp_cmp_processor_mobvista {

    public function process($arrdata,$hset_records) {
                // $log = manager_logging::getInstance();
			//	$log_profile = 'cmp_processor';
				$log = manager_logging::getInstance ();
			//	$log->setProfile($log_profile);
				$log->write(array('level' => 'debug', 'message' => "Start"));
				$log->write(array('level' => 'info', 'message' => "Info : ". print_r($arrdata, 1)));
				
                $hmo = loader_model::getInstance()->load('hmo', 'cmp');
                $hset = loader_model::getInstance()->load('hset', 'cmp');
		$pix_id = base64_decode(substr($arrdata['id'],2));
		$pix_parse = explode('|',$pix_id);
                $mdata['msgid'] = $pix_parse[1];
                $mdata['uuid'] = $pix_parse[0];
		$mdata['ip'] = $pix_parse[2];
		$mdata['msisdn'] = $arrdata['msisdn'];

		$arrdata['id'] = $pix_parse[1];

                $url = $hset_records['api_url'];
                $params = explode('|',$hset_records['params']);
                foreach($params as $idx => $row) {
                        if(isset($mdata[$row])) {
                                if($row=='msgData') {
                                        $url = str_replace('@'.$row.'@',urlencode($mdata[$row]),$url);
                                } else {
                                        $url = str_replace('@'.$row.'@',$mdata[$row],$url);
                                }
                        } else {
                                return true;
                        }
                }

				$log->write(array('level' => 'info', 'message' => "url : " . $url));
				
                $hmo_data = new model_data_hmo();
                $hset_data = new model_data_hset();
                $hmo_data->msisdn = $mdata['msisdn'];
                $hmo_data->date_send = date('Y-m-d');
                $hmo_data->time_send = date('H:i:s');
                $hmo_data->hash = $arrdata['id'];
                $hmo_data->hset_id = $hset_records['id'];
                $hmo_data->status = 0;
                if($hmo->isUnique($hmo_data)) {
                        if(($hset_records['inc']+1) >= $hset_records['counter']) {
                                $arrUrl = explode('?',$url);
                                $url = $arrUrl[0];
                                $prm = isset($arrUrl[1]) ? $arrUrl[1] : '';
                                $hit = http_request::get($url, $prm, $hset_records['send_timeout']);
                                $hit = trim(strtoupper($hit));
                                //if($hit=='OK') {
                                        $hmo_data->status = 1;
                                //}
                                $hmo_data->closereason=$hit;
                                $hset_data->inc = 0;
                        } else {
                                $hset_data->inc = $hset_records['inc']+1;
                        }
                } else {
			$hset_data->inc = $hset_records['inc'];
                        $hmo_data->closereason = 'not unique';
                }
				
				$log->write(array('level' => 'info', 'message' => "hmo_data : " . print_r($hmo_data)));
				
                if($hmo->save($hmo_data)) {
                        $hset_data->id = $hset_records['id'];
                        $hset->update($hset_data);
                }

                return true;
    }
}

?>

