<?php

class xlsdp_mo_processor extends default_mo_processor {
	
	public function saveToFile($arrData) {
		//error_log("processor.php\t".json_encode($arrData)."\n",3,"/tmp/tmplog.txt");
		
		$log = manager_logging::getInstance ();
		$log->write ( array ('level' => 'debug', 'message' => "Start : " . serialize ( $arrData ) ) );
		
		$load_config = loader_config::getInstance ();
		$config_mo = $load_config->getConfig ( 'mo' );
		
		$a = explode ( "?", $arrData ['meta'] );
		$msgId = '';
		
		/*
		if (isset ( $a [2] )) {
			parse_str ( $a [2], $aXL );
			if (isset ( $aXL ['TX_ID'] ))
				$msgId = bin2hex ( $aXL ['TX_ID'] );
		}
		// $a = explode("?",$arrData ['meta']);
		// $b = explode("&", $a[2]);
		// $c = explode("=", $b[0]);
		*/

		$arrData['sms'] = str_replace("%00", "", urlencode($arrData['sms']));
		
		$a = explode ( "?", urldecode($arrData['meta']) );
		if (isset ( $a [2] )) {
			parse_str ( $a [2], $aXL );
			if (isset ( $aXL ['TX_ID'] )) {
				$msgId = $aXL ['TX_ID'];
				$arrData['TX_ID'] = $msgId;
			}				
		}
		
		$mo_data = loader_data::get ( 'mo' );
		// print_r($mo_data) . "/n";
		// print_r($arrData);
		//$mo_data->msisdn = $arrData ['msisdn'];
		$mo_data->rawSMS = $arrData ['sms'];
		$mo_data->adn = $arrData ['to'];
		
		$msisdn = str_replace("%2B", "", urlencode($arrData['msisdn']));
		$raw_sms = str_replace("%2B", " ", urlencode($arrData['sms']));

		$mo_data->msisdn = $msisdn;
		$mo_data->rawSMS = $raw_sms;
		
		$split_raw_sms = explode(" ", $_REQUEST ['sms']);
		//$split_raw_sms = explode(" ", $mo_data->rawSMS);
		$service = ""; $cs = ""; $ck = ""; $adnet = ""; $pixel = "";
		
		$trigger = $split_raw_sms[0]; // REG
		$service = trim($split_raw_sms[1]); // MAIN SERVICE
			
		//if(count($split_raw_sms) > 2){
			$cs = trim($split_raw_sms[2]); // SUBKEYWORD

		if(!empty($split_raw_sms[3]))
			$ck = $split_raw_sms[3]; // CAMPAIGN KEYWORD

		if(!empty($split_raw_sms[4]))
			$pixel = $split_raw_sms[4]; // ADNET KEYWORD
			
			//print_r($split_raw_sms);
			$mo_data->customService = $cs;
			$mo_data->fullrawsms = implode(" ", $split_raw_sms);
		//}
		
		if($arrData ['partner'] <> '' || !empty($arrData ['partner']))
			$mo_data->customService = strtoupper(trim($arrData ['partner']));

		/*
		$lservice = array('cicil1','cicil2','cicil3','cicil4','cicil5','cicil6');
		$lmsisdn = array(591032274,20733087,1291997609,1291997611);
		
		$log->write ( array ('level' => 'debug', 'message' => "Filtering, msisdn : [{$msisdn}], raw : [".print_r($split_raw_sms,true)."]" ) );
		
		if(in_array(strtolower($service),$lservice))
		{
			if(strtolower($cs) == 'spc')
			{
				if(!in_array((int)$msisdn,$lmsisdn))
				{
					$log->write ( array ('level' => 'debug', 'message' => "MSISDN [{$msisdn}], not allowed without subkeyword spc [1]" ) );
					return $config_mo->returnCode ['NOK'];
				}
			}
			else
			{
				$log->write ( array ('level' => 'debug', 'message' => "MSISDN [{$msisdn}], not allowed without subkeyword spc [2]" ) );
				return $config_mo->returnCode ['NOK'];
			}
		}
		*/
		
		// $mo_data->msgId = bin2hex($c[1]);
		$mo_data->msgId = $msgId;
		// $arrData ['trx_date']->modify('+7 hours');
		$trxdate = date ( 'Y-m-d H:i:s' );
		$mo_data->incomingDate = $this->setDate ( $trxdate );

		$mo_data->channel = ($arrData['channel'] != '') ? $arrData['channel'] : 'sms';

		// $mo_data->incomingDate = $this->setDate($arrData ['trx_date']);
		$mo_data->type = 'mtpull';
		
		// Modify pixel when length more than 5 char then spcified as cpa keyword

		if(strtoupper($trigger) == "REG" && strtoupper($service) == "CEPEK")
		{
			$params = array(
				 "trxid=".$mo_data->msgId
				,"serv_id=".strtolower($service)
				,"partner=xldm"
				,"msisdn=".$mo_data->msisdn
				,"px=xllink1"
			);
			
			$fullurl = "http://kbtools.net/xldm.php";
			
			$hit = http_request::get ( $fullurl, implode("&", $params), 10 );
			
			$mo_data->customService = "XLLINK1";

		}
		
		if(strlen($mo_data->customService) >= 10 && in_array(strtoupper($service), array("MEMBER")))
		//if(strlen($mo_data->customService) >= 10 && in_array(strtoupper($service), array("MEMBER")))
		{
			$mo_data->customService = preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) {
    			return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
			}, $mo_data->customService);

			if(strpos($mo_data->customService,"-") !== FALSE)
			{
				list($testcode,$realpixel) = explode("-", $mo_data->customService);

				if(strpos($mo_data->customService,"test") !== FALSE)
					$mo_data->customService = "or_test-" . $realpixel;
				else
					$mo_data->customService = str_replace("A","_",$testcode) . "-" . $realpixel;
			}

			$params = array(
				 "trxid=".$mo_data->msgId
				,"serv_id=member"
				,"partner=xldm"
				,"msisdn=".$mo_data->msisdn
				,"px=" . $mo_data->customService
			);

			$fullurl = "http://kbtools.net/xldm.php";
			
			$hit = http_request::get ( $fullurl, implode("&", $params), 10 );
			
			$mo_data->customService = "CPA";
		}
		
		if(strtoupper($trigger) == "REG" && in_array(strtoupper($service), array("MEMBER")))
		{
			$mo_data->customService = "SAM";
			
			// REPORTING AIRPAY
			//$this->requestForward($mo_data);
		}
		
		$buffer_file = buffer_file::getInstance ();
		
		$path = $buffer_file->generate_file_name ( $mo_data );
			
		if ($buffer_file->save ( $path, $mo_data )) {
			return $config_mo->returnCode ['OK'];
		} else {
			return $config_mo->returnCode ['NOK'];
		}
	}
	
	private function setDate($char = '') {
		$log = manager_logging::getInstance ();
		$log->write ( array ('level' => 'debug', 'message' => "Start : " . $char ) );
		
		if (empty ( $char )) {
			return date ( "Y-m-d H:i:s" );
		} else {
			$y = substr ( $char, 0, 4 );
			$m = substr ( $char, 5, 2 );
			$d = substr ( $char, 8, 2 );
			$h = substr ( $char, 11, 2 );
			$i = substr ( $char, 14, 2 );
			$s = substr ( $char, 17, 2 );
			$datetime = $y . '-' . $m . '-' . $d . ' ' . $h . ':' . $i . ':' . $s;
			return $datetime;
		}
	}
	
	private function requestForward($mo_data)
	{
		$log = manager_logging::getInstance();
		//http://149.129.252.221:8028/app/api/waki_in.php?msisdn=6289536625&operator=3&sdc=99876&sms=REG+Member&trx_id=628273293273209809809809890&service_type=2&trx_date=20100120103012
		
		$params = array(
			"msisdn=".$mo_data->msisdn
		   ,"operator=3"
		   ,"sdc=99876"
		   ,"sms=".urlencode($mo_data->rawSMS)
		   ,"trx_id=".$mo_data->msgId
		   ,"service_type=2"
		   ,"trx_date=".date("YmdHis")
		);
		
		$fullurl = "http://149.129.252.221:8028/app/api/waki_in.php";

		$hit = http_request::get ( $fullurl, implode("&", $params), 10 );
		
		return true;
	}
}
