<?php

class xlsdp_mo_parser extends default_mo_parser {

    public function parseMessage($mo_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $originalrawSMS = $mo_data->rawSMS;
        $mo_data->rawSMS = urlencode($mo_data->rawSMS);
        $mo_data->rawSMS = str_replace("%C2%A7", "_", $mo_data->rawSMS);
        $mo_data->rawSMS = str_replace("%00", "", $mo_data->rawSMS);
        $mo_data->rawSMS = str_replace("\\0", "", $mo_data->rawSMS);
        $mo_data->rawSMS = strtoupper($mo_data->rawSMS);
        $extractSMS = explode(" ", urldecode($mo_data->rawSMS));
        $channel_list = loader_config::getInstance()->getConfig('main')->channel;


        if (strpos($mo_data->rawSMS, 'text:') !== FALSE && strpos($mo_data->rawSMS, 'dlvrd:') !== FALSE) {
            $tmpsms = explode('text:', $mo_data->rawSMS, 2);
            if (trim($tmpsms[1]) != '') {
                $mo_data->rawSMS = trim($tmpsms[1]);
            }
        }

        foreach ($channel_list as $key => $channelValue) {

            $channel2Upper = strtoupper($channelValue);
            if (strpos($mo_data->rawSMS, $channel2Upper . '_') !== FALSE) {

                $mo_data->rawSMS = str_replace($channel2Upper . '_', '', $mo_data->rawSMS);
                $sms2keyword = explode("_", $extractSMS[1]);
                $extractSMS[1] = $sms2keyword[1] . " " . $sms2keyword[0];
            }
        }

        $implodeSMS = implode(' ', $extractSMS);

        $extractSMS = explode(" ", urldecode($implodeSMS));

        if ($extractSMS[0] == "REG" || $extractSMS[0] == 'UNREG') {

            $mo_data->requestType = strtolower(trim($extractSMS[0]));
            $mo_data->keyword = strtolower(trim($extractSMS[1]));
            
             $partner_list = loader_config::getInstance()->getConfig('partner')->partner;
            
            // partner 3rd keyword is partner
             if (!empty($extractSMS[2]))  {
             	if (in_array(strtolower(trim($extractSMS[2])), $partner_list))               	
             		$mo_data->partner = strtolower(trim($extractSMS[2]));
             	else 
             		$mo_data->partner = "";
             } 
               
            //channel 4th keyword is channel
            $channel_list = loader_config::getInstance()->getConfig('main')->channel;
            $mo_data->channel = 'sms';
            if (!empty($extractSMS[3])) {
             	if (in_array(strtolower(trim($extractSMS[3])), $channel_list))               	
             		$mo_data->channel = strtolower(trim($extractSMS[3]));
             	else 
             		if (empty($mo_data->channel)) $mo_data->channel = 'sms';
            }
            
            $mo_data->parameter = "";
            
        } else {
        	
            $mo_data->requestType = 'pull';
            if (!empty($extractSMS[0])) $mo_data->keyword = strtolower(trim($extractSMS[0]));            
            if (!empty($extractSMS[1])) $mo_data->parameter = strtolower(trim($extractSMS[1]));
            
             $partner_list = loader_config::getInstance()->getConfig('partner')->partner;
            
            // partner 3rd keyword is partner
             if (!empty($extractSMS[2]))  {             	
             	if (in_array(strtolower(trim($extractSMS[2])), $partner_list))               	
             		$mo_data->partner = strtolower(trim($extractSMS[2]));
             	else 
             		$mo_data->partner = "";
             } 
            
            $mo_data->channel = 'sms';
            if (!empty($extractSMS[3])) {
             	if (in_array(strtolower(trim($extractSMS[3])), $channel_list))               	
             		$mo_data->channel = strtolower(trim($extractSMS[3]));
             	else 
             		if (empty($mo_data->channel)) $mo_data->channel = 'sms';
            }
             
        }
        
        $mo_data->msgData = strtolower(trim($extractSMS[0] . " " . $extractSMS[1]));
        
        /*
        if (!empty($extractSMS[3])) $mo_data->parameter = strtolower(trim($extractSMS[3]));            
        if (!empty($extractSMS[2])) $mo_data->channel = strtolower(trim($extractSMS[2]));
        else  $mo_data->channel = 'sms';
        */    
         
        $mo_data->rawSMS = urldecode($originalrawSMS);
        //$mo_data->msisdn    = urldecode($mo_data->msisdn);
        $mo_data->msisdn = str_replace("+", "", $mo_data->msisdn);        
        
        //var_dump($extractSMS, $mo_data);


/*
        if (strpos($mo_data->rawSMS, 'text:') !== FALSE && strpos($mo_data->rawSMS, 'dlvrd:') !== FALSE) {
            $tmpsms = explode('text:', $mo_data->rawSMS, 2);
            if (trim($tmpsms[1]) != '') {
                $mo_data->rawSMS = trim($tmpsms[1]);
            }
        }

        foreach ($channel_list as $key => $channelValue) {

            $channel2Upper = strtoupper($channelValue);
            if (strpos($mo_data->rawSMS, $channel2Upper . '_') !== FALSE) {

                $mo_data->rawSMS = str_replace($channel2Upper . '_', '', $mo_data->rawSMS);
                $sms2keyword = explode("_", $extractSMS[1]);
                $extractSMS[1] = $sms2keyword[1] . " " . $sms2keyword[0];
            }
        }

        $implodeSMS = implode(' ', $extractSMS);

        $extractSMS = explode(" ", urldecode($implodeSMS));

        if ($extractSMS[0] == "REG" || $extractSMS[0] == 'UNREG') {

            $mo_data->requestType = strtolower(trim($extractSMS[0]));
            $mo_data->keyword = strtolower(trim($extractSMS[1]));
        } else {
            $mo_data->requestType = 'pull';
            if (!empty($extractSMS[1])) {
                $mo_data->keyword = strtolower(trim($extractSMS[1]));
            }
        }
        $mo_data->msgData = strtolower(trim($extractSMS[0] . " " . $extractSMS[1]));

        if (!empty($extractSMS[3]))
            $mo_data->parameter = strtolower(trim($extractSMS[3]));
        if (!empty($extractSMS[2]))
            $mo_data->channel = strtolower(trim($extractSMS[2]));
        else
            $mo_data->channel = 'sms';
        $mo_data->rawSMS = urldecode($originalrawSMS);
        //$mo_data->msisdn    = urldecode($mo_data->msisdn);
        $mo_data->msisdn = str_replace("+", "", $mo_data->msisdn);
*/


        return $mo_data;
    }

}
